`timescale 1ns/100ps 


module  multiplier_16_tb;
  reg[15:0] a,b_in;
  wire rdy;
  wire[31:0] multiplier_out;
  reg clk,rst_n,en;
  initial
  begin
    clk=0;
    forever #50 clk=~clk;
  end
  initial
  begin
    $dumpfile("sim_wave.vcd");//creates vcd file for visual simulation
    $dumpvars(0,multiplier_16_tb);//dump signals 
    rst_n=0;
    en=0;
    a=16'h1231;
    b_in=16'ha231;
    #100
    begin
      rst_n=1;
      en=0;
      a=16'h2137;
      b_in=16'h0142;
    end
    #100
      begin
      rst_n=1;
      en=1;
      a=16'h0234;
      b_in=16'h12a7;
    end
    #100
    begin
      rst_n=1;
      en=1;
      a=16'h0012;
      b_in=16'ha261;
    end
    #100
    begin
      rst_n=0;
      en=1;
      a=16'h1112;
      b_in=16'h0879;
    end
    #10000 $stop;
  end
  multiplier_16 DUT(.clk(clk),.rst_n(rst_n),.en(en),.a(a),.b_in(b_in),.rdy(rdy),.multiplier_out(multiplier_out));
endmodule

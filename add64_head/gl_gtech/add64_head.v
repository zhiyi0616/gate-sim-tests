/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Sun Nov 15 20:58:22 2020
/////////////////////////////////////////////////////////////


module add1_0 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_61 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_62 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_63 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_16 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_0 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_0 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0]) );
  add1_63 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_62 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_61 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_16 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_49 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_50 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_51 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_52 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_13 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_13 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_52 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_51 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_50 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_49 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_13 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_53 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_54 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_55 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_56 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_14 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_14 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_56 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_55 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_54 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_53 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_14 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_57 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_58 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_59 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_60 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_15 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_15 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_60 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_59 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_58 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_57 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_15 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module CLA_4_20 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add16_head_0 ( A, B, C_in, F, Gm, Pm, C_out );
  input [15:0] A;
  input [15:0] B;
  output [15:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add4_head_0 A0 ( .A(A[3:0]), .B(B[3:0]), .C_in(C_in), .F(F[3:0]), .Gm(G[0]), 
        .Pm(P[0]) );
  add4_head_15 A1 ( .A(A[7:4]), .B(B[7:4]), .C_in(C[1]), .F(F[7:4]), .Gm(G[1]), 
        .Pm(P[1]) );
  add4_head_14 A3 ( .A(A[11:8]), .B(B[11:8]), .C_in(C[2]), .F(F[11:8]), .Gm(
        G[2]), .Pm(P[2]) );
  add4_head_13 A4 ( .A(A[15:12]), .B(B[15:12]), .C_in(C[3]), .F(F[15:12]), 
        .Gm(G[3]), .Pm(P[3]) );
  CLA_4_20 AAt ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module CLA_4_0 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add1_1 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_2 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_3 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_4 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_1 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_1 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_4 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0]) );
  add1_3 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1]) );
  add1_2 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2]) );
  add1_1 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3]) );
  CLA_4_1 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_5 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_6 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_7 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_8 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_2 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_2 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_8 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0]) );
  add1_7 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1]) );
  add1_6 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2]) );
  add1_5 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3]) );
  CLA_4_2 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_9 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_10 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_11 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_12 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_3 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_3 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_12 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_11 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_10 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_9 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3]) );
  CLA_4_3 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_13 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_14 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_15 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_16 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_4 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_4 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_16 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_15 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_14 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_13 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_4 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module CLA_4_17 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add16_head_1 ( A, B, C_in, F, Gm, Pm, C_out );
  input [15:0] A;
  input [15:0] B;
  output [15:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add4_head_4 A0 ( .A(A[3:0]), .B(B[3:0]), .C_in(C_in), .F(F[3:0]), .Gm(G[0]), 
        .Pm(P[0]) );
  add4_head_3 A1 ( .A(A[7:4]), .B(B[7:4]), .C_in(C[1]), .F(F[7:4]), .Gm(G[1]), 
        .Pm(P[1]) );
  add4_head_2 A3 ( .A(A[11:8]), .B(B[11:8]), .C_in(C[2]), .F(F[11:8]), .Gm(
        G[2]), .Pm(P[2]) );
  add4_head_1 A4 ( .A(A[15:12]), .B(B[15:12]), .C_in(C[3]), .F(F[15:12]), .Gm(
        G[3]), .Pm(P[3]) );
  CLA_4_17 AAt ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_17 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_18 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_19 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_20 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_5 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_5 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_20 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_19 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_18 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_17 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_5 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_21 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_22 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_23 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_24 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_6 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_6 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_24 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_23 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_22 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_21 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_6 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_25 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_26 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_27 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_28 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_7 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_7 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_28 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_27 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_26 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_25 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_7 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_29 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_30 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_31 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_32 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_8 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_8 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_32 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_31 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_30 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_29 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_8 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module CLA_4_18 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add16_head_2 ( A, B, C_in, F, Gm, Pm, C_out );
  input [15:0] A;
  input [15:0] B;
  output [15:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add4_head_8 A0 ( .A(A[3:0]), .B(B[3:0]), .C_in(C_in), .F(F[3:0]), .Gm(G[0]), 
        .Pm(P[0]) );
  add4_head_7 A1 ( .A(A[7:4]), .B(B[7:4]), .C_in(C[1]), .F(F[7:4]), .Gm(G[1]), 
        .Pm(P[1]) );
  add4_head_6 A3 ( .A(A[11:8]), .B(B[11:8]), .C_in(C[2]), .F(F[11:8]), .Gm(
        G[2]), .Pm(P[2]) );
  add4_head_5 A4 ( .A(A[15:12]), .B(B[15:12]), .C_in(C[3]), .F(F[15:12]), .Gm(
        G[3]), .Pm(P[3]) );
  CLA_4_18 AAt ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_33 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_34 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_35 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_36 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_9 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_9 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_36 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_35 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_34 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_33 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_9 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_37 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_38 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_39 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_40 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_10 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_10 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_40 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_39 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_38 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_37 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_10 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_41 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_42 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_43 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_44 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_11 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_11 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_44 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_43 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_42 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_41 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_11 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add1_45 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_46 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_47 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module add1_48 ( a, b, C_in, f, g, p );
  input a, b, C_in;
  output f, g, p;
  wire   n1, n2, n3;

  GTECH_MUXI2 U1 ( .A(n1), .B(n2), .S(C_in), .Z(f) );
  GTECH_AND_NOT U2 ( .A(p), .B(g), .Z(n2) );
  GTECH_AND_NOT U3 ( .A(b), .B(n3), .Z(g) );
  GTECH_NOT U4 ( .A(a), .Z(n3) );
  GTECH_OR2 U5 ( .A(b), .B(a), .Z(p) );
  GTECH_XNOR2 U6 ( .A(a), .B(b), .Z(n1) );
endmodule


module CLA_4_12 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add4_head_12 ( A, B, C_in, F, Gm, Pm, C_out );
  input [3:0] A;
  input [3:0] B;
  output [3:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add1_48 u1 ( .a(A[0]), .b(B[0]), .C_in(C_in), .f(F[0]), .g(G[0]), .p(P[0])
         );
  add1_47 u2 ( .a(A[1]), .b(B[1]), .C_in(C[1]), .f(F[1]), .g(G[1]), .p(P[1])
         );
  add1_46 u3 ( .a(A[2]), .b(B[2]), .C_in(C[2]), .f(F[2]), .g(G[2]), .p(P[2])
         );
  add1_45 u4 ( .a(A[3]), .b(B[3]), .C_in(C[3]), .f(F[3]), .g(G[3]), .p(P[3])
         );
  CLA_4_12 uut ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module CLA_4_19 ( P, G, C_in, Ci, Gm, Pm );
  input [3:0] P;
  input [3:0] G;
  output [4:1] Ci;
  input C_in;
  output Gm, Pm;
  wire   n1, n2;

  GTECH_AND4 U1 ( .A(P[3]), .B(P[2]), .C(P[1]), .D(P[0]), .Z(Pm) );
  GTECH_AO21 U2 ( .A(n1), .B(P[3]), .C(G[3]), .Z(Gm) );
  GTECH_AO21 U3 ( .A(n2), .B(P[2]), .C(G[2]), .Z(n1) );
  GTECH_AO21 U4 ( .A(P[1]), .B(G[0]), .C(G[1]), .Z(n2) );
  GTECH_AO21 U5 ( .A(Ci[3]), .B(P[3]), .C(G[3]), .Z(Ci[4]) );
  GTECH_AO21 U6 ( .A(Ci[2]), .B(P[2]), .C(G[2]), .Z(Ci[3]) );
  GTECH_AO21 U7 ( .A(Ci[1]), .B(P[1]), .C(G[1]), .Z(Ci[2]) );
  GTECH_AO21 U8 ( .A(C_in), .B(P[0]), .C(G[0]), .Z(Ci[1]) );
endmodule


module add16_head_3 ( A, B, C_in, F, Gm, Pm, C_out );
  input [15:0] A;
  input [15:0] B;
  output [15:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add4_head_12 A0 ( .A(A[3:0]), .B(B[3:0]), .C_in(C_in), .F(F[3:0]), .Gm(G[0]), 
        .Pm(P[0]) );
  add4_head_11 A1 ( .A(A[7:4]), .B(B[7:4]), .C_in(C[1]), .F(F[7:4]), .Gm(G[1]), 
        .Pm(P[1]) );
  add4_head_10 A3 ( .A(A[11:8]), .B(B[11:8]), .C_in(C[2]), .F(F[11:8]), .Gm(
        G[2]), .Pm(P[2]) );
  add4_head_9 A4 ( .A(A[15:12]), .B(B[15:12]), .C_in(C[3]), .F(F[15:12]), .Gm(
        G[3]), .Pm(P[3]) );
  CLA_4_19 AAt ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


module add64_head ( A, B, C_in, F, Gm, Pm, C_out );
  input [63:0] A;
  input [63:0] B;
  output [63:0] F;
  input C_in;
  output Gm, Pm, C_out;

  wire   [3:0] G;
  wire   [3:0] P;
  wire   [3:1] C;

  add16_head_0 A0 ( .A(A[15:0]), .B(B[15:0]), .C_in(C_in), .F(F[15:0]), .Gm(
        G[0]), .Pm(P[0]) );
  add16_head_3 A1 ( .A(A[31:16]), .B(B[31:16]), .C_in(C[1]), .F(F[31:16]), 
        .Gm(G[1]), .Pm(P[1]) );
  add16_head_2 A3 ( .A(A[47:32]), .B(B[47:32]), .C_in(C[2]), .F(F[47:32]), 
        .Gm(G[2]), .Pm(P[2]) );
  add16_head_1 A4 ( .A(A[63:48]), .B(B[63:48]), .C_in(C[3]), .F(F[63:48]), 
        .Gm(G[3]), .Pm(P[3]) );
  CLA_4_0 AAt ( .P(P), .G(G), .C_in(C_in), .Ci({C_out, C}), .Gm(Gm), .Pm(Pm)
         );
endmodule


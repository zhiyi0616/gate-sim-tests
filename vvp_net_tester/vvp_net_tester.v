module vvp_net_tester();
    reg a_outer = 0;        // 测试 REG 变量赋初值
    wire b_outer = 1;       // 测试 wire 变量赋初值
    wire floating = 1;      // 测试 悬空变量赋初值
    wire c;
    wire branch_a;
    wire branch_b;
    adder adder1(c,a_outer,b_outer);    // 测试信号传入
    assign branch_a = a_outer;
    assign #10 branch_b = b_outer;
    initial begin                       // 测试多个 initial 并行
        #10 a_outer = 1;
    end
    initial begin
        #20 a_outer = 0;                // 测试多个 initial 并行
    end
endmodule


module adder(
    output wire res, 
    input wire a, 
    input wire b
);
    wire tmp;
    xor xor1(res, a, b);
    nor nor1(res,a,tmp);
endmodule

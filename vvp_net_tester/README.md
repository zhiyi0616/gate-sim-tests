
本设计用于测试 VVP 文件，不需要综合。

# VVP 会如何处理赋初值
在 Verilog 中，可以使用 `wire a = 1;` 对变量赋初始值。这个语法一般不用。
- 对 reg 变量赋初值：在 T_0 线程中直接 %pushi 再 %store
- 对 wire 变量赋初值：使用 `.functor BUFT 1` 驱动


# VVP 如何处理未赋初值的变量
在 Verilog 中，不对变量赋初值是正常的做法。
- reg 默认 x
- wire 如果完全悬空没链接到任何网络，直接优化掉（删掉）
- wire 如果连接到输入，且没有被任何输出连接，则由 `.functor BUFZ` 高阻驱动

# VVP 如何处理多个 initial
开启多个 Thread 。注意不同的 initial 是同时并行执行的。

# VVP 中的 NET 变量的 Label 有何用
无用。目前未发现任何驱动信号函有 net 变量。

# VVP 如何处理信号分支？
在 Verilog 中，使用连续赋值 `assign a=b;` 可以将 a 作为 b 的分支。
- 如果 b 是 var(reg) 变量， 那么 a 被 b 驱动
- 如果 b 是 net(wire) 变量， 假设 b 由 b_d 驱动，那么 a 由 `b_d -> bufz` 驱动
  - 如果有延迟 `assign #10 a=b;`，那么一个 `.delay` 会被加在 `b_d` 与 `bufz` 之间。

# VVP 如何处理信号穿入
外部信号 a_outer 连接到内部模块的 a_inner 端口之后：
- a_inner 和 a_outer 都由同一个 driver 驱动
- a_inner 在 comment 中被标记为 a_outer 的 alias





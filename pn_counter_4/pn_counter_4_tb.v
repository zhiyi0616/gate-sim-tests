module pn_counter_4_tb();
    reg clk_tb;
    reg s_rst_tb;
    reg pos_nneg_tb;
    wire [3:0] count_tb;
    pn_counter PNC_DUT(
        clk_tb, s_rst_tb,pos_nneg_tb,count_tb,
    );
    initial begin       // Dumping and clock
        $dumpfile("sim_wave.vcd");
        $dumpvars(0, pn_counter_tb);
        clk_tb = 1'b1;

        forever #10 clk_tb = ~clk_tb;
    end
    initial begin       
        #1;             // Later then clock
        #10 s_rst_tb = 1'b1;
        #10 s_rst_tb = 1'b0;
        #10 pos_nneg_tb = 1'b1;
        #100 pos_nneg_tb = 1'b0;
        #100 $finish();
    end
endmodule

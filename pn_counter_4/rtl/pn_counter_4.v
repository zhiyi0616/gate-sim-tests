

module pn_counter_4(
    clk, s_rst, pos_nneg, count,
);
    input clk;
    input s_rst;
    input pos_nneg;
    output reg [3:0] count;
    always @(posedge clk) begin
        if (s_rst) 
            count<=4'b0;
        else
            if (pos_nneg)
                count<=count+4'b1;
            else
                count<=count-4'b1;
    end
endmodule


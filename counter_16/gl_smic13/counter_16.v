/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Sun Nov 15 15:50:38 2020
/////////////////////////////////////////////////////////////


module counter_16 ( count, count_tri, clk, rst_l, load_l, enable_l, cnt_in, 
        oe_l );
  output [3:0] count;
  output [3:0] count_tri;
  input [3:0] cnt_in;
  input clk, rst_l, load_l, enable_l, oe_l;
  wire   n19, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33,
         n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44;
  tri   [3:0] count_tri;

  FFDRHD1X count_reg_0_ ( .D(n26), .CK(clk), .RN(rst_l), .Q(count[0]), .QN(n22) );
  FFDRHD1X count_reg_3_ ( .D(n23), .CK(clk), .RN(rst_l), .Q(count[3]), .QN(n21) );
  FFDQRHD1X count_reg_2_ ( .D(n24), .CK(clk), .RN(rst_l), .Q(count[2]) );
  FFDQRHD1X count_reg_1_ ( .D(n25), .CK(clk), .RN(rst_l), .Q(count[1]) );
  BUFTSHDMX count_tri_tri_0_ ( .A(count[0]), .E(n19), .Z(count_tri[0]) );
  BUFTSHDMX count_tri_tri_1_ ( .A(count[1]), .E(n19), .Z(count_tri[1]) );
  BUFTSHDMX count_tri_tri_2_ ( .A(count[2]), .E(n19), .Z(count_tri[2]) );
  BUFTSHDMX count_tri_tri_3_ ( .A(count[3]), .E(n19), .Z(count_tri[3]) );
  NAND2HDUX U27 ( .A(n27), .B(n28), .Z(n26) );
  NAND2HDUX U28 ( .A(cnt_in[0]), .B(n29), .Z(n28) );
  MUX2HDLX U29 ( .A(n30), .B(n31), .S0(n22), .Z(n27) );
  NAND2HDUX U30 ( .A(load_l), .B(n31), .Z(n30) );
  NAND2HDUX U31 ( .A(n32), .B(n33), .Z(n25) );
  NAND2HDUX U32 ( .A(cnt_in[1]), .B(n29), .Z(n33) );
  MUX2HDLX U33 ( .A(n34), .B(n35), .S0(count[1]), .Z(n32) );
  NAND2HDUX U34 ( .A(n36), .B(count[0]), .Z(n34) );
  NAND2B1HD1X U35 ( .AN(n37), .B(n38), .Z(n24) );
  NAND2HDUX U36 ( .A(cnt_in[2]), .B(n29), .Z(n38) );
  MUX2HDLX U37 ( .A(n39), .B(n40), .S0(count[2]), .Z(n37) );
  NAND2HDUX U38 ( .A(n41), .B(n42), .Z(n23) );
  NAND2HDUX U39 ( .A(cnt_in[3]), .B(n29), .Z(n42) );
  MUX2HDLX U40 ( .A(n43), .B(n44), .S0(n21), .Z(n41) );
  NAND2HDUX U41 ( .A(n39), .B(count[2]), .Z(n44) );
  AND3HD1X U42 ( .A(count[1]), .B(n36), .C(count[0]), .Z(n39) );
  AOI21B2HD1X U43 ( .AN(n31), .BN(count[2]), .C(n40), .Z(n43) );
  OAI21HDLX U44 ( .A(count[1]), .B(n31), .C(n35), .Z(n40) );
  MUX2HDLX U45 ( .A(n29), .B(count[0]), .S0(n36), .Z(n35) );
  INVCLKHD1X U46 ( .A(n36), .Z(n31) );
  NOR2HDUX U47 ( .A(enable_l), .B(n29), .Z(n36) );
  INVCLKHD1X U48 ( .A(load_l), .Z(n29) );
  INVCLKHD1X U49 ( .A(oe_l), .Z(n19) );
endmodule


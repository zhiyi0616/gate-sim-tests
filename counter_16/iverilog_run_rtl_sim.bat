@echo off

set testbentch_file=counter_16_tb.v
set dut_file=counter_16.v

set gtech_lib_file=..\_LIBS\gtech_lib.v
set smic13_lib_file=..\_LIBS\smic13_notmchk.v

if exist rtl (
    echo Compiling rtl\%dut_file% with testbench.
    iverilog -o "%testbentch_file%_rtl.vvp" %testbentch_file% rtl\%dut_file%
    vvp -n "%testbentch_file%_rtl.vvp"
    mv sim_wave.vcd sim_wave_rtl.vcd
    echo You can use "gtkwave sim_wave_rtl.vcd" to check waveform.
)
if exist gl_smic13 (
    echo Compiling gl_smic13\%dut_file% with testbench.
    iverilog -o "%testbentch_file%_gl_smic13.vvp" %testbentch_file% gl_smic13\%dut_file% %smic13_lib_file%
    vvp -n "%testbentch_file%_gl_smic13.vvp"
    mv sim_wave.vcd sim_wave_gl_smic13.vcd
    echo You can use "gtkwave sim_wave_gl_smic13.vcd" to check waveform.
)
if exist gl_gtech (
    echo Compiling gl_gtech\%dut_file% with testbench.
    iverilog -o "%testbentch_file%_gl_gtech.vvp" %testbentch_file% gl_gtech\%dut_file% %gtech_lib_file%
    vvp -n "%testbentch_file%_gl_gtech.vvp"
    mv sim_wave.vcd sim_wave_gl_gtech.vcd
    echo You can use "gtkwave sim_wave_gl_gtech.vcd" to check waveform.
)

pause

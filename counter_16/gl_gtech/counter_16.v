/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Wed Oct 21 05:05:45 2020
/////////////////////////////////////////////////////////////


module counter_16 ( count, count_tri, clk, rst_l, load_l, enable_l, cnt_in, 
        oe_l );
  output [3:0] count;
  output [3:0] count_tri;
  input [3:0] cnt_in;
  input clk, rst_l, load_l, enable_l, oe_l;
  wire   n19, n21, n23, n25, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36,
         n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48;
  tri   [3:0] count_tri;

  GTECH_FD2 \count_reg[0]  ( .D(n40), .CP(clk), .CD(rst_l), .Q(count[0]), .QN(
        n25) );
  GTECH_FD2 \count_reg[1]  ( .D(n41), .CP(clk), .CD(rst_l), .Q(count[1]), .QN(
        n23) );
  GTECH_FD2 \count_reg[2]  ( .D(n42), .CP(clk), .CD(rst_l), .Q(count[2]), .QN(
        n21) );
  GTECH_FD2 \count_reg[3]  ( .D(n43), .CP(clk), .CD(rst_l), .Q(count[3]), .QN(
        n19) );
  GTECH_TBUF \count_tri_tri[0]  ( .A(n45), .E(n44), .Z(count_tri[0]) );
  GTECH_TBUF \count_tri_tri[1]  ( .A(n46), .E(n44), .Z(count_tri[1]) );
  GTECH_TBUF \count_tri_tri[2]  ( .A(n47), .E(n44), .Z(count_tri[2]) );
  GTECH_TBUF \count_tri_tri[3]  ( .A(n48), .E(n44), .Z(count_tri[3]) );
  GTECH_AO22 U25 ( .A(n27), .B(load_l), .C(cnt_in[0]), .D(n28), .Z(n40) );
  GTECH_OAI21 U26 ( .A(n46), .B(n29), .C(n30), .Z(n41) );
  GTECH_MUXI2 U27 ( .A(n31), .B(cnt_in[1]), .S(n28), .Z(n30) );
  GTECH_AND_NOT U28 ( .A(n32), .B(n23), .Z(n31) );
  GTECH_NAND2 U29 ( .A(n33), .B(n34), .Z(n42) );
  GTECH_OR3 U30 ( .A(n47), .B(n23), .C(n29), .Z(n34) );
  GTECH_MUXI2 U31 ( .A(n35), .B(cnt_in[2]), .S(n28), .Z(n33) );
  GTECH_AND_NOT U32 ( .A(n36), .B(n21), .Z(n35) );
  GTECH_NAND2 U33 ( .A(n37), .B(n38), .Z(n43) );
  GTECH_OR4 U34 ( .A(n29), .B(n48), .C(n21), .D(n23), .Z(n38) );
  GTECH_OR3 U35 ( .A(n28), .B(enable_l), .C(n27), .Z(n29) );
  GTECH_MUXI2 U36 ( .A(n39), .B(cnt_in[3]), .S(n28), .Z(n37) );
  GTECH_NOT U37 ( .A(load_l), .Z(n28) );
  GTECH_OA21 U38 ( .A(n21), .B(n36), .C(n48), .Z(n39) );
  GTECH_OR_NOT U39 ( .A(n32), .B(n46), .Z(n36) );
  GTECH_OR2 U40 ( .A(enable_l), .B(n27), .Z(n32) );
  GTECH_XOR2 U41 ( .A(enable_l), .B(n25), .Z(n27) );
  GTECH_NOT U42 ( .A(oe_l), .Z(n44) );
  GTECH_NOT U43 ( .A(n25), .Z(n45) );
  GTECH_NOT U44 ( .A(n23), .Z(n46) );
  GTECH_NOT U45 ( .A(n21), .Z(n47) );
  GTECH_NOT U46 ( .A(n19), .Z(n48) );
endmodule


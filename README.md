# About gtech_lib.v
File `gtech_lib.v` is copied out from the install folder of Synopsys DC 2018. It is used for simulation.

# How to Generate GTECH-based Netlist

1. Make sure no lib is set in `.synopsys_dc.setup`
2. Open DC-GUI via command `design_vision` (Note that here is no `-topo` option, or you are not able to use `compile` command.)
3. Set link library to `*`, target library to `gtech.db` and symbol lib to empty
4. Read RTL file via command `read_file XXXX.v`
5. Set current design to the top module via command `set current_design XXXX` 
6. Run `compile`
7. Output gate-level netlist via `write -f verilog -o XXX.v -h` (`-h` for hierarchy)

Note that if you write before compile, the output netlist will contain both GTECH cells and other type of cells.

# Dir Structure
For `_LIBS` Folder
- gtech_lib.v is copied out from DC 2018.06
- smic13_notmchk.v is copied out from the smic13 techlib/STD 
  - Timing checks are commented out.
    - $setuphold
    - $width
  - The delayed signals of `$setuphold` is deleted.
    - dly_CKN
    - dly_GN
    - dly_CK
    - dly_RN
    - dly_R
    - dly_SN
    - dly_S
    - dly_D
    - dly_E
    - dly_TE
    - dly_TI
    - dly_G
For each teat case `CASE` 
- `rtl`: store the RTL files.
  - `CASE.v`: Include the top module, should use `include` statement to import other design files. The top module name should be `CASE`.
  - `XXX.V`: File include other modules(designs).
- `gl_gtech`: Store the Gate-level net-lists synthesized by DC via library GTECH. 
- `gl_smic13`: Store the Gate-level net-lists synthesized by DC via library SMIC 0.13 um. 
- `CASE_tb.v`: The test-bench file, with no `include` statement inside.



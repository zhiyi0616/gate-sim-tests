module test_force();
    wire a,b,c,d;
    assign b = a;
    assign #5 c = a;
    assign #b c = a;      // re-assign cause .net8
    initial begin
        $dumpfile("test_force");
        $dumpvars(0,test_force);
        #10;
        force a = 1'b1;
        #b;
        release a;
        #10;
        $finish;
    end
endmodule
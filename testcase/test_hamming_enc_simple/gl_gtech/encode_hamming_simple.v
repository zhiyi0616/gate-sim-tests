/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Fri Jan 15 15:51:00 2021
/////////////////////////////////////////////////////////////


module encode_hamming_simple ( in, out );
  input [3:1] in;
  output [6:1] out;

  assign out[6] = in[3];
  assign out[5] = in[2];
  assign out[3] = in[1];

  GTECH_XOR2 U4 ( .A(out[6]), .B(out[5]), .Z(out[4]) );
  GTECH_XOR2 U5 ( .A(out[6]), .B(out[3]), .Z(out[2]) );
  GTECH_XOR2 U6 ( .A(out[5]), .B(out[3]), .Z(out[1]) );
endmodule


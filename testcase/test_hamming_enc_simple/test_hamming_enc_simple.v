module encode_hamming_simple(
    in,     // Source info bits
    out     // Encoded bits
);
    input wire [3:1] in;
    output wire [6:1] out;
    assign out[01]=in[1]^ in[2];
    assign out[02]=in[1]^ in[3];
    assign out[04]=in[2]^ in[3];

    assign out[03]=in[01];
    assign out[05]=in[02];
    assign out[06]=in[03];

endmodule

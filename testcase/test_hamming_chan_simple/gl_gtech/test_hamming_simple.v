/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Fri Jan 15 15:38:10 2021
/////////////////////////////////////////////////////////////


module channel_hamming_simple ( in, flip_bit_id, out );
  input [3:1] in;
  input [1:0] flip_bit_id;
  output [3:1] out;
  wire   n4, n5, n6;

  GTECH_XOR2 U7 ( .A(in[3]), .B(n4), .Z(out[3]) );
  GTECH_AND2 U8 ( .A(flip_bit_id[0]), .B(flip_bit_id[1]), .Z(n4) );
  GTECH_XOR2 U9 ( .A(in[2]), .B(n5), .Z(out[2]) );
  GTECH_AND_NOT U10 ( .A(flip_bit_id[1]), .B(flip_bit_id[0]), .Z(n5) );
  GTECH_XOR2 U11 ( .A(in[1]), .B(n6), .Z(out[1]) );
  GTECH_AND_NOT U12 ( .A(flip_bit_id[0]), .B(flip_bit_id[1]), .Z(n6) );
endmodule


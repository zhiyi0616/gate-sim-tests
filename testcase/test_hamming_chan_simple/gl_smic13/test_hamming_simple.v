/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Fri Jan 15 15:26:51 2021
/////////////////////////////////////////////////////////////


module channel_hamming_simple ( in, flip_bit_id, out );
  input [3:1] in;
  input [1:0] flip_bit_id;
  output [3:1] out;
  wire   n6, n7, n8, n9, n10;

  XOR2CLKHD1X U9 ( .A(in[3]), .B(n6), .Z(out[3]) );
  NOR2HDUX U10 ( .A(n7), .B(n8), .Z(n6) );
  XOR2CLKHD1X U11 ( .A(in[2]), .B(n9), .Z(out[2]) );
  NOR2HDUX U12 ( .A(flip_bit_id[0]), .B(n8), .Z(n9) );
  INVCLKHD1X U13 ( .A(flip_bit_id[1]), .Z(n8) );
  XOR2CLKHD1X U14 ( .A(in[1]), .B(n10), .Z(out[1]) );
  NOR2HDUX U15 ( .A(flip_bit_id[1]), .B(n7), .Z(n10) );
  INVCLKHD1X U16 ( .A(flip_bit_id[0]), .Z(n7) );
endmodule


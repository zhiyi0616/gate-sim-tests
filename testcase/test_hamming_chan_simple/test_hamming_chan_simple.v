module channel_hamming_simple(in,flip_bit_id,out);
    input wire [3:1]in;
    input wire [1:0] flip_bit_id;
    output wire [3:1]out;

    assign out[01]= (flip_bit_id==2'd01)?~in[01]:in[01];
    assign out[02]= (flip_bit_id==2'd02)?~in[02]:in[02];
    assign out[03]= (flip_bit_id==2'd03)?~in[03]:in[03];


endmodule
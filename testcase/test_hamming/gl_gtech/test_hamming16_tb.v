/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Fri Jan 15 15:15:55 2021
/////////////////////////////////////////////////////////////


module encode_hamming16 ( in, out );
  input [16:1] in;
  output [21:1] out;
  wire   \in[16] , \in[15] , \in[14] , \in[13] , \in[12] , \in[11] , \in[10] ,
         \in[9] , \in[8] , \in[7] , \in[6] , \in[5] , \in[4] , \in[3] ,
         \in[2] , \in[1] , n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11;
  assign out[21] = \in[16] ;
  assign \in[16]  = in[16];
  assign out[20] = \in[15] ;
  assign \in[15]  = in[15];
  assign out[19] = \in[14] ;
  assign \in[14]  = in[14];
  assign out[18] = \in[13] ;
  assign \in[13]  = in[13];
  assign out[17] = \in[12] ;
  assign \in[12]  = in[12];
  assign out[15] = \in[11] ;
  assign \in[11]  = in[11];
  assign out[14] = \in[10] ;
  assign \in[10]  = in[10];
  assign out[13] = \in[9] ;
  assign \in[9]  = in[9];
  assign out[12] = \in[8] ;
  assign \in[8]  = in[8];
  assign out[11] = \in[7] ;
  assign \in[7]  = in[7];
  assign out[10] = \in[6] ;
  assign \in[6]  = in[6];
  assign out[9] = \in[5] ;
  assign \in[5]  = in[5];
  assign out[7] = \in[4] ;
  assign \in[4]  = in[4];
  assign out[6] = \in[3] ;
  assign \in[3]  = in[3];
  assign out[5] = \in[2] ;
  assign \in[2]  = in[2];
  assign out[3] = \in[1] ;
  assign \in[1]  = in[1];

  GTECH_XOR3 U1 ( .A(n1), .B(n2), .C(n3), .Z(out[8]) );
  GTECH_XNOR3 U2 ( .A(\in[5] ), .B(\in[9] ), .C(n4), .Z(n3) );
  GTECH_XNOR3 U3 ( .A(n5), .B(n6), .C(n7), .Z(out[4]) );
  GTECH_XOR3 U4 ( .A(\in[3] ), .B(\in[11] ), .C(n2), .Z(n7) );
  GTECH_XOR2 U5 ( .A(\in[10] ), .B(\in[8] ), .Z(n2) );
  GTECH_XOR4 U6 ( .A(n4), .B(n8), .C(n9), .D(n10), .Z(out[2]) );
  GTECH_XOR3 U7 ( .A(\in[4] ), .B(\in[3] ), .C(\in[10] ), .Z(n9) );
  GTECH_NOT U8 ( .A(\in[6] ), .Z(n4) );
  GTECH_XOR4 U9 ( .A(\in[12] ), .B(n6), .C(n11), .D(n8), .Z(out[1]) );
  GTECH_XNOR2 U10 ( .A(n1), .B(\in[1] ), .Z(n8) );
  GTECH_XOR2 U11 ( .A(\in[11] ), .B(\in[7] ), .Z(n1) );
  GTECH_XOR3 U12 ( .A(\in[5] ), .B(\in[16] ), .C(\in[14] ), .Z(n11) );
  GTECH_XNOR3 U13 ( .A(\in[4] ), .B(\in[2] ), .C(\in[9] ), .Z(n6) );
  GTECH_XOR3 U14 ( .A(\in[12] ), .B(n5), .C(n10), .Z(out[16]) );
  GTECH_XOR2 U15 ( .A(\in[14] ), .B(\in[13] ), .Z(n10) );
  GTECH_XOR2 U16 ( .A(\in[16] ), .B(\in[15] ), .Z(n5) );
endmodule


module channel_change1_16 ( in, flip_bit_id, out );
  input [21:1] in;
  input [4:0] flip_bit_id;
  output [21:1] out;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36;

  GTECH_XOR2 U1 ( .A(in[9]), .B(n1), .Z(out[9]) );
  GTECH_AND_NOT U2 ( .A(n2), .B(n3), .Z(n1) );
  GTECH_XOR2 U3 ( .A(in[8]), .B(n4), .Z(out[8]) );
  GTECH_AND_NOT U4 ( .A(n5), .B(n3), .Z(n4) );
  GTECH_XOR2 U5 ( .A(in[7]), .B(n6), .Z(out[7]) );
  GTECH_AND2 U6 ( .A(n7), .B(n8), .Z(n6) );
  GTECH_XOR2 U7 ( .A(in[6]), .B(n9), .Z(out[6]) );
  GTECH_AND2 U8 ( .A(n7), .B(n10), .Z(n9) );
  GTECH_XOR2 U9 ( .A(in[5]), .B(n11), .Z(out[5]) );
  GTECH_AND2 U10 ( .A(n2), .B(n7), .Z(n11) );
  GTECH_XOR2 U11 ( .A(in[4]), .B(n12), .Z(out[4]) );
  GTECH_AND2 U12 ( .A(n5), .B(n7), .Z(n12) );
  GTECH_NOR3 U13 ( .A(flip_bit_id[3]), .B(flip_bit_id[4]), .C(n13), .Z(n7) );
  GTECH_XOR2 U14 ( .A(in[3]), .B(n14), .Z(out[3]) );
  GTECH_AND2 U15 ( .A(n8), .B(n15), .Z(n14) );
  GTECH_XOR2 U16 ( .A(in[2]), .B(n16), .Z(out[2]) );
  GTECH_AND2 U17 ( .A(n15), .B(n10), .Z(n16) );
  GTECH_XOR2 U18 ( .A(in[21]), .B(n17), .Z(out[21]) );
  GTECH_AND_NOT U19 ( .A(n2), .B(n18), .Z(n17) );
  GTECH_XOR2 U20 ( .A(in[20]), .B(n19), .Z(out[20]) );
  GTECH_AND_NOT U21 ( .A(n5), .B(n18), .Z(n19) );
  GTECH_OR3 U22 ( .A(n13), .B(flip_bit_id[3]), .C(n20), .Z(n18) );
  GTECH_XOR2 U23 ( .A(in[1]), .B(n21), .Z(out[1]) );
  GTECH_AND2 U24 ( .A(n2), .B(n15), .Z(n21) );
  GTECH_NOR3 U25 ( .A(flip_bit_id[3]), .B(flip_bit_id[4]), .C(flip_bit_id[2]), 
        .Z(n15) );
  GTECH_XOR2 U26 ( .A(in[19]), .B(n22), .Z(out[19]) );
  GTECH_AND2 U27 ( .A(n8), .B(n23), .Z(n22) );
  GTECH_XOR2 U28 ( .A(in[18]), .B(n24), .Z(out[18]) );
  GTECH_AND2 U29 ( .A(n10), .B(n23), .Z(n24) );
  GTECH_XOR2 U30 ( .A(in[17]), .B(n25), .Z(out[17]) );
  GTECH_AND2 U31 ( .A(n23), .B(n2), .Z(n25) );
  GTECH_XOR2 U32 ( .A(in[16]), .B(n26), .Z(out[16]) );
  GTECH_AND2 U33 ( .A(n5), .B(n23), .Z(n26) );
  GTECH_NOR3 U34 ( .A(flip_bit_id[2]), .B(flip_bit_id[3]), .C(n20), .Z(n23) );
  GTECH_NOT U35 ( .A(flip_bit_id[4]), .Z(n20) );
  GTECH_XOR2 U36 ( .A(in[15]), .B(n27), .Z(out[15]) );
  GTECH_AND2 U37 ( .A(n8), .B(n28), .Z(n27) );
  GTECH_XOR2 U38 ( .A(in[14]), .B(n29), .Z(out[14]) );
  GTECH_AND2 U39 ( .A(n10), .B(n28), .Z(n29) );
  GTECH_XOR2 U40 ( .A(in[13]), .B(n30), .Z(out[13]) );
  GTECH_AND2 U41 ( .A(n28), .B(n2), .Z(n30) );
  GTECH_AND2 U42 ( .A(n31), .B(flip_bit_id[0]), .Z(n2) );
  GTECH_XOR2 U43 ( .A(in[12]), .B(n32), .Z(out[12]) );
  GTECH_AND2 U44 ( .A(n28), .B(n5), .Z(n32) );
  GTECH_AND2 U45 ( .A(n31), .B(n33), .Z(n5) );
  GTECH_NOT U46 ( .A(flip_bit_id[1]), .Z(n31) );
  GTECH_NOR3 U47 ( .A(n34), .B(flip_bit_id[4]), .C(n13), .Z(n28) );
  GTECH_NOT U48 ( .A(flip_bit_id[2]), .Z(n13) );
  GTECH_XOR2 U49 ( .A(in[11]), .B(n35), .Z(out[11]) );
  GTECH_AND_NOT U50 ( .A(n8), .B(n3), .Z(n35) );
  GTECH_AND2 U51 ( .A(flip_bit_id[0]), .B(flip_bit_id[1]), .Z(n8) );
  GTECH_XOR2 U52 ( .A(in[10]), .B(n36), .Z(out[10]) );
  GTECH_AND_NOT U53 ( .A(n10), .B(n3), .Z(n36) );
  GTECH_OR3 U54 ( .A(flip_bit_id[2]), .B(flip_bit_id[4]), .C(n34), .Z(n3) );
  GTECH_NOT U55 ( .A(flip_bit_id[3]), .Z(n34) );
  GTECH_AND2 U56 ( .A(n33), .B(flip_bit_id[1]), .Z(n10) );
  GTECH_NOT U57 ( .A(flip_bit_id[0]), .Z(n33) );
endmodule


module decode_hamming16 ( in, out, check );
  input [21:1] in;
  output [16:1] out;
  output [5:1] check;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42;

  GTECH_XOR2 U1 ( .A(in[13]), .B(n1), .Z(out[9]) );
  GTECH_NOR2 U2 ( .A(n2), .B(n3), .Z(n1) );
  GTECH_XOR2 U3 ( .A(in[12]), .B(n4), .Z(out[8]) );
  GTECH_AND2 U4 ( .A(n5), .B(n6), .Z(n4) );
  GTECH_XOR2 U5 ( .A(in[11]), .B(n7), .Z(out[7]) );
  GTECH_NOR2 U6 ( .A(check[3]), .B(n8), .Z(n7) );
  GTECH_XOR2 U7 ( .A(in[10]), .B(n9), .Z(out[6]) );
  GTECH_AND2 U8 ( .A(n6), .B(n10), .Z(n9) );
  GTECH_XOR2 U9 ( .A(in[9]), .B(n11), .Z(out[5]) );
  GTECH_NOR2 U10 ( .A(check[3]), .B(n3), .Z(n11) );
  GTECH_OR2 U11 ( .A(n12), .B(check[2]), .Z(n3) );
  GTECH_XOR2 U12 ( .A(in[7]), .B(n13), .Z(out[4]) );
  GTECH_AND_NOT U13 ( .A(n14), .B(n15), .Z(n13) );
  GTECH_XOR2 U14 ( .A(in[6]), .B(n16), .Z(out[3]) );
  GTECH_NOR3 U15 ( .A(check[1]), .B(check[4]), .C(n15), .Z(n16) );
  GTECH_OR3 U16 ( .A(check[5]), .B(n17), .C(n2), .Z(n15) );
  GTECH_XOR2 U17 ( .A(in[5]), .B(n18), .Z(out[2]) );
  GTECH_AND3 U18 ( .A(n19), .B(n5), .C(n14), .Z(n18) );
  GTECH_XOR2 U19 ( .A(in[3]), .B(n20), .Z(out[1]) );
  GTECH_AND3 U20 ( .A(n19), .B(n10), .C(n14), .Z(n20) );
  GTECH_NOT U21 ( .A(check[5]), .Z(n19) );
  GTECH_XOR2 U22 ( .A(in[21]), .B(n21), .Z(out[16]) );
  GTECH_AND2 U23 ( .A(n5), .B(n22), .Z(n21) );
  GTECH_XOR2 U24 ( .A(in[20]), .B(n23), .Z(out[15]) );
  GTECH_AND_NOT U25 ( .A(n5), .B(n24), .Z(n23) );
  GTECH_AND2 U26 ( .A(n17), .B(check[3]), .Z(n5) );
  GTECH_XOR2 U27 ( .A(in[19]), .B(n25), .Z(out[14]) );
  GTECH_AND2 U28 ( .A(n22), .B(n10), .Z(n25) );
  GTECH_XOR2 U29 ( .A(in[18]), .B(n26), .Z(out[13]) );
  GTECH_AND_NOT U30 ( .A(n10), .B(n24), .Z(n26) );
  GTECH_NAND3 U31 ( .A(n27), .B(n28), .C(check[5]), .Z(n24) );
  GTECH_AND2 U32 ( .A(n2), .B(check[2]), .Z(n10) );
  GTECH_XOR2 U33 ( .A(in[17]), .B(n29), .Z(out[12]) );
  GTECH_AND3 U34 ( .A(n17), .B(n2), .C(n22), .Z(n29) );
  GTECH_AND2 U35 ( .A(n14), .B(check[5]), .Z(n22) );
  GTECH_AND2 U36 ( .A(check[1]), .B(n28), .Z(n14) );
  GTECH_XOR2 U37 ( .A(in[15]), .B(n30), .Z(out[11]) );
  GTECH_NOR2 U38 ( .A(n2), .B(n8), .Z(n30) );
  GTECH_OR2 U39 ( .A(n12), .B(n17), .Z(n8) );
  GTECH_NOT U40 ( .A(check[2]), .Z(n17) );
  GTECH_OR3 U41 ( .A(check[5]), .B(n28), .C(n27), .Z(n12) );
  GTECH_NOT U42 ( .A(check[3]), .Z(n2) );
  GTECH_XOR2 U43 ( .A(in[14]), .B(n31), .Z(out[10]) );
  GTECH_AND3 U44 ( .A(check[3]), .B(check[2]), .C(n6), .Z(n31) );
  GTECH_NOR3 U45 ( .A(check[1]), .B(check[5]), .C(n28), .Z(n6) );
  GTECH_XNOR3 U46 ( .A(in[16]), .B(n32), .C(n33), .Z(check[5]) );
  GTECH_XNOR3 U47 ( .A(in[20]), .B(in[19]), .C(in[18]), .Z(n33) );
  GTECH_NOT U48 ( .A(n28), .Z(check[4]) );
  GTECH_XOR4 U49 ( .A(in[10]), .B(n34), .C(n35), .D(n36), .Z(n28) );
  GTECH_XNOR3 U50 ( .A(in[8]), .B(in[15]), .C(in[14]), .Z(n35) );
  GTECH_XOR4 U51 ( .A(in[20]), .B(n37), .C(n38), .D(n34), .Z(check[3]) );
  GTECH_XNOR2 U52 ( .A(in[12]), .B(in[13]), .Z(n34) );
  GTECH_XNOR3 U53 ( .A(in[5]), .B(in[4]), .C(in[21]), .Z(n38) );
  GTECH_XOR4 U54 ( .A(in[10]), .B(n37), .C(n39), .D(n40), .Z(check[2]) );
  GTECH_XNOR3 U55 ( .A(in[2]), .B(in[18]), .C(in[11]), .Z(n39) );
  GTECH_XNOR3 U56 ( .A(in[6]), .B(in[14]), .C(n41), .Z(n37) );
  GTECH_NOT U57 ( .A(n27), .Z(check[1]) );
  GTECH_XOR4 U58 ( .A(n40), .B(n32), .C(n42), .D(n36), .Z(n27) );
  GTECH_XNOR2 U59 ( .A(in[11]), .B(in[9]), .Z(n36) );
  GTECH_XOR4 U60 ( .A(in[5]), .B(in[1]), .C(n41), .D(in[13]), .Z(n42) );
  GTECH_XNOR2 U61 ( .A(in[15]), .B(in[7]), .Z(n41) );
  GTECH_XOR2 U62 ( .A(in[17]), .B(in[21]), .Z(n32) );
  GTECH_XNOR2 U63 ( .A(in[19]), .B(in[3]), .Z(n40) );
endmodule


module test_hamming16_tb (  );
  wire   n1;
  wire   [20:0] encoded;
  wire   [20:0] received;

  encode_hamming16 dut1 ( .in({n1, n1, n1, n1, n1, n1, n1, n1, n1, n1, n1, n1, 
        n1, n1, n1, n1}), .out(encoded) );
  channel_change1_16 dut3 ( .in(encoded), .flip_bit_id({n1, n1, n1, n1, n1}), 
        .out(received) );
  decode_hamming16 dut2 ( .in(received) );
  GTECH_ZERO U1 ( .Z(n1) );
endmodule


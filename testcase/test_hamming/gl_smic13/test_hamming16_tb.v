/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Fri Jan 15 15:18:44 2021
/////////////////////////////////////////////////////////////


module encode_hamming16 ( in, out );
  input [16:1] in;
  output [21:1] out;
  wire   \in[16] , \in[15] , \in[14] , \in[13] , \in[12] , \in[11] , \in[10] ,
         \in[9] , \in[8] , \in[7] , \in[6] , \in[5] , \in[4] , \in[3] ,
         \in[2] , \in[1] , n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12;
  assign out[21] = \in[16] ;
  assign \in[16]  = in[16];
  assign out[20] = \in[15] ;
  assign \in[15]  = in[15];
  assign out[19] = \in[14] ;
  assign \in[14]  = in[14];
  assign out[18] = \in[13] ;
  assign \in[13]  = in[13];
  assign out[17] = \in[12] ;
  assign \in[12]  = in[12];
  assign out[15] = \in[11] ;
  assign \in[11]  = in[11];
  assign out[14] = \in[10] ;
  assign \in[10]  = in[10];
  assign out[13] = \in[9] ;
  assign \in[9]  = in[9];
  assign out[12] = \in[8] ;
  assign \in[8]  = in[8];
  assign out[11] = \in[7] ;
  assign \in[7]  = in[7];
  assign out[10] = \in[6] ;
  assign \in[6]  = in[6];
  assign out[9] = \in[5] ;
  assign \in[5]  = in[5];
  assign out[7] = \in[4] ;
  assign \in[4]  = in[4];
  assign out[6] = \in[3] ;
  assign \in[3]  = in[3];
  assign out[5] = \in[2] ;
  assign \in[2]  = in[2];
  assign out[3] = \in[1] ;
  assign \in[1]  = in[1];

  XOR3HDLX U1 ( .A(n1), .B(n2), .C(n3), .Z(out[8]) );
  XOR3HDLX U2 ( .A(\in[5] ), .B(\in[9] ), .C(\in[6] ), .Z(n3) );
  XNOR3HD1X U3 ( .A(n4), .B(n5), .C(n6), .Z(out[4]) );
  XOR3HDLX U4 ( .A(\in[3] ), .B(\in[11] ), .C(n2), .Z(n6) );
  XNOR2HD1X U5 ( .A(\in[10] ), .B(\in[8] ), .Z(n2) );
  XOR3HDLX U6 ( .A(n7), .B(n8), .C(n9), .Z(out[2]) );
  XNOR3HD1X U7 ( .A(\in[4] ), .B(\in[3] ), .C(\in[10] ), .Z(n9) );
  XOR2CLKHD1X U8 ( .A(n10), .B(\in[6] ), .Z(n8) );
  XNOR3HD1X U9 ( .A(n11), .B(n7), .C(n12), .Z(out[1]) );
  XNOR2HD1X U10 ( .A(n4), .B(\in[12] ), .Z(n12) );
  XOR3HDLX U11 ( .A(\in[4] ), .B(\in[2] ), .C(\in[9] ), .Z(n4) );
  XNOR2HD1X U12 ( .A(n1), .B(\in[1] ), .Z(n7) );
  XNOR2HD1X U13 ( .A(\in[11] ), .B(\in[7] ), .Z(n1) );
  XOR3HDLX U14 ( .A(\in[5] ), .B(\in[16] ), .C(\in[14] ), .Z(n11) );
  XNOR3HD1X U15 ( .A(n5), .B(\in[12] ), .C(n10), .Z(out[16]) );
  XNOR2HD1X U16 ( .A(\in[14] ), .B(\in[13] ), .Z(n10) );
  XOR2CLKHD1X U17 ( .A(\in[16] ), .B(\in[15] ), .Z(n5) );
endmodule


module channel_change1_16 ( in, flip_bit_id, out );
  input [21:1] in;
  input [4:0] flip_bit_id;
  output [21:1] out;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36;

  XOR2CLKHD1X U1 ( .A(in[9]), .B(n1), .Z(out[9]) );
  NOR2HDUX U2 ( .A(n2), .B(n3), .Z(n1) );
  XOR2CLKHD1X U3 ( .A(in[8]), .B(n4), .Z(out[8]) );
  NOR2HDUX U4 ( .A(n3), .B(n5), .Z(n4) );
  XOR2CLKHD1X U5 ( .A(in[7]), .B(n6), .Z(out[7]) );
  NOR2HDUX U6 ( .A(n7), .B(n8), .Z(n6) );
  XOR2CLKHD1X U7 ( .A(in[6]), .B(n9), .Z(out[6]) );
  NOR2HDUX U8 ( .A(n7), .B(n10), .Z(n9) );
  XOR2CLKHD1X U9 ( .A(in[5]), .B(n11), .Z(out[5]) );
  NOR2HDUX U10 ( .A(n2), .B(n7), .Z(n11) );
  XOR2CLKHD1X U11 ( .A(in[4]), .B(n12), .Z(out[4]) );
  NOR2HDUX U12 ( .A(n5), .B(n7), .Z(n12) );
  NAND3HDLX U13 ( .A(n13), .B(n14), .C(flip_bit_id[2]), .Z(n7) );
  XOR2CLKHD1X U14 ( .A(in[3]), .B(n15), .Z(out[3]) );
  NOR2HDUX U15 ( .A(n8), .B(n16), .Z(n15) );
  XOR2CLKHD1X U16 ( .A(in[2]), .B(n17), .Z(out[2]) );
  NOR2HDUX U17 ( .A(n10), .B(n16), .Z(n17) );
  XOR2CLKHD1X U18 ( .A(in[21]), .B(n18), .Z(out[21]) );
  NOR2HDUX U19 ( .A(n2), .B(n19), .Z(n18) );
  XOR2CLKHD1X U20 ( .A(in[20]), .B(n20), .Z(out[20]) );
  NOR2HDUX U21 ( .A(n5), .B(n19), .Z(n20) );
  NAND3HDLX U22 ( .A(flip_bit_id[2]), .B(n13), .C(flip_bit_id[4]), .Z(n19) );
  XOR2CLKHD1X U23 ( .A(in[1]), .B(n21), .Z(out[1]) );
  NOR2HDUX U24 ( .A(n2), .B(n16), .Z(n21) );
  NAND3HDLX U25 ( .A(n13), .B(n14), .C(n22), .Z(n16) );
  XOR2CLKHD1X U26 ( .A(in[19]), .B(n23), .Z(out[19]) );
  NOR2HDUX U27 ( .A(n8), .B(n24), .Z(n23) );
  XOR2CLKHD1X U28 ( .A(in[18]), .B(n25), .Z(out[18]) );
  NOR2HDUX U29 ( .A(n10), .B(n24), .Z(n25) );
  XOR2CLKHD1X U30 ( .A(in[17]), .B(n26), .Z(out[17]) );
  NOR2HDUX U31 ( .A(n2), .B(n24), .Z(n26) );
  XOR2CLKHD1X U32 ( .A(in[16]), .B(n27), .Z(out[16]) );
  NOR2HDUX U33 ( .A(n5), .B(n24), .Z(n27) );
  NAND3HDLX U34 ( .A(n22), .B(n13), .C(flip_bit_id[4]), .Z(n24) );
  INVCLKHD1X U35 ( .A(flip_bit_id[3]), .Z(n13) );
  XOR2CLKHD1X U36 ( .A(in[15]), .B(n28), .Z(out[15]) );
  NOR2HDUX U37 ( .A(n8), .B(n29), .Z(n28) );
  XOR2CLKHD1X U38 ( .A(in[14]), .B(n30), .Z(out[14]) );
  NOR2HDUX U39 ( .A(n10), .B(n29), .Z(n30) );
  XOR2CLKHD1X U40 ( .A(in[13]), .B(n31), .Z(out[13]) );
  NOR2HDUX U41 ( .A(n2), .B(n29), .Z(n31) );
  NAND2HDUX U42 ( .A(flip_bit_id[0]), .B(n32), .Z(n2) );
  XOR2CLKHD1X U43 ( .A(in[12]), .B(n33), .Z(out[12]) );
  NOR2HDUX U44 ( .A(n5), .B(n29), .Z(n33) );
  NAND3HDLX U45 ( .A(flip_bit_id[3]), .B(n14), .C(flip_bit_id[2]), .Z(n29) );
  NAND2HDUX U46 ( .A(n32), .B(n34), .Z(n5) );
  INVCLKHD1X U47 ( .A(flip_bit_id[1]), .Z(n32) );
  XOR2CLKHD1X U48 ( .A(in[11]), .B(n35), .Z(out[11]) );
  NOR2HDUX U49 ( .A(n3), .B(n8), .Z(n35) );
  NAND2HDUX U50 ( .A(flip_bit_id[1]), .B(flip_bit_id[0]), .Z(n8) );
  XOR2CLKHD1X U51 ( .A(in[10]), .B(n36), .Z(out[10]) );
  NOR2HDUX U52 ( .A(n3), .B(n10), .Z(n36) );
  NAND2HDUX U53 ( .A(flip_bit_id[1]), .B(n34), .Z(n10) );
  INVCLKHD1X U54 ( .A(flip_bit_id[0]), .Z(n34) );
  NAND3HDLX U55 ( .A(n22), .B(n14), .C(flip_bit_id[3]), .Z(n3) );
  INVCLKHD1X U56 ( .A(flip_bit_id[4]), .Z(n14) );
  INVCLKHD1X U57 ( .A(flip_bit_id[2]), .Z(n22) );
endmodule


module decode_hamming16 ( in, out, check );
  input [21:1] in;
  output [16:1] out;
  output [5:1] check;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
         n45, n46, n47, n48, n49, n50, n51, n52;

  XOR2CLKHD1X U1 ( .A(in[13]), .B(n1), .Z(out[9]) );
  NOR2HDUX U2 ( .A(n2), .B(n3), .Z(n1) );
  XOR2CLKHD1X U3 ( .A(in[12]), .B(n4), .Z(out[8]) );
  NOR2HDUX U4 ( .A(n2), .B(n5), .Z(n4) );
  XOR2CLKHD1X U5 ( .A(in[11]), .B(n6), .Z(out[7]) );
  NOR2HDUX U6 ( .A(n3), .B(n7), .Z(n6) );
  XOR2CLKHD1X U7 ( .A(in[10]), .B(n8), .Z(out[6]) );
  NOR2HDUX U8 ( .A(n5), .B(n7), .Z(n8) );
  XOR2CLKHD1X U9 ( .A(in[9]), .B(n9), .Z(out[5]) );
  NOR2B1HD1X U10 ( .AN(n10), .B(n3), .Z(n9) );
  XNOR2HD1X U11 ( .A(in[7]), .B(n11), .Z(out[4]) );
  NAND2HDUX U12 ( .A(n12), .B(n13), .Z(n11) );
  XNOR2HD1X U13 ( .A(in[6]), .B(n14), .Z(out[3]) );
  NAND4HDLX U14 ( .A(n15), .B(n12), .C(n16), .D(n17), .Z(n14) );
  INVCLKHD1X U15 ( .A(n18), .Z(n12) );
  XNOR2HD1X U16 ( .A(in[5]), .B(n19), .Z(out[2]) );
  NAND2HDUX U17 ( .A(n13), .B(n20), .Z(n19) );
  XNOR2HD1X U18 ( .A(in[3]), .B(n21), .Z(out[1]) );
  NAND2HDUX U19 ( .A(n13), .B(n22), .Z(n21) );
  NOR2B1HD1X U20 ( .AN(n23), .B(check[5]), .Z(n13) );
  XNOR2HD1X U21 ( .A(in[21]), .B(n24), .Z(out[16]) );
  NAND2HDUX U22 ( .A(n25), .B(n20), .Z(n24) );
  INVCLKHD1X U23 ( .A(n2), .Z(n20) );
  XOR2CLKHD1X U24 ( .A(in[20]), .B(n26), .Z(out[15]) );
  NOR2HDUX U25 ( .A(n2), .B(n27), .Z(n26) );
  NAND2HDUX U26 ( .A(check[3]), .B(n28), .Z(n2) );
  XNOR2HD1X U27 ( .A(in[19]), .B(n29), .Z(out[14]) );
  NAND2HDUX U28 ( .A(n25), .B(n22), .Z(n29) );
  INVCLKHD1X U29 ( .A(n7), .Z(n22) );
  XOR2CLKHD1X U30 ( .A(in[18]), .B(n30), .Z(out[13]) );
  NOR2HDUX U31 ( .A(n7), .B(n27), .Z(n30) );
  NAND3HDLX U32 ( .A(n16), .B(n17), .C(check[5]), .Z(n27) );
  NAND2HDUX U33 ( .A(check[2]), .B(n31), .Z(n7) );
  XNOR2HD1X U34 ( .A(in[17]), .B(n32), .Z(out[12]) );
  NAND2HDUX U35 ( .A(n25), .B(n10), .Z(n32) );
  NOR2HDUX U36 ( .A(check[2]), .B(check[3]), .Z(n10) );
  NOR2B1HD1X U37 ( .AN(n23), .B(n15), .Z(n25) );
  NOR2HDUX U38 ( .A(n17), .B(check[4]), .Z(n23) );
  XOR2CLKHD1X U39 ( .A(in[15]), .B(n33), .Z(out[11]) );
  NOR2HDUX U40 ( .A(n3), .B(n18), .Z(n33) );
  NAND3HDLX U41 ( .A(n15), .B(check[1]), .C(check[4]), .Z(n3) );
  XOR2CLKHD1X U42 ( .A(in[14]), .B(n34), .Z(out[10]) );
  NOR2HDUX U43 ( .A(n5), .B(n18), .Z(n34) );
  NAND2HDUX U44 ( .A(check[2]), .B(check[3]), .Z(n18) );
  NAND3HDLX U45 ( .A(n17), .B(n15), .C(check[4]), .Z(n5) );
  INVCLKHD1X U46 ( .A(n15), .Z(check[5]) );
  XOR3HDLX U47 ( .A(in[16]), .B(n35), .C(n36), .Z(n15) );
  XOR3HDLX U48 ( .A(in[20]), .B(in[19]), .C(n37), .Z(n36) );
  INVCLKHD1X U49 ( .A(in[18]), .Z(n37) );
  INVCLKHD1X U50 ( .A(n16), .Z(check[4]) );
  XOR2CLKHD1X U51 ( .A(n38), .B(n39), .Z(n16) );
  XOR3HDLX U52 ( .A(in[8]), .B(in[15]), .C(in[14]), .Z(n39) );
  XOR3HDLX U53 ( .A(n40), .B(in[10]), .C(n41), .Z(n38) );
  INVCLKHD1X U54 ( .A(n31), .Z(check[3]) );
  XOR2CLKHD1X U55 ( .A(n42), .B(n43), .Z(n31) );
  XOR3HDLX U56 ( .A(in[5]), .B(in[4]), .C(in[21]), .Z(n43) );
  XOR3HDLX U57 ( .A(in[20]), .B(n40), .C(n44), .Z(n42) );
  XOR2CLKHD1X U58 ( .A(in[12]), .B(in[13]), .Z(n40) );
  INVCLKHD1X U59 ( .A(n28), .Z(check[2]) );
  XOR3HDLX U60 ( .A(n45), .B(n46), .C(n44), .Z(n28) );
  XNOR3HD1X U61 ( .A(in[6]), .B(in[14]), .C(n47), .Z(n44) );
  XOR3HDLX U62 ( .A(in[2]), .B(in[18]), .C(in[11]), .Z(n46) );
  XOR2CLKHD1X U63 ( .A(in[10]), .B(n48), .Z(n45) );
  INVCLKHD1X U64 ( .A(n17), .Z(check[1]) );
  XOR2CLKHD1X U65 ( .A(n49), .B(n50), .Z(n17) );
  XOR3HDLX U66 ( .A(n51), .B(in[13]), .C(n48), .Z(n50) );
  XOR2CLKHD1X U67 ( .A(in[19]), .B(in[3]), .Z(n48) );
  XOR2CLKHD1X U68 ( .A(in[5]), .B(in[1]), .Z(n51) );
  XOR3HDLX U69 ( .A(n47), .B(n35), .C(n41), .Z(n49) );
  XOR2CLKHD1X U70 ( .A(n52), .B(in[9]), .Z(n41) );
  INVCLKHD1X U71 ( .A(in[11]), .Z(n52) );
  XOR2CLKHD1X U72 ( .A(in[17]), .B(in[21]), .Z(n35) );
  XOR2CLKHD1X U73 ( .A(in[15]), .B(in[7]), .Z(n47) );
endmodule


module test_hamming16_tb (  );

  wire   [15:0] source;
  wire   [20:0] encoded;
  wire   [4:0] chan_flip_bit;
  wire   [20:0] received;

  encode_hamming16 dut1 ( .in({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .out(encoded) );
  channel_change1_16 dut3 ( .in(encoded), .flip_bit_id({1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0}), .out(received) );
  decode_hamming16 dut2 ( .in(received) );
endmodule


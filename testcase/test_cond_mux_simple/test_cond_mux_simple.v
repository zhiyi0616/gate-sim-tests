module test_cond_mux_simple();
    reg [1:0]a;
    wire b;
    assign b = a==2'b10 ? 1'b1 : 1'b0;

endmodule
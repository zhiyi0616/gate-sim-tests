// This file is to figure out the msb-lsb arrangement in vvp
module test_msb_lsb();
    wire [3:0]a;
    wire [3:0]b; 
    wire [1:0]c;
    wire [5:0]d;
    assign b = a|4'b1100;
    assign c[1] = b[1]; // left of c
    assign c[0] = b[3]; // right of c
    assign d={c,a};

endmodule

/*

o0000000001077068 .functor BUFZ 4, C4<zzzz>; HiZ drive
L_00000000010b7838 .functor BUFT 1, C4<1100>, C4<0>, C4<0>, C4<0>;
L_00000000008f83c0 .functor OR 4, o0000000001077068, L_00000000010b7838, C4<0000>, C4<0000>;
v00000000008f8320_0 .net/2u *"_s0", 3 0, L_00000000010b7838;  1 drivers
v00000000008fef10_0 .net *"_s12", 0 0, L_00000000010714b0;  1 drivers
v00000000008fa120_0 .net *"_s7", 0 0, L_00000000010757a0;  1 drivers
v00000000008fa1c0_0 .net "a", 3 0, o0000000001077068;  0 drivers
v00000000010755c0_0 .net "b", 3 0, L_00000000008f83c0;  1 drivers
v0000000001075660_0 .net "c", 1 0, L_0000000001071410;  1 drivers
v0000000001075700_0 .net "d", 5 0, L_0000000001071550;  1 drivers
L_00000000010757a0 .part L_00000000008f83c0, 1, 1;
L_0000000001071410 .concat8 [ 1 1 0 0], L_00000000010714b0, L_00000000010757a0;
L_00000000010714b0 .part L_00000000008f83c0, 3, 1;
L_0000000001071550 .concat [ 4 2 0 0], o0000000001077068, L_0000000001071410;


*/
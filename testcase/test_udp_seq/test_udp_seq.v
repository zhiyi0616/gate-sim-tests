module test_seq_udp();
    reg clk_tb;
    reg a;
    wire i,o;

    // build a loop
    and(i,o, a);
    dff(o,clk_tb,i);
endmodule 


primitive dff(out,clk,in);
    input clk,in;
    reg  out;
    output  out;
    table 
        //clk  in  out-1   out
        (01)   0   : ?   : 0 ;
        (01)   1   : ?   : 1 ;
        (0x)   1   : 1   : 1 ;
        (0x)   0   : 0   : 0 ;
        (?0)   ?   : ?   : - ;
        ?   (??)   : ?   : - ;
    endtable
endprimitive



/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Fri Jan 15 15:47:58 2021
/////////////////////////////////////////////////////////////


module decode_hamming16 ( in, out, check );
  input [6:1] in;
  output [3:1] out;
  output [3:1] check;
  wire   n7, n8, n9, n10, n11, n12;

  XOR2CLKHD1X U13 ( .A(in[6]), .B(n7), .Z(out[3]) );
  NOR3HD1X U14 ( .A(n8), .B(n9), .C(check[1]), .Z(n7) );
  XOR2CLKHD1X U15 ( .A(in[5]), .B(n10), .Z(out[2]) );
  NOR3HD1X U16 ( .A(n11), .B(n9), .C(check[2]), .Z(n10) );
  INVCLKHD1X U17 ( .A(check[3]), .Z(n9) );
  XOR2CLKHD1X U18 ( .A(in[3]), .B(n12), .Z(out[1]) );
  NOR3HD1X U19 ( .A(n8), .B(n11), .C(check[3]), .Z(n12) );
  INVCLKHD1X U20 ( .A(check[1]), .Z(n11) );
  INVCLKHD1X U21 ( .A(check[2]), .Z(n8) );
  XOR3HDLX U22 ( .A(in[6]), .B(in[4]), .C(in[5]), .Z(check[3]) );
  XOR3HDLX U23 ( .A(in[6]), .B(in[2]), .C(in[3]), .Z(check[2]) );
  XOR3HDLX U24 ( .A(in[5]), .B(in[1]), .C(in[3]), .Z(check[1]) );
endmodule


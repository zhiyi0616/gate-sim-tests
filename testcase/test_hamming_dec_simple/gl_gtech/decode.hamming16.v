/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Fri Jan 15 15:46:17 2021
/////////////////////////////////////////////////////////////


module decode_hamming16 ( in, out, check );
  input [6:1] in;
  output [3:1] out;
  output [3:1] check;
  wire   n8, n9, n10, n11, n12;

  GTECH_XOR2 U12 ( .A(in[6]), .B(n8), .Z(out[3]) );
  GTECH_NOR3 U13 ( .A(check[1]), .B(n9), .C(n10), .Z(n8) );
  GTECH_XOR2 U14 ( .A(in[5]), .B(n11), .Z(out[2]) );
  GTECH_AND3 U15 ( .A(n10), .B(check[1]), .C(check[3]), .Z(n11) );
  GTECH_XOR2 U16 ( .A(in[3]), .B(n12), .Z(out[1]) );
  GTECH_AND3 U17 ( .A(n9), .B(check[1]), .C(check[2]), .Z(n12) );
  GTECH_NOT U18 ( .A(n9), .Z(check[3]) );
  GTECH_XNOR3 U19 ( .A(in[6]), .B(in[4]), .C(in[5]), .Z(n9) );
  GTECH_NOT U20 ( .A(n10), .Z(check[2]) );
  GTECH_XNOR3 U21 ( .A(in[6]), .B(in[2]), .C(in[3]), .Z(n10) );
  GTECH_XOR3 U22 ( .A(in[5]), .B(in[1]), .C(in[3]), .Z(check[1]) );
endmodule


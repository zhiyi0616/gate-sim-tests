module test_udp_comb();
    wire out_tb;
    reg cin_tb, a_tb,b_tb;
    sum(out_tb,cin_tb,a_tb,b_tb);
endmodule


primitive sum(out,cin,a,b);
    output out;
    input a,b,cin;
    table 
        0 0 0 : 0;
        0 0 1 : 1;
        0 1 0 : 1;
        0 1 1 : 0;
        1 0 0 : 1;
        1 0 1 : 0;
        1 1 0 : 0;
        1 1 1 : 1;
    endtable
endprimitive
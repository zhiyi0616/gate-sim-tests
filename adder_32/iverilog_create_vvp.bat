@echo off

set dut_file=adder_32.v

set gtech_lib_file=..\_LIBS\gtech_lib.v
set smic13_lib_file=..\_LIBS\smic13_notmchk.v

if exist "rtl" (
    iverilog -o "rtl\%dut_file%.vvp" rtl\%dut_file% 
    echo File "rtl\%dut_file%.vvp" generated.
)

if exist "gl_gtech" (
    iverilog -o "gl_gtech\%dut_file%.vvp" gl_gtech\%dut_file% %gtech_lib_file%
    echo File "gl_gtech\%dut_file%.vvp" generated.
)

if exist "gl_smic13" (
    iverilog -o "gl_smic13\%dut_file%.vvp" gl_smic13\%dut_file% %smic13_lib_file%
    echo File "gl_smic13\%dut_file%.vvp" generated.
)

pause

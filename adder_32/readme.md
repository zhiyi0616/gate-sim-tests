# 32-bits Adder
Found by Haichuan Hu


# 关于DC报错
DC 可能会因为 `always` 块中同时有阻塞和非阻塞赋值报错。注意这样的语法在仿真中是可以的，但是在综合时不得存在。

# 文件夹
- 文件夹 `rtl` 包含寄存器传输级 Verilog 文件（原始文件）
- 文件夹 `gl_gtech` 包含由 DC 2018 综合生成的纯 GTECH 网表。


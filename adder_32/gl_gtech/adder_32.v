/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Wed Oct 21 04:06:54 2020
/////////////////////////////////////////////////////////////


module full_adder_0 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_1 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_2 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_3 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_4 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_5 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_6 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_7 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_8 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_9 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_10 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_11 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_12 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_13 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_14 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_15 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_16 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_17 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_18 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_19 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_20 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_21 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_22 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_23 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_24 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_25 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_26 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_27 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_28 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_29 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_30 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module full_adder_31 ( A, B, C_in, S, C_out );
  input A, B, C_in;
  output S, C_out;
  wire   n1;

  GTECH_XOR2 U1 ( .A(C_in), .B(n1), .Z(S) );
  GTECH_AO22 U2 ( .A(B), .B(A), .C(n1), .D(C_in), .Z(C_out) );
  GTECH_XOR2 U3 ( .A(A), .B(B), .Z(n1) );
endmodule


module adder_32 ( A, B, M, S, V, C );
  input [31:0] A;
  input [31:0] B;
  output [31:0] S;
  input M;
  output V, C;

  wire   [31:0] net_MxorB;
  wire   [30:0] carry;

  full_adder_0 faddr0 ( .A(A[0]), .B(net_MxorB[0]), .C_in(M), .S(S[0]), 
        .C_out(carry[0]) );
  full_adder_31 faddr1 ( .A(A[1]), .B(net_MxorB[1]), .C_in(carry[0]), .S(S[1]), 
        .C_out(carry[1]) );
  full_adder_30 faddr2 ( .A(A[2]), .B(net_MxorB[2]), .C_in(carry[1]), .S(S[2]), 
        .C_out(carry[2]) );
  full_adder_29 faddr3 ( .A(A[3]), .B(net_MxorB[3]), .C_in(carry[2]), .S(S[3]), 
        .C_out(carry[3]) );
  full_adder_28 faddr4 ( .A(A[4]), .B(net_MxorB[4]), .C_in(carry[3]), .S(S[4]), 
        .C_out(carry[4]) );
  full_adder_27 faddr5 ( .A(A[5]), .B(net_MxorB[5]), .C_in(carry[4]), .S(S[5]), 
        .C_out(carry[5]) );
  full_adder_26 faddr6 ( .A(A[6]), .B(net_MxorB[6]), .C_in(carry[5]), .S(S[6]), 
        .C_out(carry[6]) );
  full_adder_25 faddr7 ( .A(A[7]), .B(net_MxorB[7]), .C_in(carry[6]), .S(S[7]), 
        .C_out(carry[7]) );
  full_adder_24 faddr8 ( .A(A[8]), .B(net_MxorB[8]), .C_in(carry[7]), .S(S[8]), 
        .C_out(carry[8]) );
  full_adder_23 faddr9 ( .A(A[9]), .B(net_MxorB[9]), .C_in(carry[8]), .S(S[9]), 
        .C_out(carry[9]) );
  full_adder_22 faddr10 ( .A(A[10]), .B(net_MxorB[10]), .C_in(carry[9]), .S(
        S[10]), .C_out(carry[10]) );
  full_adder_21 faddr11 ( .A(A[11]), .B(net_MxorB[11]), .C_in(carry[10]), .S(
        S[11]), .C_out(carry[11]) );
  full_adder_20 faddr12 ( .A(A[12]), .B(net_MxorB[12]), .C_in(carry[11]), .S(
        S[12]), .C_out(carry[12]) );
  full_adder_19 faddr13 ( .A(A[13]), .B(net_MxorB[13]), .C_in(carry[12]), .S(
        S[13]), .C_out(carry[13]) );
  full_adder_18 faddr14 ( .A(A[14]), .B(net_MxorB[14]), .C_in(carry[13]), .S(
        S[14]), .C_out(carry[14]) );
  full_adder_17 faddr15 ( .A(A[15]), .B(net_MxorB[15]), .C_in(carry[14]), .S(
        S[15]), .C_out(carry[15]) );
  full_adder_16 faddr16 ( .A(A[16]), .B(net_MxorB[16]), .C_in(carry[15]), .S(
        S[16]), .C_out(carry[16]) );
  full_adder_15 faddr17 ( .A(A[17]), .B(net_MxorB[17]), .C_in(carry[16]), .S(
        S[17]), .C_out(carry[17]) );
  full_adder_14 faddr18 ( .A(A[18]), .B(net_MxorB[18]), .C_in(carry[17]), .S(
        S[18]), .C_out(carry[18]) );
  full_adder_13 faddr19 ( .A(A[19]), .B(net_MxorB[19]), .C_in(carry[18]), .S(
        S[19]), .C_out(carry[19]) );
  full_adder_12 faddr20 ( .A(A[20]), .B(net_MxorB[20]), .C_in(carry[19]), .S(
        S[20]), .C_out(carry[20]) );
  full_adder_11 faddr21 ( .A(A[21]), .B(net_MxorB[21]), .C_in(carry[20]), .S(
        S[21]), .C_out(carry[21]) );
  full_adder_10 faddr22 ( .A(A[22]), .B(net_MxorB[22]), .C_in(carry[21]), .S(
        S[22]), .C_out(carry[22]) );
  full_adder_9 faddr23 ( .A(A[23]), .B(net_MxorB[23]), .C_in(carry[22]), .S(
        S[23]), .C_out(carry[23]) );
  full_adder_8 faddr24 ( .A(A[24]), .B(net_MxorB[24]), .C_in(carry[23]), .S(
        S[24]), .C_out(carry[24]) );
  full_adder_7 faddr25 ( .A(A[25]), .B(net_MxorB[25]), .C_in(carry[24]), .S(
        S[25]), .C_out(carry[25]) );
  full_adder_6 faddr26 ( .A(A[26]), .B(net_MxorB[26]), .C_in(carry[25]), .S(
        S[26]), .C_out(carry[26]) );
  full_adder_5 faddr27 ( .A(A[27]), .B(net_MxorB[27]), .C_in(carry[26]), .S(
        S[27]), .C_out(carry[27]) );
  full_adder_4 faddr28 ( .A(A[28]), .B(net_MxorB[28]), .C_in(carry[27]), .S(
        S[28]), .C_out(carry[28]) );
  full_adder_3 faddr29 ( .A(A[29]), .B(net_MxorB[29]), .C_in(carry[28]), .S(
        S[29]), .C_out(carry[29]) );
  full_adder_2 faddr30 ( .A(A[30]), .B(net_MxorB[30]), .C_in(carry[29]), .S(
        S[30]), .C_out(carry[30]) );
  full_adder_1 faddr31 ( .A(A[31]), .B(net_MxorB[31]), .C_in(carry[30]), .S(
        S[31]), .C_out(C) );
  GTECH_XOR2 U34 ( .A(M), .B(B[9]), .Z(net_MxorB[9]) );
  GTECH_XOR2 U35 ( .A(M), .B(B[8]), .Z(net_MxorB[8]) );
  GTECH_XOR2 U36 ( .A(M), .B(B[7]), .Z(net_MxorB[7]) );
  GTECH_XOR2 U37 ( .A(M), .B(B[6]), .Z(net_MxorB[6]) );
  GTECH_XOR2 U38 ( .A(M), .B(B[5]), .Z(net_MxorB[5]) );
  GTECH_XOR2 U39 ( .A(M), .B(B[4]), .Z(net_MxorB[4]) );
  GTECH_XOR2 U40 ( .A(M), .B(B[3]), .Z(net_MxorB[3]) );
  GTECH_XOR2 U41 ( .A(M), .B(B[31]), .Z(net_MxorB[31]) );
  GTECH_XOR2 U42 ( .A(M), .B(B[30]), .Z(net_MxorB[30]) );
  GTECH_XOR2 U43 ( .A(M), .B(B[2]), .Z(net_MxorB[2]) );
  GTECH_XOR2 U44 ( .A(M), .B(B[29]), .Z(net_MxorB[29]) );
  GTECH_XOR2 U45 ( .A(M), .B(B[28]), .Z(net_MxorB[28]) );
  GTECH_XOR2 U46 ( .A(M), .B(B[27]), .Z(net_MxorB[27]) );
  GTECH_XOR2 U47 ( .A(M), .B(B[26]), .Z(net_MxorB[26]) );
  GTECH_XOR2 U48 ( .A(M), .B(B[25]), .Z(net_MxorB[25]) );
  GTECH_XOR2 U49 ( .A(M), .B(B[24]), .Z(net_MxorB[24]) );
  GTECH_XOR2 U50 ( .A(M), .B(B[23]), .Z(net_MxorB[23]) );
  GTECH_XOR2 U51 ( .A(M), .B(B[22]), .Z(net_MxorB[22]) );
  GTECH_XOR2 U52 ( .A(M), .B(B[21]), .Z(net_MxorB[21]) );
  GTECH_XOR2 U53 ( .A(M), .B(B[20]), .Z(net_MxorB[20]) );
  GTECH_XOR2 U54 ( .A(M), .B(B[1]), .Z(net_MxorB[1]) );
  GTECH_XOR2 U55 ( .A(M), .B(B[19]), .Z(net_MxorB[19]) );
  GTECH_XOR2 U56 ( .A(M), .B(B[18]), .Z(net_MxorB[18]) );
  GTECH_XOR2 U57 ( .A(M), .B(B[17]), .Z(net_MxorB[17]) );
  GTECH_XOR2 U58 ( .A(M), .B(B[16]), .Z(net_MxorB[16]) );
  GTECH_XOR2 U59 ( .A(M), .B(B[15]), .Z(net_MxorB[15]) );
  GTECH_XOR2 U60 ( .A(M), .B(B[14]), .Z(net_MxorB[14]) );
  GTECH_XOR2 U61 ( .A(M), .B(B[13]), .Z(net_MxorB[13]) );
  GTECH_XOR2 U62 ( .A(M), .B(B[12]), .Z(net_MxorB[12]) );
  GTECH_XOR2 U63 ( .A(M), .B(B[11]), .Z(net_MxorB[11]) );
  GTECH_XOR2 U64 ( .A(M), .B(B[10]), .Z(net_MxorB[10]) );
  GTECH_XOR2 U65 ( .A(M), .B(B[0]), .Z(net_MxorB[0]) );
  GTECH_XOR2 U66 ( .A(carry[20]), .B(C), .Z(V) );
endmodule


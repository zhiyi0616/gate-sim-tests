/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Mon May 10 13:07:17 2021
/////////////////////////////////////////////////////////////


module per_uart_DW01_dec_0 ( A, SUM );
  input [15:0] A;
  output [15:0] SUM;
  wire   n1, n2, n3, n4, n5, n6, n7, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27;

  GTECH_NOT U1 ( .A(n23), .Z(n1) );
  GTECH_NOT U2 ( .A(n24), .Z(n2) );
  GTECH_NOT U3 ( .A(n25), .Z(n3) );
  GTECH_NOT U4 ( .A(n26), .Z(n4) );
  GTECH_NOT U5 ( .A(n27), .Z(n5) );
  GTECH_NOT U6 ( .A(n17), .Z(n6) );
  GTECH_NOT U7 ( .A(n19), .Z(n7) );
  GTECH_NOT U8 ( .A(A[0]), .Z(SUM[0]) );
  GTECH_NOT U9 ( .A(A[12]), .Z(n9) );
  GTECH_NOT U10 ( .A(A[9]), .Z(n10) );
  GTECH_NOT U11 ( .A(A[7]), .Z(n11) );
  GTECH_NOT U12 ( .A(A[5]), .Z(n12) );
  GTECH_NOT U13 ( .A(A[1]), .Z(n13) );
  GTECH_OAI21 U14 ( .A(n14), .B(n10), .C(n3), .Z(SUM[9]) );
  GTECH_AO21 U15 ( .A(n4), .B(A[8]), .C(n14), .Z(SUM[8]) );
  GTECH_OAI21 U16 ( .A(n15), .B(n11), .C(n4), .Z(SUM[7]) );
  GTECH_AO21 U17 ( .A(n5), .B(A[6]), .C(n15), .Z(SUM[6]) );
  GTECH_OAI21 U18 ( .A(n16), .B(n12), .C(n5), .Z(SUM[5]) );
  GTECH_AO21 U19 ( .A(n6), .B(A[4]), .C(n16), .Z(SUM[4]) );
  GTECH_AO21 U20 ( .A(n7), .B(A[3]), .C(n17), .Z(SUM[3]) );
  GTECH_AO21 U21 ( .A(n18), .B(A[2]), .C(n19), .Z(SUM[2]) );
  GTECH_OAI21 U22 ( .A(SUM[0]), .B(n13), .C(n18), .Z(SUM[1]) );
  GTECH_XOR2 U23 ( .A(A[15]), .B(n20), .Z(SUM[15]) );
  GTECH_AND_NOT U24 ( .A(n21), .B(A[14]), .Z(n20) );
  GTECH_XOR2 U25 ( .A(A[14]), .B(n21), .Z(SUM[14]) );
  GTECH_AO21 U26 ( .A(n1), .B(A[13]), .C(n21), .Z(SUM[13]) );
  GTECH_NOR2 U27 ( .A(n1), .B(A[13]), .Z(n21) );
  GTECH_OAI21 U28 ( .A(n22), .B(n9), .C(n1), .Z(SUM[12]) );
  GTECH_AND2 U29 ( .A(n22), .B(n9), .Z(n23) );
  GTECH_AO21 U30 ( .A(n2), .B(A[11]), .C(n22), .Z(SUM[11]) );
  GTECH_NOR2 U31 ( .A(n2), .B(A[11]), .Z(n22) );
  GTECH_AO21 U32 ( .A(n3), .B(A[10]), .C(n24), .Z(SUM[10]) );
  GTECH_NOR2 U33 ( .A(n3), .B(A[10]), .Z(n24) );
  GTECH_AND2 U34 ( .A(n14), .B(n10), .Z(n25) );
  GTECH_NOR2 U35 ( .A(n4), .B(A[8]), .Z(n14) );
  GTECH_AND2 U36 ( .A(n15), .B(n11), .Z(n26) );
  GTECH_NOR2 U37 ( .A(n5), .B(A[6]), .Z(n15) );
  GTECH_AND2 U38 ( .A(n16), .B(n12), .Z(n27) );
  GTECH_NOR2 U39 ( .A(n6), .B(A[4]), .Z(n16) );
  GTECH_NOR2 U40 ( .A(n7), .B(A[3]), .Z(n17) );
  GTECH_NOR2 U41 ( .A(A[2]), .B(n18), .Z(n19) );
  GTECH_NAND2 U42 ( .A(SUM[0]), .B(n13), .Z(n18) );
endmodule


module per_uart_DW01_inc_0 ( A, SUM );
  input [15:0] A;
  output [15:0] SUM;

  wire   [15:2] carry;

  GTECH_ADD_AB U1_1_1 ( .A(A[1]), .B(A[0]), .COUT(carry[2]), .S(SUM[1]) );
  GTECH_ADD_AB U1_1_2 ( .A(A[2]), .B(carry[2]), .COUT(carry[3]), .S(SUM[2]) );
  GTECH_ADD_AB U1_1_3 ( .A(A[3]), .B(carry[3]), .COUT(carry[4]), .S(SUM[3]) );
  GTECH_ADD_AB U1_1_4 ( .A(A[4]), .B(carry[4]), .COUT(carry[5]), .S(SUM[4]) );
  GTECH_ADD_AB U1_1_5 ( .A(A[5]), .B(carry[5]), .COUT(carry[6]), .S(SUM[5]) );
  GTECH_ADD_AB U1_1_6 ( .A(A[6]), .B(carry[6]), .COUT(carry[7]), .S(SUM[6]) );
  GTECH_ADD_AB U1_1_7 ( .A(A[7]), .B(carry[7]), .COUT(carry[8]), .S(SUM[7]) );
  GTECH_ADD_AB U1_1_8 ( .A(A[8]), .B(carry[8]), .COUT(carry[9]), .S(SUM[8]) );
  GTECH_ADD_AB U1_1_9 ( .A(A[9]), .B(carry[9]), .COUT(carry[10]), .S(SUM[9])
         );
  GTECH_ADD_AB U1_1_10 ( .A(A[10]), .B(carry[10]), .COUT(carry[11]), .S(
        SUM[10]) );
  GTECH_ADD_AB U1_1_11 ( .A(A[11]), .B(carry[11]), .COUT(carry[12]), .S(
        SUM[11]) );
  GTECH_ADD_AB U1_1_12 ( .A(A[12]), .B(carry[12]), .COUT(carry[13]), .S(
        SUM[12]) );
  GTECH_ADD_AB U1_1_13 ( .A(A[13]), .B(carry[13]), .COUT(carry[14]), .S(
        SUM[13]) );
  GTECH_ADD_AB U1_1_14 ( .A(A[14]), .B(carry[14]), .COUT(carry[15]), .S(
        SUM[14]) );
  GTECH_NOT U1 ( .A(A[0]), .Z(SUM[0]) );
  GTECH_XOR2 U2 ( .A(carry[15]), .B(A[15]), .Z(SUM[15]) );
endmodule


module per_uart_DW01_inc_1 ( A, SUM );
  input [15:0] A;
  output [15:0] SUM;

  wire   [15:2] carry;

  GTECH_ADD_AB U1_1_1 ( .A(A[1]), .B(A[0]), .COUT(carry[2]), .S(SUM[1]) );
  GTECH_ADD_AB U1_1_2 ( .A(A[2]), .B(carry[2]), .COUT(carry[3]), .S(SUM[2]) );
  GTECH_ADD_AB U1_1_3 ( .A(A[3]), .B(carry[3]), .COUT(carry[4]), .S(SUM[3]) );
  GTECH_ADD_AB U1_1_4 ( .A(A[4]), .B(carry[4]), .COUT(carry[5]), .S(SUM[4]) );
  GTECH_ADD_AB U1_1_5 ( .A(A[5]), .B(carry[5]), .COUT(carry[6]), .S(SUM[5]) );
  GTECH_ADD_AB U1_1_6 ( .A(A[6]), .B(carry[6]), .COUT(carry[7]), .S(SUM[6]) );
  GTECH_ADD_AB U1_1_7 ( .A(A[7]), .B(carry[7]), .COUT(carry[8]), .S(SUM[7]) );
  GTECH_ADD_AB U1_1_8 ( .A(A[8]), .B(carry[8]), .COUT(carry[9]), .S(SUM[8]) );
  GTECH_ADD_AB U1_1_9 ( .A(A[9]), .B(carry[9]), .COUT(carry[10]), .S(SUM[9])
         );
  GTECH_ADD_AB U1_1_10 ( .A(A[10]), .B(carry[10]), .COUT(carry[11]), .S(
        SUM[10]) );
  GTECH_ADD_AB U1_1_11 ( .A(A[11]), .B(carry[11]), .COUT(carry[12]), .S(
        SUM[11]) );
  GTECH_ADD_AB U1_1_12 ( .A(A[12]), .B(carry[12]), .COUT(carry[13]), .S(
        SUM[12]) );
  GTECH_ADD_AB U1_1_13 ( .A(A[13]), .B(carry[13]), .COUT(carry[14]), .S(
        SUM[13]) );
  GTECH_ADD_AB U1_1_14 ( .A(A[14]), .B(carry[14]), .COUT(carry[15]), .S(
        SUM[14]) );
  GTECH_NOT U1 ( .A(A[0]), .Z(SUM[0]) );
  GTECH_XOR2 U2 ( .A(carry[15]), .B(A[15]), .Z(SUM[15]) );
endmodule


module per_uart ( clk_i, reset_i, addr_i, wdata_i, rdata_o, size_i, rd_i, wr_i, 
        uart_rx_i, uart_tx_o );
  input [31:0] addr_i;
  input [31:0] wdata_i;
  output [31:0] rdata_o;
  input [1:0] size_i;
  input clk_i, reset_i, rd_i, wr_i, uart_rx_i;
  output uart_tx_o;
  wire   N39, N40, N41, N42, N43, N44, N45, N46, N47, N48, N49, N50, N51, N52,
         N53, N54, N125, N126, N127, N128, N129, N130, N131, N132, N133, N134,
         N135, N136, N137, N138, N139, N140, N159, N160, N161, N162, N163,
         N164, N165, N166, N167, N168, N169, N170, N171, N172, N173, N174,
         N232, N233, N234, N235, N236, N237, N238, N239, n113, n130, n131,
         n132, n133, n134, n135, n136, n137, n138, n139, n140, n141, n142,
         n143, n144, n145, n158, n159, n160, n161, n162, n163, n164, n165,
         n166, n167, n168, n169, n170, n171, n172, n173, n174, n175, n176,
         n177, n178, n179, n180, n181, n182, n183, n184, n185, n186, n187,
         n188, n189, n190, n191, n192, n193, n194, n195, n196, n197, n198,
         n199, n200, n201, n202, n203, n204, n205, n206, n207, n208, n209,
         n210, n211, n212, n213, n214, n215, n216, n217, n218, n219, n220,
         n221, n222, n223, n224, n225, n226, n227, n228, n229, n230, n231,
         n232, n233, n234, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13,
         n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27,
         n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41,
         n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55,
         n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69,
         n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81, n82, n83,
         n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97,
         n98, n99, n100, n101, n102, n103, n104, n105, n106, n107, n108, n109,
         n110, n111, n112, n114, n115, n116, n117, n118, n119, n120, n121,
         n122, n123, n124, n125, n126, n127, n128, n129, n146, n147, n148,
         n149, n150, n151, n152, n153, n154, n155, n156, n157, n235, n236,
         n237, n238, n239, n240, n241, n242, n243, n244, n245;
  wire   [3:0] tx_bit_cnt_r;
  wire   [15:0] tx_br_cnt_r;
  wire   [9:1] tx_shifter_r;
  wire   [8:0] rx_shifter_r;
  wire   [3:0] rx_bit_cnt_r;
  wire   [15:0] rx_br_cnt_r;
  wire   [7:0] rx_data_r;
  tri   clk_i;
  tri   reset_i;
  tri   [31:0] addr_i;
  tri   [31:0] wdata_i;
  tri   rd_i;
  tri   wr_i;
  assign rdata_o[8] = rdata_o[31];
  assign rdata_o[9] = rdata_o[31];
  assign rdata_o[10] = rdata_o[31];
  assign rdata_o[11] = rdata_o[31];
  assign rdata_o[12] = rdata_o[31];
  assign rdata_o[13] = rdata_o[31];
  assign rdata_o[14] = rdata_o[31];
  assign rdata_o[15] = rdata_o[31];
  assign rdata_o[16] = rdata_o[31];
  assign rdata_o[17] = rdata_o[31];
  assign rdata_o[18] = rdata_o[31];
  assign rdata_o[19] = rdata_o[31];
  assign rdata_o[20] = rdata_o[31];
  assign rdata_o[21] = rdata_o[31];
  assign rdata_o[22] = rdata_o[31];
  assign rdata_o[23] = rdata_o[31];
  assign rdata_o[24] = rdata_o[31];
  assign rdata_o[25] = rdata_o[31];
  assign rdata_o[26] = rdata_o[31];
  assign rdata_o[27] = rdata_o[31];
  assign rdata_o[28] = rdata_o[31];
  assign rdata_o[29] = rdata_o[31];
  assign rdata_o[30] = rdata_o[31];

  GTECH_FD1 br_r_reg_15_ ( .D(n234), .CP(clk_i), .QN(n145) );
  GTECH_FD1 br_r_reg_14_ ( .D(n233), .CP(clk_i), .QN(n144) );
  GTECH_FD1 br_r_reg_13_ ( .D(n232), .CP(clk_i), .QN(n143) );
  GTECH_FD1 br_r_reg_12_ ( .D(n231), .CP(clk_i), .QN(n142) );
  GTECH_FD1 br_r_reg_11_ ( .D(n230), .CP(clk_i), .QN(n141) );
  GTECH_FD1 br_r_reg_10_ ( .D(n229), .CP(clk_i), .QN(n140) );
  GTECH_FD1 br_r_reg_9_ ( .D(n228), .CP(clk_i), .QN(n139) );
  GTECH_FD1 br_r_reg_8_ ( .D(n227), .CP(clk_i), .QN(n138) );
  GTECH_FD1 br_r_reg_7_ ( .D(n226), .CP(clk_i), .QN(n137) );
  GTECH_FD1 br_r_reg_6_ ( .D(n225), .CP(clk_i), .QN(n136) );
  GTECH_FD1 br_r_reg_5_ ( .D(n224), .CP(clk_i), .QN(n135) );
  GTECH_FD1 br_r_reg_4_ ( .D(n223), .CP(clk_i), .QN(n134) );
  GTECH_FD1 br_r_reg_3_ ( .D(n222), .CP(clk_i), .QN(n133) );
  GTECH_FD1 br_r_reg_2_ ( .D(n221), .CP(clk_i), .QN(n132) );
  GTECH_FD1 br_r_reg_1_ ( .D(n220), .CP(clk_i), .QN(n131) );
  GTECH_FD1 br_r_reg_0_ ( .D(n219), .CP(clk_i), .QN(n113) );
  GTECH_FD1 tx_br_cnt_r_reg_0_ ( .D(n218), .CP(clk_i), .Q(tx_br_cnt_r[0]) );
  GTECH_FD1 tx_br_cnt_r_reg_15_ ( .D(n217), .CP(clk_i), .Q(tx_br_cnt_r[15]) );
  GTECH_FD1 tx_bit_cnt_r_reg_3_ ( .D(n216), .CP(clk_i), .Q(tx_bit_cnt_r[3]) );
  GTECH_FD1 tx_ready_r_reg ( .D(n212), .CP(clk_i), .Q(n236) );
  GTECH_FD1 tx_br_cnt_r_reg_1_ ( .D(n197), .CP(clk_i), .Q(tx_br_cnt_r[1]) );
  GTECH_FD1 tx_br_cnt_r_reg_2_ ( .D(n198), .CP(clk_i), .Q(tx_br_cnt_r[2]) );
  GTECH_FD1 tx_br_cnt_r_reg_3_ ( .D(n199), .CP(clk_i), .Q(tx_br_cnt_r[3]) );
  GTECH_FD1 tx_br_cnt_r_reg_4_ ( .D(n200), .CP(clk_i), .Q(tx_br_cnt_r[4]) );
  GTECH_FD1 tx_br_cnt_r_reg_5_ ( .D(n201), .CP(clk_i), .Q(tx_br_cnt_r[5]) );
  GTECH_FD1 tx_br_cnt_r_reg_6_ ( .D(n202), .CP(clk_i), .Q(tx_br_cnt_r[6]) );
  GTECH_FD1 tx_br_cnt_r_reg_7_ ( .D(n203), .CP(clk_i), .Q(tx_br_cnt_r[7]) );
  GTECH_FD1 tx_br_cnt_r_reg_8_ ( .D(n204), .CP(clk_i), .Q(tx_br_cnt_r[8]) );
  GTECH_FD1 tx_br_cnt_r_reg_9_ ( .D(n205), .CP(clk_i), .Q(tx_br_cnt_r[9]) );
  GTECH_FD1 tx_br_cnt_r_reg_10_ ( .D(n206), .CP(clk_i), .Q(tx_br_cnt_r[10]) );
  GTECH_FD1 tx_br_cnt_r_reg_11_ ( .D(n207), .CP(clk_i), .Q(tx_br_cnt_r[11]) );
  GTECH_FD1 tx_br_cnt_r_reg_12_ ( .D(n208), .CP(clk_i), .Q(tx_br_cnt_r[12]) );
  GTECH_FD1 tx_br_cnt_r_reg_13_ ( .D(n209), .CP(clk_i), .Q(tx_br_cnt_r[13]) );
  GTECH_FD1 tx_br_cnt_r_reg_14_ ( .D(n210), .CP(clk_i), .Q(tx_br_cnt_r[14]) );
  GTECH_FD1 tx_shifter_r_reg_9_ ( .D(n211), .CP(clk_i), .Q(tx_shifter_r[9]) );
  GTECH_FD1 tx_shifter_r_reg_8_ ( .D(n237), .CP(clk_i), .Q(tx_shifter_r[8]) );
  GTECH_FD1 tx_shifter_r_reg_7_ ( .D(n238), .CP(clk_i), .Q(tx_shifter_r[7]) );
  GTECH_FD1 tx_shifter_r_reg_6_ ( .D(n239), .CP(clk_i), .Q(tx_shifter_r[6]) );
  GTECH_FD1 tx_shifter_r_reg_5_ ( .D(n240), .CP(clk_i), .Q(tx_shifter_r[5]) );
  GTECH_FD1 tx_shifter_r_reg_4_ ( .D(n241), .CP(clk_i), .Q(tx_shifter_r[4]) );
  GTECH_FD1 tx_shifter_r_reg_3_ ( .D(n242), .CP(clk_i), .Q(tx_shifter_r[3]) );
  GTECH_FD1 tx_shifter_r_reg_2_ ( .D(n243), .CP(clk_i), .Q(tx_shifter_r[2]) );
  GTECH_FD1 tx_shifter_r_reg_1_ ( .D(n244), .CP(clk_i), .Q(tx_shifter_r[1]) );
  GTECH_FD1 tx_shifter_r_reg_0_ ( .D(n196), .CP(clk_i), .Q(uart_tx_o), .QN(
        n130) );
  GTECH_FD1 tx_bit_cnt_r_reg_1_ ( .D(n214), .CP(clk_i), .Q(tx_bit_cnt_r[1]) );
  GTECH_FD1 tx_bit_cnt_r_reg_0_ ( .D(n215), .CP(clk_i), .Q(tx_bit_cnt_r[0]) );
  GTECH_FD1 tx_bit_cnt_r_reg_2_ ( .D(n213), .CP(clk_i), .Q(tx_bit_cnt_r[2]) );
  GTECH_FD1 rx_br_cnt_r_reg_0_ ( .D(n195), .CP(clk_i), .Q(rx_br_cnt_r[0]) );
  GTECH_FD1 rx_br_cnt_r_reg_15_ ( .D(n245), .CP(clk_i), .Q(rx_br_cnt_r[15]) );
  GTECH_FD1 rx_br_cnt_r_reg_14_ ( .D(n175), .CP(clk_i), .Q(rx_br_cnt_r[14]) );
  GTECH_FD1 rx_br_cnt_r_reg_13_ ( .D(n176), .CP(clk_i), .Q(rx_br_cnt_r[13]) );
  GTECH_FD1 rx_shifter_r_reg_8_ ( .D(n194), .CP(clk_i), .Q(rx_shifter_r[8]) );
  GTECH_FD1 rx_done_r_reg ( .D(n189), .CP(clk_i), .QN(n157) );
  GTECH_FD1 rx_bit_cnt_r_reg_3_ ( .D(n193), .CP(clk_i), .Q(rx_bit_cnt_r[3]) );
  GTECH_FD1 rx_br_cnt_r_reg_12_ ( .D(n177), .CP(clk_i), .Q(rx_br_cnt_r[12]) );
  GTECH_FD1 rx_br_cnt_r_reg_11_ ( .D(n178), .CP(clk_i), .Q(rx_br_cnt_r[11]) );
  GTECH_FD1 rx_br_cnt_r_reg_10_ ( .D(n179), .CP(clk_i), .Q(rx_br_cnt_r[10]) );
  GTECH_FD1 rx_br_cnt_r_reg_9_ ( .D(n180), .CP(clk_i), .Q(rx_br_cnt_r[9]) );
  GTECH_FD1 rx_br_cnt_r_reg_8_ ( .D(n181), .CP(clk_i), .Q(rx_br_cnt_r[8]) );
  GTECH_FD1 rx_br_cnt_r_reg_7_ ( .D(n182), .CP(clk_i), .Q(rx_br_cnt_r[7]) );
  GTECH_FD1 rx_br_cnt_r_reg_6_ ( .D(n183), .CP(clk_i), .Q(rx_br_cnt_r[6]) );
  GTECH_FD1 rx_br_cnt_r_reg_5_ ( .D(n184), .CP(clk_i), .Q(rx_br_cnt_r[5]) );
  GTECH_FD1 rx_br_cnt_r_reg_4_ ( .D(n185), .CP(clk_i), .Q(rx_br_cnt_r[4]) );
  GTECH_FD1 rx_br_cnt_r_reg_3_ ( .D(n186), .CP(clk_i), .Q(rx_br_cnt_r[3]) );
  GTECH_FD1 rx_br_cnt_r_reg_2_ ( .D(n187), .CP(clk_i), .Q(rx_br_cnt_r[2]) );
  GTECH_FD1 rx_br_cnt_r_reg_1_ ( .D(n188), .CP(clk_i), .Q(rx_br_cnt_r[1]) );
  GTECH_FD1 rx_bit_cnt_r_reg_1_ ( .D(n191), .CP(clk_i), .Q(rx_bit_cnt_r[1]) );
  GTECH_FD1 rx_bit_cnt_r_reg_2_ ( .D(n190), .CP(clk_i), .Q(rx_bit_cnt_r[2]) );
  GTECH_FD1 rx_bit_cnt_r_reg_0_ ( .D(n192), .CP(clk_i), .Q(rx_bit_cnt_r[0]) );
  GTECH_FD1 rx_shifter_r_reg_7_ ( .D(n160), .CP(clk_i), .Q(rx_shifter_r[7]) );
  GTECH_FD1 rx_shifter_r_reg_6_ ( .D(n162), .CP(clk_i), .Q(rx_shifter_r[6]) );
  GTECH_FD1 rx_shifter_r_reg_5_ ( .D(n164), .CP(clk_i), .Q(rx_shifter_r[5]) );
  GTECH_FD1 rx_shifter_r_reg_4_ ( .D(n166), .CP(clk_i), .Q(rx_shifter_r[4]) );
  GTECH_FD1 rx_shifter_r_reg_3_ ( .D(n168), .CP(clk_i), .Q(rx_shifter_r[3]) );
  GTECH_FD1 rx_shifter_r_reg_2_ ( .D(n170), .CP(clk_i), .Q(rx_shifter_r[2]) );
  GTECH_FD1 rx_shifter_r_reg_1_ ( .D(n172), .CP(clk_i), .Q(rx_shifter_r[1]) );
  GTECH_FD1 rx_shifter_r_reg_0_ ( .D(n174), .CP(clk_i), .Q(rx_shifter_r[0]) );
  GTECH_FD1 rx_data_r_reg_0_ ( .D(n173), .CP(clk_i), .Q(rx_data_r[0]) );
  GTECH_FD1 rx_data_r_reg_1_ ( .D(n171), .CP(clk_i), .Q(rx_data_r[1]) );
  GTECH_FD1 rx_data_r_reg_2_ ( .D(n169), .CP(clk_i), .Q(rx_data_r[2]) );
  GTECH_FD1 rx_data_r_reg_3_ ( .D(n167), .CP(clk_i), .Q(rx_data_r[3]) );
  GTECH_FD1 rx_data_r_reg_4_ ( .D(n165), .CP(clk_i), .Q(rx_data_r[4]) );
  GTECH_FD1 rx_data_r_reg_5_ ( .D(n163), .CP(clk_i), .Q(rx_data_r[5]) );
  GTECH_FD1 rx_data_r_reg_6_ ( .D(n161), .CP(clk_i), .Q(rx_data_r[6]) );
  GTECH_FD1 rx_data_r_reg_7_ ( .D(n159), .CP(clk_i), .Q(rx_data_r[7]) );
  GTECH_FD1 rx_ready_r_reg ( .D(n158), .CP(clk_i), .QN(n235) );
  GTECH_FD1 reg_data_r_reg_7_ ( .D(N239), .CP(clk_i), .Q(rdata_o[7]) );
  GTECH_FD1 reg_data_r_reg_6_ ( .D(N238), .CP(clk_i), .Q(rdata_o[6]) );
  GTECH_FD1 reg_data_r_reg_5_ ( .D(N237), .CP(clk_i), .Q(rdata_o[5]) );
  GTECH_FD1 reg_data_r_reg_4_ ( .D(N236), .CP(clk_i), .Q(rdata_o[4]) );
  GTECH_FD1 reg_data_r_reg_3_ ( .D(N235), .CP(clk_i), .Q(rdata_o[3]) );
  GTECH_FD1 reg_data_r_reg_2_ ( .D(N234), .CP(clk_i), .Q(rdata_o[2]) );
  GTECH_FD1 reg_data_r_reg_1_ ( .D(N233), .CP(clk_i), .Q(rdata_o[1]) );
  GTECH_FD1 reg_data_r_reg_0_ ( .D(N232), .CP(clk_i), .Q(rdata_o[0]) );
  per_uart_DW01_dec_0 sub_145 ( .A(rx_br_cnt_r), .SUM({N174, N173, N172, N171, 
        N170, N169, N168, N167, N166, N165, N164, N163, N162, N161, N160, N159}) );
  per_uart_DW01_inc_0 add_135 ( .A(rx_br_cnt_r), .SUM({N140, N139, N138, N137, 
        N136, N135, N134, N133, N132, N131, N130, N129, N128, N127, N126, N125}) );
  per_uart_DW01_inc_1 add_102 ( .A(tx_br_cnt_r), .SUM({N54, N53, N52, N51, N50, 
        N49, N48, N47, N46, N45, N44, N43, N42, N41, N40, N39}) );
  GTECH_ZERO U3 ( .Z(rdata_o[31]) );
  GTECH_NOT U4 ( .A(n2), .Z(n237) );
  GTECH_AOI222 U5 ( .A(wdata_i[7]), .B(n3), .C(tx_shifter_r[9]), .D(n4), .E(
        tx_shifter_r[8]), .F(n5), .Z(n2) );
  GTECH_NOT U6 ( .A(n6), .Z(n238) );
  GTECH_AOI222 U7 ( .A(wdata_i[6]), .B(n3), .C(tx_shifter_r[8]), .D(n4), .E(
        tx_shifter_r[7]), .F(n5), .Z(n6) );
  GTECH_NOT U8 ( .A(n7), .Z(n239) );
  GTECH_AOI222 U9 ( .A(wdata_i[5]), .B(n3), .C(tx_shifter_r[7]), .D(n4), .E(
        tx_shifter_r[6]), .F(n5), .Z(n7) );
  GTECH_NOT U10 ( .A(n8), .Z(n240) );
  GTECH_AOI222 U11 ( .A(wdata_i[4]), .B(n3), .C(tx_shifter_r[6]), .D(n4), .E(
        tx_shifter_r[5]), .F(n5), .Z(n8) );
  GTECH_NOT U12 ( .A(n9), .Z(n241) );
  GTECH_AOI222 U13 ( .A(wdata_i[3]), .B(n3), .C(tx_shifter_r[5]), .D(n4), .E(
        tx_shifter_r[4]), .F(n5), .Z(n9) );
  GTECH_NOT U14 ( .A(n10), .Z(n242) );
  GTECH_AOI222 U15 ( .A(wdata_i[2]), .B(n3), .C(tx_shifter_r[4]), .D(n4), .E(
        tx_shifter_r[3]), .F(n5), .Z(n10) );
  GTECH_NOT U16 ( .A(n11), .Z(n243) );
  GTECH_AOI222 U17 ( .A(wdata_i[1]), .B(n3), .C(tx_shifter_r[3]), .D(n4), .E(
        tx_shifter_r[2]), .F(n5), .Z(n11) );
  GTECH_NOT U18 ( .A(n12), .Z(n244) );
  GTECH_AOI222 U19 ( .A(wdata_i[0]), .B(n3), .C(tx_shifter_r[2]), .D(n4), .E(
        tx_shifter_r[1]), .F(n5), .Z(n12) );
  GTECH_NOT U20 ( .A(n13), .Z(n245) );
  GTECH_AOI222 U21 ( .A(N140), .B(n14), .C(N174), .D(n15), .E(rx_br_cnt_r[15]), 
        .F(n16), .Z(n13) );
  GTECH_OAI2N2 U22 ( .A(n145), .B(n17), .C(wdata_i[15]), .D(n18), .Z(n234) );
  GTECH_OAI2N2 U23 ( .A(n144), .B(n17), .C(wdata_i[14]), .D(n18), .Z(n233) );
  GTECH_OAI2N2 U24 ( .A(n143), .B(n17), .C(wdata_i[13]), .D(n18), .Z(n232) );
  GTECH_OAI2N2 U25 ( .A(n142), .B(n17), .C(wdata_i[12]), .D(n18), .Z(n231) );
  GTECH_OAI2N2 U26 ( .A(n141), .B(n17), .C(wdata_i[11]), .D(n18), .Z(n230) );
  GTECH_OAI2N2 U27 ( .A(n140), .B(n17), .C(wdata_i[10]), .D(n18), .Z(n229) );
  GTECH_OAI2N2 U28 ( .A(n139), .B(n17), .C(wdata_i[9]), .D(n18), .Z(n228) );
  GTECH_OAI2N2 U29 ( .A(n138), .B(n17), .C(wdata_i[8]), .D(n18), .Z(n227) );
  GTECH_OAI2N2 U30 ( .A(n137), .B(n17), .C(wdata_i[7]), .D(n18), .Z(n226) );
  GTECH_OAI2N2 U31 ( .A(n136), .B(n17), .C(wdata_i[6]), .D(n18), .Z(n225) );
  GTECH_OAI2N2 U32 ( .A(n135), .B(n17), .C(wdata_i[5]), .D(n18), .Z(n224) );
  GTECH_OAI2N2 U33 ( .A(n134), .B(n17), .C(wdata_i[4]), .D(n18), .Z(n223) );
  GTECH_OAI2N2 U34 ( .A(n133), .B(n17), .C(wdata_i[3]), .D(n18), .Z(n222) );
  GTECH_OAI2N2 U35 ( .A(n132), .B(n17), .C(wdata_i[2]), .D(n18), .Z(n221) );
  GTECH_OAI2N2 U36 ( .A(n131), .B(n17), .C(wdata_i[1]), .D(n18), .Z(n220) );
  GTECH_OAI2N2 U37 ( .A(n113), .B(n17), .C(wdata_i[0]), .D(n18), .Z(n219) );
  GTECH_OR_NOT U38 ( .A(n18), .B(n19), .Z(n17) );
  GTECH_NOR8 U39 ( .A(addr_i[1]), .B(addr_i[3]), .C(addr_i[0]), .D(n20), .E(
        n21), .F(addr_i[4]), .G(addr_i[5]), .H(n22), .Z(n18) );
  GTECH_OR3 U40 ( .A(addr_i[7]), .B(reset_i), .C(addr_i[6]), .Z(n22) );
  GTECH_NOT U41 ( .A(wr_i), .Z(n21) );
  GTECH_NOT U42 ( .A(addr_i[2]), .Z(n20) );
  GTECH_AO22 U43 ( .A(N39), .B(n23), .C(tx_br_cnt_r[0]), .D(n24), .Z(n218) );
  GTECH_AO22 U44 ( .A(N54), .B(n23), .C(tx_br_cnt_r[15]), .D(n24), .Z(n217) );
  GTECH_AO21 U45 ( .A(n25), .B(tx_bit_cnt_r[3]), .C(n3), .Z(n216) );
  GTECH_AO21 U46 ( .A(n26), .B(tx_bit_cnt_r[2]), .C(n27), .Z(n25) );
  GTECH_AO21 U47 ( .A(n5), .B(tx_bit_cnt_r[0]), .C(n28), .Z(n215) );
  GTECH_NAND2 U48 ( .A(n29), .B(n30), .Z(n214) );
  GTECH_MUXI2 U49 ( .A(n28), .B(n31), .S(tx_bit_cnt_r[1]), .Z(n30) );
  GTECH_MUX2 U50 ( .A(n32), .B(n27), .S(tx_bit_cnt_r[2]), .Z(n213) );
  GTECH_AO21 U51 ( .A(n26), .B(tx_bit_cnt_r[1]), .C(n31), .Z(n27) );
  GTECH_AO21 U52 ( .A(n26), .B(tx_bit_cnt_r[0]), .C(n5), .Z(n31) );
  GTECH_AND_NOT U53 ( .A(n28), .B(tx_bit_cnt_r[1]), .Z(n32) );
  GTECH_AND_NOT U54 ( .A(n4), .B(tx_bit_cnt_r[0]), .Z(n28) );
  GTECH_NAND2 U55 ( .A(n19), .B(n33), .Z(n212) );
  GTECH_MUXI2 U56 ( .A(n34), .B(n29), .S(n236), .Z(n33) );
  GTECH_AO21 U57 ( .A(tx_shifter_r[9]), .B(n5), .C(n35), .Z(n211) );
  GTECH_OR_NOT U58 ( .A(n4), .B(n29), .Z(n35) );
  GTECH_AO22 U59 ( .A(N53), .B(n23), .C(tx_br_cnt_r[14]), .D(n24), .Z(n210) );
  GTECH_AO22 U60 ( .A(N52), .B(n23), .C(tx_br_cnt_r[13]), .D(n24), .Z(n209) );
  GTECH_AO22 U61 ( .A(N51), .B(n23), .C(tx_br_cnt_r[12]), .D(n24), .Z(n208) );
  GTECH_AO22 U62 ( .A(N50), .B(n23), .C(tx_br_cnt_r[11]), .D(n24), .Z(n207) );
  GTECH_AO22 U63 ( .A(N49), .B(n23), .C(tx_br_cnt_r[10]), .D(n24), .Z(n206) );
  GTECH_AO22 U64 ( .A(N48), .B(n23), .C(tx_br_cnt_r[9]), .D(n24), .Z(n205) );
  GTECH_AO22 U65 ( .A(N47), .B(n23), .C(tx_br_cnt_r[8]), .D(n24), .Z(n204) );
  GTECH_AO22 U66 ( .A(N46), .B(n23), .C(tx_br_cnt_r[7]), .D(n24), .Z(n203) );
  GTECH_AO22 U67 ( .A(N45), .B(n23), .C(tx_br_cnt_r[6]), .D(n24), .Z(n202) );
  GTECH_AO22 U68 ( .A(N44), .B(n23), .C(tx_br_cnt_r[5]), .D(n24), .Z(n201) );
  GTECH_AO22 U69 ( .A(N43), .B(n23), .C(tx_br_cnt_r[4]), .D(n24), .Z(n200) );
  GTECH_AO22 U70 ( .A(N42), .B(n23), .C(tx_br_cnt_r[3]), .D(n24), .Z(n199) );
  GTECH_AO22 U71 ( .A(N41), .B(n23), .C(tx_br_cnt_r[2]), .D(n24), .Z(n198) );
  GTECH_AO22 U72 ( .A(N40), .B(n23), .C(tx_br_cnt_r[1]), .D(n24), .Z(n197) );
  GTECH_AND2 U73 ( .A(n36), .B(n26), .Z(n23) );
  GTECH_OAI21 U74 ( .A(n37), .B(n130), .C(n38), .Z(n196) );
  GTECH_AOI21 U75 ( .A(n4), .B(tx_shifter_r[1]), .C(reset_i), .Z(n38) );
  GTECH_AND_NOT U76 ( .A(n26), .B(n5), .Z(n4) );
  GTECH_NOT U77 ( .A(n5), .Z(n37) );
  GTECH_AND2 U78 ( .A(n39), .B(n29), .Z(n5) );
  GTECH_NOT U79 ( .A(n3), .Z(n29) );
  GTECH_AND4 U80 ( .A(wr_i), .B(n236), .C(n40), .D(n34), .Z(n3) );
  GTECH_AO21 U81 ( .A(n19), .B(n36), .C(n24), .Z(n39) );
  GTECH_AND_NOT U82 ( .A(n19), .B(n26), .Z(n24) );
  GTECH_AND_NOT U83 ( .A(n19), .B(n34), .Z(n26) );
  GTECH_NOR4 U84 ( .A(tx_bit_cnt_r[0]), .B(tx_bit_cnt_r[1]), .C(
        tx_bit_cnt_r[2]), .D(tx_bit_cnt_r[3]), .Z(n34) );
  GTECH_NAND2 U85 ( .A(n41), .B(n42), .Z(n36) );
  GTECH_AND8 U86 ( .A(n43), .B(n44), .C(n45), .D(n46), .E(n47), .F(n48), .G(
        n49), .H(n50), .Z(n42) );
  GTECH_XOR2 U87 ( .A(tx_br_cnt_r[14]), .B(n144), .Z(n50) );
  GTECH_XOR2 U88 ( .A(tx_br_cnt_r[13]), .B(n143), .Z(n49) );
  GTECH_XOR2 U89 ( .A(tx_br_cnt_r[12]), .B(n142), .Z(n48) );
  GTECH_XOR2 U90 ( .A(tx_br_cnt_r[11]), .B(n141), .Z(n47) );
  GTECH_XOR2 U91 ( .A(tx_br_cnt_r[10]), .B(n140), .Z(n46) );
  GTECH_XOR2 U92 ( .A(tx_br_cnt_r[9]), .B(n139), .Z(n45) );
  GTECH_XOR2 U93 ( .A(tx_br_cnt_r[8]), .B(n138), .Z(n44) );
  GTECH_XOR2 U94 ( .A(tx_br_cnt_r[7]), .B(n137), .Z(n43) );
  GTECH_AND8 U95 ( .A(n51), .B(n52), .C(n53), .D(n54), .E(n55), .F(n56), .G(
        n57), .H(n58), .Z(n41) );
  GTECH_XOR2 U96 ( .A(tx_br_cnt_r[6]), .B(n136), .Z(n58) );
  GTECH_XOR2 U97 ( .A(tx_br_cnt_r[5]), .B(n135), .Z(n57) );
  GTECH_XOR2 U98 ( .A(tx_br_cnt_r[4]), .B(n134), .Z(n56) );
  GTECH_XOR2 U99 ( .A(tx_br_cnt_r[3]), .B(n133), .Z(n55) );
  GTECH_XOR2 U100 ( .A(tx_br_cnt_r[2]), .B(n132), .Z(n54) );
  GTECH_XOR2 U101 ( .A(tx_br_cnt_r[15]), .B(n145), .Z(n53) );
  GTECH_XOR2 U102 ( .A(tx_br_cnt_r[0]), .B(n113), .Z(n52) );
  GTECH_XOR2 U103 ( .A(tx_br_cnt_r[1]), .B(n131), .Z(n51) );
  GTECH_NAND2 U104 ( .A(n59), .B(n60), .Z(n195) );
  GTECH_AOI21 U105 ( .A(n16), .B(rx_br_cnt_r[0]), .C(n61), .Z(n60) );
  GTECH_AO21 U106 ( .A(N159), .B(n15), .C(reset_i), .Z(n61) );
  GTECH_AOI2N2 U107 ( .A(N125), .B(n14), .C(n131), .D(n62), .Z(n59) );
  GTECH_AO22 U108 ( .A(uart_rx_i), .B(n63), .C(rx_shifter_r[8]), .D(n64), .Z(
        n194) );
  GTECH_AO22 U109 ( .A(n65), .B(rx_bit_cnt_r[3]), .C(n66), .D(n67), .Z(n193)
         );
  GTECH_AO21 U110 ( .A(n68), .B(rx_bit_cnt_r[2]), .C(n69), .Z(n65) );
  GTECH_NOT U111 ( .A(n70), .Z(n69) );
  GTECH_OR_NOT U112 ( .A(n71), .B(n72), .Z(n192) );
  GTECH_MUX2 U113 ( .A(n73), .B(n74), .S(n66), .Z(n72) );
  GTECH_MUX2 U114 ( .A(n71), .B(n75), .S(rx_bit_cnt_r[1]), .Z(n191) );
  GTECH_MUXI2 U115 ( .A(n76), .B(n70), .S(rx_bit_cnt_r[2]), .Z(n190) );
  GTECH_AOI21 U116 ( .A(n68), .B(rx_bit_cnt_r[1]), .C(n75), .Z(n70) );
  GTECH_OAI21 U117 ( .A(n77), .B(n73), .C(n66), .Z(n75) );
  GTECH_OR_NOT U118 ( .A(rx_bit_cnt_r[1]), .B(n71), .Z(n76) );
  GTECH_AND3 U119 ( .A(n66), .B(n73), .C(n68), .Z(n71) );
  GTECH_NOT U120 ( .A(rx_bit_cnt_r[0]), .Z(n73) );
  GTECH_AO21 U121 ( .A(n67), .B(n78), .C(n79), .Z(n66) );
  GTECH_OR2 U122 ( .A(n80), .B(reset_i), .Z(n79) );
  GTECH_AO21 U123 ( .A(n81), .B(n68), .C(n82), .Z(n189) );
  GTECH_NAND2 U124 ( .A(n83), .B(n84), .Z(n188) );
  GTECH_AOI22 U125 ( .A(N160), .B(n15), .C(n16), .D(rx_br_cnt_r[1]), .Z(n84)
         );
  GTECH_AOI2N2 U126 ( .A(N126), .B(n14), .C(n132), .D(n62), .Z(n83) );
  GTECH_NAND2 U127 ( .A(n85), .B(n86), .Z(n187) );
  GTECH_AOI22 U128 ( .A(N161), .B(n15), .C(n16), .D(rx_br_cnt_r[2]), .Z(n86)
         );
  GTECH_AOI2N2 U129 ( .A(N127), .B(n14), .C(n133), .D(n62), .Z(n85) );
  GTECH_NAND2 U130 ( .A(n87), .B(n88), .Z(n186) );
  GTECH_AOI22 U131 ( .A(N162), .B(n15), .C(n16), .D(rx_br_cnt_r[3]), .Z(n88)
         );
  GTECH_AOI2N2 U132 ( .A(N128), .B(n14), .C(n134), .D(n62), .Z(n87) );
  GTECH_NAND2 U133 ( .A(n89), .B(n90), .Z(n185) );
  GTECH_AOI22 U134 ( .A(N163), .B(n15), .C(n16), .D(rx_br_cnt_r[4]), .Z(n90)
         );
  GTECH_AOI2N2 U135 ( .A(N129), .B(n14), .C(n135), .D(n62), .Z(n89) );
  GTECH_NAND2 U136 ( .A(n91), .B(n92), .Z(n184) );
  GTECH_AOI22 U137 ( .A(N164), .B(n15), .C(n16), .D(rx_br_cnt_r[5]), .Z(n92)
         );
  GTECH_AOI2N2 U138 ( .A(N130), .B(n14), .C(n136), .D(n62), .Z(n91) );
  GTECH_NAND2 U139 ( .A(n93), .B(n94), .Z(n183) );
  GTECH_AOI22 U140 ( .A(N165), .B(n15), .C(n16), .D(rx_br_cnt_r[6]), .Z(n94)
         );
  GTECH_AOI2N2 U141 ( .A(N131), .B(n14), .C(n137), .D(n62), .Z(n93) );
  GTECH_NAND2 U142 ( .A(n95), .B(n96), .Z(n182) );
  GTECH_AOI22 U143 ( .A(N166), .B(n15), .C(n16), .D(rx_br_cnt_r[7]), .Z(n96)
         );
  GTECH_AOI2N2 U144 ( .A(N132), .B(n14), .C(n138), .D(n62), .Z(n95) );
  GTECH_NAND2 U145 ( .A(n97), .B(n98), .Z(n181) );
  GTECH_AOI22 U146 ( .A(N167), .B(n15), .C(n16), .D(rx_br_cnt_r[8]), .Z(n98)
         );
  GTECH_AOI2N2 U147 ( .A(N133), .B(n14), .C(n139), .D(n62), .Z(n97) );
  GTECH_NAND2 U148 ( .A(n99), .B(n100), .Z(n180) );
  GTECH_AOI22 U149 ( .A(N168), .B(n15), .C(n16), .D(rx_br_cnt_r[9]), .Z(n100)
         );
  GTECH_AOI2N2 U150 ( .A(N134), .B(n14), .C(n140), .D(n62), .Z(n99) );
  GTECH_NAND2 U151 ( .A(n101), .B(n102), .Z(n179) );
  GTECH_AOI22 U152 ( .A(N169), .B(n15), .C(n16), .D(rx_br_cnt_r[10]), .Z(n102)
         );
  GTECH_AOI2N2 U153 ( .A(N135), .B(n14), .C(n141), .D(n62), .Z(n101) );
  GTECH_NAND2 U154 ( .A(n103), .B(n104), .Z(n178) );
  GTECH_AOI22 U155 ( .A(N170), .B(n15), .C(n16), .D(rx_br_cnt_r[11]), .Z(n104)
         );
  GTECH_AOI2N2 U156 ( .A(N136), .B(n14), .C(n142), .D(n62), .Z(n103) );
  GTECH_NAND2 U157 ( .A(n105), .B(n106), .Z(n177) );
  GTECH_AOI22 U158 ( .A(N171), .B(n15), .C(n16), .D(rx_br_cnt_r[12]), .Z(n106)
         );
  GTECH_AOI2N2 U159 ( .A(N137), .B(n14), .C(n143), .D(n62), .Z(n105) );
  GTECH_NAND2 U160 ( .A(n107), .B(n108), .Z(n176) );
  GTECH_AOI22 U161 ( .A(N172), .B(n15), .C(n16), .D(rx_br_cnt_r[13]), .Z(n108)
         );
  GTECH_AOI2N2 U162 ( .A(N138), .B(n14), .C(n144), .D(n62), .Z(n107) );
  GTECH_NAND2 U163 ( .A(n109), .B(n110), .Z(n175) );
  GTECH_AOI22 U164 ( .A(N173), .B(n15), .C(n16), .D(rx_br_cnt_r[14]), .Z(n110)
         );
  GTECH_NOR2 U165 ( .A(n74), .B(n16), .Z(n15) );
  GTECH_AND3 U166 ( .A(n77), .B(n19), .C(n111), .Z(n16) );
  GTECH_OA21 U167 ( .A(n78), .B(n74), .C(n62), .Z(n111) );
  GTECH_AND2 U168 ( .A(n112), .B(n114), .Z(n78) );
  GTECH_NOR8 U169 ( .A(rx_br_cnt_r[2]), .B(rx_br_cnt_r[3]), .C(rx_br_cnt_r[4]), 
        .D(rx_br_cnt_r[5]), .E(rx_br_cnt_r[6]), .F(rx_br_cnt_r[7]), .G(
        rx_br_cnt_r[8]), .H(rx_br_cnt_r[9]), .Z(n114) );
  GTECH_NOR8 U170 ( .A(rx_br_cnt_r[0]), .B(rx_br_cnt_r[10]), .C(
        rx_br_cnt_r[11]), .D(rx_br_cnt_r[12]), .E(rx_br_cnt_r[13]), .F(
        rx_br_cnt_r[14]), .G(rx_br_cnt_r[15]), .H(rx_br_cnt_r[1]), .Z(n112) );
  GTECH_NOT U171 ( .A(n67), .Z(n74) );
  GTECH_NOR3 U172 ( .A(rx_shifter_r[8]), .B(uart_rx_i), .C(n115), .Z(n67) );
  GTECH_AOI2N2 U173 ( .A(N139), .B(n14), .C(n145), .D(n62), .Z(n109) );
  GTECH_OR3 U174 ( .A(n115), .B(rx_shifter_r[8]), .C(n116), .Z(n62) );
  GTECH_NOT U175 ( .A(uart_rx_i), .Z(n116) );
  GTECH_NOR2 U176 ( .A(n77), .B(n117), .Z(n14) );
  GTECH_AO22 U177 ( .A(n63), .B(rx_shifter_r[1]), .C(n64), .D(rx_shifter_r[0]), 
        .Z(n174) );
  GTECH_MUX2 U178 ( .A(rx_data_r[0]), .B(rx_shifter_r[0]), .S(n82), .Z(n173)
         );
  GTECH_AO22 U179 ( .A(n63), .B(rx_shifter_r[2]), .C(n64), .D(rx_shifter_r[1]), 
        .Z(n172) );
  GTECH_MUX2 U180 ( .A(rx_data_r[1]), .B(rx_shifter_r[1]), .S(n82), .Z(n171)
         );
  GTECH_AO22 U181 ( .A(n63), .B(rx_shifter_r[3]), .C(n64), .D(rx_shifter_r[2]), 
        .Z(n170) );
  GTECH_MUX2 U182 ( .A(rx_data_r[2]), .B(rx_shifter_r[2]), .S(n82), .Z(n169)
         );
  GTECH_AO22 U183 ( .A(n63), .B(rx_shifter_r[4]), .C(n64), .D(rx_shifter_r[3]), 
        .Z(n168) );
  GTECH_MUX2 U184 ( .A(rx_data_r[3]), .B(rx_shifter_r[3]), .S(n82), .Z(n167)
         );
  GTECH_AO22 U185 ( .A(n63), .B(rx_shifter_r[5]), .C(n64), .D(rx_shifter_r[4]), 
        .Z(n166) );
  GTECH_MUX2 U186 ( .A(rx_data_r[4]), .B(rx_shifter_r[4]), .S(n82), .Z(n165)
         );
  GTECH_AO22 U187 ( .A(n63), .B(rx_shifter_r[6]), .C(n64), .D(rx_shifter_r[5]), 
        .Z(n164) );
  GTECH_MUX2 U188 ( .A(rx_data_r[5]), .B(rx_shifter_r[5]), .S(n82), .Z(n163)
         );
  GTECH_AO22 U189 ( .A(n64), .B(rx_shifter_r[6]), .C(n63), .D(rx_shifter_r[7]), 
        .Z(n162) );
  GTECH_MUX2 U190 ( .A(rx_data_r[6]), .B(rx_shifter_r[6]), .S(n82), .Z(n161)
         );
  GTECH_AO22 U191 ( .A(rx_shifter_r[8]), .B(n63), .C(n64), .D(rx_shifter_r[7]), 
        .Z(n160) );
  GTECH_AND_NOT U192 ( .A(n68), .B(n64), .Z(n63) );
  GTECH_NOR3 U193 ( .A(n82), .B(reset_i), .C(n80), .Z(n64) );
  GTECH_AND_NOT U194 ( .A(n117), .B(n77), .Z(n80) );
  GTECH_NOT U195 ( .A(n68), .Z(n77) );
  GTECH_AND2 U196 ( .A(n118), .B(n119), .Z(n117) );
  GTECH_AND8 U197 ( .A(n120), .B(n121), .C(n122), .D(n123), .E(n124), .F(n125), 
        .G(n126), .H(n127), .Z(n119) );
  GTECH_XOR2 U198 ( .A(rx_br_cnt_r[14]), .B(n144), .Z(n127) );
  GTECH_XOR2 U199 ( .A(rx_br_cnt_r[13]), .B(n143), .Z(n126) );
  GTECH_XOR2 U200 ( .A(rx_br_cnt_r[12]), .B(n142), .Z(n125) );
  GTECH_XOR2 U201 ( .A(rx_br_cnt_r[11]), .B(n141), .Z(n124) );
  GTECH_XOR2 U202 ( .A(rx_br_cnt_r[10]), .B(n140), .Z(n123) );
  GTECH_XOR2 U203 ( .A(rx_br_cnt_r[9]), .B(n139), .Z(n122) );
  GTECH_XOR2 U204 ( .A(rx_br_cnt_r[8]), .B(n138), .Z(n121) );
  GTECH_XOR2 U205 ( .A(rx_br_cnt_r[7]), .B(n137), .Z(n120) );
  GTECH_AND8 U206 ( .A(n128), .B(n129), .C(n146), .D(n147), .E(n148), .F(n149), 
        .G(n150), .H(n151), .Z(n118) );
  GTECH_XOR2 U207 ( .A(rx_br_cnt_r[6]), .B(n136), .Z(n151) );
  GTECH_XOR2 U208 ( .A(rx_br_cnt_r[5]), .B(n135), .Z(n150) );
  GTECH_XOR2 U209 ( .A(rx_br_cnt_r[4]), .B(n134), .Z(n149) );
  GTECH_XOR2 U210 ( .A(rx_br_cnt_r[3]), .B(n133), .Z(n148) );
  GTECH_XOR2 U211 ( .A(rx_br_cnt_r[2]), .B(n132), .Z(n147) );
  GTECH_XOR2 U212 ( .A(rx_br_cnt_r[15]), .B(n145), .Z(n146) );
  GTECH_XOR2 U213 ( .A(rx_br_cnt_r[0]), .B(n113), .Z(n129) );
  GTECH_XOR2 U214 ( .A(rx_br_cnt_r[1]), .B(n131), .Z(n128) );
  GTECH_NOR2 U215 ( .A(reset_i), .B(n152), .Z(n68) );
  GTECH_MUX2 U216 ( .A(rx_data_r[7]), .B(rx_shifter_r[7]), .S(n82), .Z(n159)
         );
  GTECH_AND_NOT U217 ( .A(rx_shifter_r[8]), .B(n115), .Z(n82) );
  GTECH_NAND3 U218 ( .A(n152), .B(n19), .C(n157), .Z(n115) );
  GTECH_NOR4 U219 ( .A(rx_bit_cnt_r[0]), .B(rx_bit_cnt_r[1]), .C(
        rx_bit_cnt_r[2]), .D(rx_bit_cnt_r[3]), .Z(n152) );
  GTECH_OA21 U220 ( .A(n153), .B(n81), .C(n19), .Z(n158) );
  GTECH_NOT U221 ( .A(n157), .Z(n81) );
  GTECH_AOI21 U222 ( .A(rd_i), .B(n154), .C(n235), .Z(n153) );
  GTECH_AND2 U223 ( .A(rx_data_r[7]), .B(n40), .Z(N239) );
  GTECH_AND2 U224 ( .A(rx_data_r[6]), .B(n40), .Z(N238) );
  GTECH_AND2 U225 ( .A(rx_data_r[5]), .B(n40), .Z(N237) );
  GTECH_AND2 U226 ( .A(rx_data_r[4]), .B(n40), .Z(N236) );
  GTECH_AND2 U227 ( .A(rx_data_r[3]), .B(n40), .Z(N235) );
  GTECH_AND2 U228 ( .A(rx_data_r[2]), .B(n40), .Z(N234) );
  GTECH_MUX2 U229 ( .A(n155), .B(rx_data_r[1]), .S(n40), .Z(N233) );
  GTECH_NOT U230 ( .A(n235), .Z(n155) );
  GTECH_MUX2 U231 ( .A(n236), .B(rx_data_r[0]), .S(n40), .Z(N232) );
  GTECH_AND2 U232 ( .A(n154), .B(n19), .Z(n40) );
  GTECH_NOT U233 ( .A(reset_i), .Z(n19) );
  GTECH_NOR8 U234 ( .A(n156), .B(addr_i[0]), .C(addr_i[1]), .D(addr_i[2]), .E(
        addr_i[4]), .F(addr_i[5]), .G(addr_i[6]), .H(addr_i[7]), .Z(n154) );
  GTECH_NOT U235 ( .A(addr_i[3]), .Z(n156) );
endmodule


module per_gpio ( clk_i, reset_i, addr_i, wdata_i, rdata_o, size_i, rd_i, wr_i, 
        gpio_in_i, gpio_out_o );
  input [31:0] addr_i;
  input [31:0] wdata_i;
  output [31:0] rdata_o;
  input [1:0] size_i;
  input [31:0] gpio_in_i;
  output [31:0] gpio_out_o;
  input clk_i, reset_i, rd_i, wr_i;
  wire   n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93,
         n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105,
         n106, n107, n108, n109, n110, n111, n112, n113, n114, n115, n116,
         n117, n118, n119, n120, n121, n122, n123, n124, n125, n126, n127,
         n128, n129, n130, n131, n132, n133, n134, n135, n136, n137, n138,
         n139, n140, n141, n142, n143, n1, n2, n3, n4, n5, n6, n7, n8, n9, n10,
         n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24,
         n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38,
         n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52,
         n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66,
         n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n144,
         n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166,
         n167, n168, n169, n170, n171, n172;
  tri   clk_i;
  tri   reset_i;
  tri   [31:0] addr_i;
  tri   [31:0] wdata_i;
  tri   [31:0] rdata_o;
  tri   wr_i;

  GTECH_FD1 gpio_out_r_reg_31_ ( .D(n112), .CP(clk_i), .Q(gpio_out_o[31]), 
        .QN(n111) );
  GTECH_FD1 gpio_out_r_reg_30_ ( .D(n113), .CP(clk_i), .Q(gpio_out_o[30]), 
        .QN(n110) );
  GTECH_FD1 gpio_out_r_reg_29_ ( .D(n114), .CP(clk_i), .Q(gpio_out_o[29]), 
        .QN(n109) );
  GTECH_FD1 gpio_out_r_reg_28_ ( .D(n115), .CP(clk_i), .Q(gpio_out_o[28]), 
        .QN(n108) );
  GTECH_FD1 gpio_out_r_reg_27_ ( .D(n116), .CP(clk_i), .Q(gpio_out_o[27]), 
        .QN(n107) );
  GTECH_FD1 gpio_out_r_reg_26_ ( .D(n117), .CP(clk_i), .Q(gpio_out_o[26]), 
        .QN(n106) );
  GTECH_FD1 gpio_out_r_reg_25_ ( .D(n118), .CP(clk_i), .Q(gpio_out_o[25]), 
        .QN(n105) );
  GTECH_FD1 gpio_out_r_reg_24_ ( .D(n119), .CP(clk_i), .Q(gpio_out_o[24]), 
        .QN(n104) );
  GTECH_FD1 gpio_out_r_reg_23_ ( .D(n120), .CP(clk_i), .Q(gpio_out_o[23]), 
        .QN(n103) );
  GTECH_FD1 gpio_out_r_reg_22_ ( .D(n121), .CP(clk_i), .Q(gpio_out_o[22]), 
        .QN(n102) );
  GTECH_FD1 gpio_out_r_reg_21_ ( .D(n122), .CP(clk_i), .Q(gpio_out_o[21]), 
        .QN(n101) );
  GTECH_FD1 gpio_out_r_reg_20_ ( .D(n123), .CP(clk_i), .Q(gpio_out_o[20]), 
        .QN(n100) );
  GTECH_FD1 gpio_out_r_reg_19_ ( .D(n124), .CP(clk_i), .Q(gpio_out_o[19]), 
        .QN(n99) );
  GTECH_FD1 gpio_out_r_reg_18_ ( .D(n125), .CP(clk_i), .Q(gpio_out_o[18]), 
        .QN(n98) );
  GTECH_FD1 gpio_out_r_reg_17_ ( .D(n126), .CP(clk_i), .Q(gpio_out_o[17]), 
        .QN(n97) );
  GTECH_FD1 gpio_out_r_reg_16_ ( .D(n127), .CP(clk_i), .Q(gpio_out_o[16]), 
        .QN(n96) );
  GTECH_FD1 gpio_out_r_reg_15_ ( .D(n128), .CP(clk_i), .Q(gpio_out_o[15]), 
        .QN(n95) );
  GTECH_FD1 gpio_out_r_reg_14_ ( .D(n129), .CP(clk_i), .Q(gpio_out_o[14]), 
        .QN(n94) );
  GTECH_FD1 gpio_out_r_reg_13_ ( .D(n130), .CP(clk_i), .Q(gpio_out_o[13]), 
        .QN(n93) );
  GTECH_FD1 gpio_out_r_reg_12_ ( .D(n131), .CP(clk_i), .Q(gpio_out_o[12]), 
        .QN(n92) );
  GTECH_FD1 gpio_out_r_reg_11_ ( .D(n132), .CP(clk_i), .Q(gpio_out_o[11]), 
        .QN(n91) );
  GTECH_FD1 gpio_out_r_reg_10_ ( .D(n133), .CP(clk_i), .Q(gpio_out_o[10]), 
        .QN(n90) );
  GTECH_FD1 gpio_out_r_reg_9_ ( .D(n134), .CP(clk_i), .Q(gpio_out_o[9]), .QN(
        n89) );
  GTECH_FD1 gpio_out_r_reg_8_ ( .D(n135), .CP(clk_i), .Q(gpio_out_o[8]), .QN(
        n88) );
  GTECH_FD1 gpio_out_r_reg_7_ ( .D(n136), .CP(clk_i), .Q(gpio_out_o[7]), .QN(
        n87) );
  GTECH_FD1 gpio_out_r_reg_6_ ( .D(n137), .CP(clk_i), .Q(gpio_out_o[6]), .QN(
        n86) );
  GTECH_FD1 gpio_out_r_reg_5_ ( .D(n138), .CP(clk_i), .Q(gpio_out_o[5]), .QN(
        n85) );
  GTECH_FD1 gpio_out_r_reg_4_ ( .D(n139), .CP(clk_i), .Q(gpio_out_o[4]), .QN(
        n84) );
  GTECH_FD1 gpio_out_r_reg_3_ ( .D(n140), .CP(clk_i), .Q(gpio_out_o[3]), .QN(
        n83) );
  GTECH_FD1 gpio_out_r_reg_2_ ( .D(n141), .CP(clk_i), .Q(gpio_out_o[2]), .QN(
        n82) );
  GTECH_FD1 gpio_out_r_reg_1_ ( .D(n142), .CP(clk_i), .Q(gpio_out_o[1]), .QN(
        n81) );
  GTECH_FD1 gpio_out_r_reg_0_ ( .D(n143), .CP(clk_i), .Q(gpio_out_o[0]), .QN(
        n80) );
  GTECH_FD1 reg_data_r_reg_31_ ( .D(gpio_in_i[31]), .CP(clk_i), .Q(rdata_o[31]) );
  GTECH_FD1 reg_data_r_reg_30_ ( .D(gpio_in_i[30]), .CP(clk_i), .Q(rdata_o[30]) );
  GTECH_FD1 reg_data_r_reg_29_ ( .D(gpio_in_i[29]), .CP(clk_i), .Q(rdata_o[29]) );
  GTECH_FD1 reg_data_r_reg_28_ ( .D(gpio_in_i[28]), .CP(clk_i), .Q(rdata_o[28]) );
  GTECH_FD1 reg_data_r_reg_27_ ( .D(gpio_in_i[27]), .CP(clk_i), .Q(rdata_o[27]) );
  GTECH_FD1 reg_data_r_reg_26_ ( .D(gpio_in_i[26]), .CP(clk_i), .Q(rdata_o[26]) );
  GTECH_FD1 reg_data_r_reg_25_ ( .D(gpio_in_i[25]), .CP(clk_i), .Q(rdata_o[25]) );
  GTECH_FD1 reg_data_r_reg_24_ ( .D(gpio_in_i[24]), .CP(clk_i), .Q(rdata_o[24]) );
  GTECH_FD1 reg_data_r_reg_23_ ( .D(gpio_in_i[23]), .CP(clk_i), .Q(rdata_o[23]) );
  GTECH_FD1 reg_data_r_reg_22_ ( .D(gpio_in_i[22]), .CP(clk_i), .Q(rdata_o[22]) );
  GTECH_FD1 reg_data_r_reg_21_ ( .D(gpio_in_i[21]), .CP(clk_i), .Q(rdata_o[21]) );
  GTECH_FD1 reg_data_r_reg_20_ ( .D(gpio_in_i[20]), .CP(clk_i), .Q(rdata_o[20]) );
  GTECH_FD1 reg_data_r_reg_19_ ( .D(gpio_in_i[19]), .CP(clk_i), .Q(rdata_o[19]) );
  GTECH_FD1 reg_data_r_reg_18_ ( .D(gpio_in_i[18]), .CP(clk_i), .Q(rdata_o[18]) );
  GTECH_FD1 reg_data_r_reg_17_ ( .D(gpio_in_i[17]), .CP(clk_i), .Q(rdata_o[17]) );
  GTECH_FD1 reg_data_r_reg_16_ ( .D(gpio_in_i[16]), .CP(clk_i), .Q(rdata_o[16]) );
  GTECH_FD1 reg_data_r_reg_15_ ( .D(gpio_in_i[15]), .CP(clk_i), .Q(rdata_o[15]) );
  GTECH_FD1 reg_data_r_reg_14_ ( .D(gpio_in_i[14]), .CP(clk_i), .Q(rdata_o[14]) );
  GTECH_FD1 reg_data_r_reg_13_ ( .D(gpio_in_i[13]), .CP(clk_i), .Q(rdata_o[13]) );
  GTECH_FD1 reg_data_r_reg_12_ ( .D(gpio_in_i[12]), .CP(clk_i), .Q(rdata_o[12]) );
  GTECH_FD1 reg_data_r_reg_11_ ( .D(gpio_in_i[11]), .CP(clk_i), .Q(rdata_o[11]) );
  GTECH_FD1 reg_data_r_reg_10_ ( .D(gpio_in_i[10]), .CP(clk_i), .Q(rdata_o[10]) );
  GTECH_FD1 reg_data_r_reg_9_ ( .D(gpio_in_i[9]), .CP(clk_i), .Q(rdata_o[9])
         );
  GTECH_FD1 reg_data_r_reg_8_ ( .D(gpio_in_i[8]), .CP(clk_i), .Q(rdata_o[8])
         );
  GTECH_FD1 reg_data_r_reg_7_ ( .D(gpio_in_i[7]), .CP(clk_i), .Q(rdata_o[7])
         );
  GTECH_FD1 reg_data_r_reg_6_ ( .D(gpio_in_i[6]), .CP(clk_i), .Q(rdata_o[6])
         );
  GTECH_FD1 reg_data_r_reg_5_ ( .D(gpio_in_i[5]), .CP(clk_i), .Q(rdata_o[5])
         );
  GTECH_FD1 reg_data_r_reg_4_ ( .D(gpio_in_i[4]), .CP(clk_i), .Q(rdata_o[4])
         );
  GTECH_FD1 reg_data_r_reg_3_ ( .D(gpio_in_i[3]), .CP(clk_i), .Q(rdata_o[3])
         );
  GTECH_FD1 reg_data_r_reg_2_ ( .D(gpio_in_i[2]), .CP(clk_i), .Q(rdata_o[2])
         );
  GTECH_FD1 reg_data_r_reg_1_ ( .D(gpio_in_i[1]), .CP(clk_i), .Q(rdata_o[1])
         );
  GTECH_FD1 reg_data_r_reg_0_ ( .D(gpio_in_i[0]), .CP(clk_i), .Q(rdata_o[0])
         );
  GTECH_OAI2N2 U3 ( .A(n80), .B(n1), .C(n2), .D(wdata_i[0]), .Z(n143) );
  GTECH_OAI21 U4 ( .A(n3), .B(n4), .C(n5), .Z(n2) );
  GTECH_NOT U5 ( .A(n80), .Z(n4) );
  GTECH_OA21 U6 ( .A(wdata_i[0]), .B(n6), .C(n7), .Z(n1) );
  GTECH_OAI2N2 U7 ( .A(n81), .B(n8), .C(n9), .D(wdata_i[1]), .Z(n142) );
  GTECH_OAI21 U8 ( .A(n3), .B(n10), .C(n5), .Z(n9) );
  GTECH_NOT U9 ( .A(n81), .Z(n10) );
  GTECH_OA21 U10 ( .A(wdata_i[1]), .B(n6), .C(n7), .Z(n8) );
  GTECH_OAI2N2 U11 ( .A(n82), .B(n11), .C(n12), .D(wdata_i[2]), .Z(n141) );
  GTECH_OAI21 U12 ( .A(n3), .B(n13), .C(n5), .Z(n12) );
  GTECH_NOT U13 ( .A(n82), .Z(n13) );
  GTECH_OA21 U14 ( .A(wdata_i[2]), .B(n6), .C(n7), .Z(n11) );
  GTECH_OAI2N2 U15 ( .A(n83), .B(n14), .C(n15), .D(wdata_i[3]), .Z(n140) );
  GTECH_OAI21 U16 ( .A(n3), .B(n16), .C(n5), .Z(n15) );
  GTECH_NOT U17 ( .A(n83), .Z(n16) );
  GTECH_OA21 U18 ( .A(wdata_i[3]), .B(n6), .C(n7), .Z(n14) );
  GTECH_OAI2N2 U19 ( .A(n84), .B(n17), .C(n18), .D(wdata_i[4]), .Z(n139) );
  GTECH_OAI21 U20 ( .A(n3), .B(n19), .C(n5), .Z(n18) );
  GTECH_NOT U21 ( .A(n84), .Z(n19) );
  GTECH_OA21 U22 ( .A(wdata_i[4]), .B(n6), .C(n7), .Z(n17) );
  GTECH_OAI2N2 U23 ( .A(n85), .B(n20), .C(n21), .D(wdata_i[5]), .Z(n138) );
  GTECH_OAI21 U24 ( .A(n3), .B(n22), .C(n5), .Z(n21) );
  GTECH_NOT U25 ( .A(n85), .Z(n22) );
  GTECH_OA21 U26 ( .A(wdata_i[5]), .B(n6), .C(n7), .Z(n20) );
  GTECH_OAI2N2 U27 ( .A(n86), .B(n23), .C(n24), .D(wdata_i[6]), .Z(n137) );
  GTECH_OAI21 U28 ( .A(n3), .B(n25), .C(n5), .Z(n24) );
  GTECH_NOT U29 ( .A(n86), .Z(n25) );
  GTECH_OA21 U30 ( .A(wdata_i[6]), .B(n6), .C(n7), .Z(n23) );
  GTECH_OAI2N2 U31 ( .A(n87), .B(n26), .C(n27), .D(wdata_i[7]), .Z(n136) );
  GTECH_OAI21 U32 ( .A(n3), .B(n28), .C(n5), .Z(n27) );
  GTECH_NOT U33 ( .A(n87), .Z(n28) );
  GTECH_OA21 U34 ( .A(wdata_i[7]), .B(n6), .C(n7), .Z(n26) );
  GTECH_OAI2N2 U35 ( .A(n88), .B(n29), .C(n30), .D(wdata_i[8]), .Z(n135) );
  GTECH_OAI21 U36 ( .A(n3), .B(n31), .C(n5), .Z(n30) );
  GTECH_NOT U37 ( .A(n88), .Z(n31) );
  GTECH_OA21 U38 ( .A(wdata_i[8]), .B(n6), .C(n7), .Z(n29) );
  GTECH_OAI2N2 U39 ( .A(n89), .B(n32), .C(n33), .D(wdata_i[9]), .Z(n134) );
  GTECH_OAI21 U40 ( .A(n3), .B(n34), .C(n5), .Z(n33) );
  GTECH_NOT U41 ( .A(n89), .Z(n34) );
  GTECH_OA21 U42 ( .A(wdata_i[9]), .B(n6), .C(n7), .Z(n32) );
  GTECH_OAI2N2 U43 ( .A(n90), .B(n35), .C(n36), .D(wdata_i[10]), .Z(n133) );
  GTECH_OAI21 U44 ( .A(n3), .B(n37), .C(n5), .Z(n36) );
  GTECH_NOT U45 ( .A(n90), .Z(n37) );
  GTECH_OA21 U46 ( .A(wdata_i[10]), .B(n6), .C(n7), .Z(n35) );
  GTECH_OAI2N2 U47 ( .A(n91), .B(n38), .C(n39), .D(wdata_i[11]), .Z(n132) );
  GTECH_OAI21 U48 ( .A(n3), .B(n40), .C(n5), .Z(n39) );
  GTECH_NOT U49 ( .A(n91), .Z(n40) );
  GTECH_OA21 U50 ( .A(wdata_i[11]), .B(n6), .C(n7), .Z(n38) );
  GTECH_OAI2N2 U51 ( .A(n92), .B(n41), .C(n42), .D(wdata_i[12]), .Z(n131) );
  GTECH_OAI21 U52 ( .A(n3), .B(n43), .C(n5), .Z(n42) );
  GTECH_NOT U53 ( .A(n92), .Z(n43) );
  GTECH_OA21 U54 ( .A(wdata_i[12]), .B(n6), .C(n7), .Z(n41) );
  GTECH_OAI2N2 U55 ( .A(n93), .B(n44), .C(n45), .D(wdata_i[13]), .Z(n130) );
  GTECH_OAI21 U56 ( .A(n3), .B(n46), .C(n5), .Z(n45) );
  GTECH_NOT U57 ( .A(n93), .Z(n46) );
  GTECH_OA21 U58 ( .A(wdata_i[13]), .B(n6), .C(n7), .Z(n44) );
  GTECH_OAI2N2 U59 ( .A(n94), .B(n47), .C(n48), .D(wdata_i[14]), .Z(n129) );
  GTECH_OAI21 U60 ( .A(n3), .B(n49), .C(n5), .Z(n48) );
  GTECH_NOT U61 ( .A(n94), .Z(n49) );
  GTECH_OA21 U62 ( .A(wdata_i[14]), .B(n6), .C(n7), .Z(n47) );
  GTECH_OAI2N2 U63 ( .A(n95), .B(n50), .C(n51), .D(wdata_i[15]), .Z(n128) );
  GTECH_OAI21 U64 ( .A(n3), .B(n52), .C(n5), .Z(n51) );
  GTECH_NOT U65 ( .A(n95), .Z(n52) );
  GTECH_OA21 U66 ( .A(wdata_i[15]), .B(n6), .C(n7), .Z(n50) );
  GTECH_OAI2N2 U67 ( .A(n96), .B(n53), .C(n54), .D(wdata_i[16]), .Z(n127) );
  GTECH_OAI21 U68 ( .A(n3), .B(n55), .C(n5), .Z(n54) );
  GTECH_NOT U69 ( .A(n96), .Z(n55) );
  GTECH_OA21 U70 ( .A(wdata_i[16]), .B(n6), .C(n7), .Z(n53) );
  GTECH_OAI2N2 U71 ( .A(n97), .B(n56), .C(n57), .D(wdata_i[17]), .Z(n126) );
  GTECH_OAI21 U72 ( .A(n3), .B(n58), .C(n5), .Z(n57) );
  GTECH_NOT U73 ( .A(n97), .Z(n58) );
  GTECH_OA21 U74 ( .A(wdata_i[17]), .B(n6), .C(n7), .Z(n56) );
  GTECH_OAI2N2 U75 ( .A(n98), .B(n59), .C(n60), .D(wdata_i[18]), .Z(n125) );
  GTECH_OAI21 U76 ( .A(n3), .B(n61), .C(n5), .Z(n60) );
  GTECH_NOT U77 ( .A(n98), .Z(n61) );
  GTECH_OA21 U78 ( .A(wdata_i[18]), .B(n6), .C(n7), .Z(n59) );
  GTECH_OAI2N2 U79 ( .A(n99), .B(n62), .C(n63), .D(wdata_i[19]), .Z(n124) );
  GTECH_OAI21 U80 ( .A(n3), .B(n64), .C(n5), .Z(n63) );
  GTECH_NOT U81 ( .A(n99), .Z(n64) );
  GTECH_OA21 U82 ( .A(wdata_i[19]), .B(n6), .C(n7), .Z(n62) );
  GTECH_OAI2N2 U83 ( .A(n100), .B(n65), .C(n66), .D(wdata_i[20]), .Z(n123) );
  GTECH_OAI21 U84 ( .A(n3), .B(n67), .C(n5), .Z(n66) );
  GTECH_NOT U85 ( .A(n100), .Z(n67) );
  GTECH_OA21 U86 ( .A(wdata_i[20]), .B(n6), .C(n7), .Z(n65) );
  GTECH_OAI2N2 U87 ( .A(n101), .B(n68), .C(n69), .D(wdata_i[21]), .Z(n122) );
  GTECH_OAI21 U88 ( .A(n3), .B(n70), .C(n5), .Z(n69) );
  GTECH_NOT U89 ( .A(n101), .Z(n70) );
  GTECH_OA21 U90 ( .A(wdata_i[21]), .B(n6), .C(n7), .Z(n68) );
  GTECH_OAI2N2 U91 ( .A(n102), .B(n71), .C(n72), .D(wdata_i[22]), .Z(n121) );
  GTECH_OAI21 U92 ( .A(n3), .B(n73), .C(n5), .Z(n72) );
  GTECH_NOT U93 ( .A(n102), .Z(n73) );
  GTECH_OA21 U94 ( .A(wdata_i[22]), .B(n6), .C(n7), .Z(n71) );
  GTECH_OAI2N2 U95 ( .A(n103), .B(n74), .C(n75), .D(wdata_i[23]), .Z(n120) );
  GTECH_OAI21 U96 ( .A(n3), .B(n76), .C(n5), .Z(n75) );
  GTECH_NOT U97 ( .A(n103), .Z(n76) );
  GTECH_OA21 U98 ( .A(wdata_i[23]), .B(n6), .C(n7), .Z(n74) );
  GTECH_OAI2N2 U99 ( .A(n104), .B(n77), .C(n78), .D(wdata_i[24]), .Z(n119) );
  GTECH_OAI21 U100 ( .A(n3), .B(n79), .C(n5), .Z(n78) );
  GTECH_NOT U101 ( .A(n104), .Z(n79) );
  GTECH_OA21 U102 ( .A(wdata_i[24]), .B(n6), .C(n7), .Z(n77) );
  GTECH_OAI2N2 U103 ( .A(n105), .B(n144), .C(n145), .D(wdata_i[25]), .Z(n118)
         );
  GTECH_OAI21 U104 ( .A(n3), .B(n146), .C(n5), .Z(n145) );
  GTECH_NOT U105 ( .A(n105), .Z(n146) );
  GTECH_OA21 U106 ( .A(wdata_i[25]), .B(n6), .C(n7), .Z(n144) );
  GTECH_OAI2N2 U107 ( .A(n106), .B(n147), .C(n148), .D(wdata_i[26]), .Z(n117)
         );
  GTECH_OAI21 U108 ( .A(n3), .B(n149), .C(n5), .Z(n148) );
  GTECH_NOT U109 ( .A(n106), .Z(n149) );
  GTECH_OA21 U110 ( .A(wdata_i[26]), .B(n6), .C(n7), .Z(n147) );
  GTECH_OAI2N2 U111 ( .A(n107), .B(n150), .C(n151), .D(wdata_i[27]), .Z(n116)
         );
  GTECH_OAI21 U112 ( .A(n3), .B(n152), .C(n5), .Z(n151) );
  GTECH_NOT U113 ( .A(n107), .Z(n152) );
  GTECH_OA21 U114 ( .A(wdata_i[27]), .B(n6), .C(n7), .Z(n150) );
  GTECH_OAI2N2 U115 ( .A(n108), .B(n153), .C(n154), .D(wdata_i[28]), .Z(n115)
         );
  GTECH_OAI21 U116 ( .A(n3), .B(n155), .C(n5), .Z(n154) );
  GTECH_NOT U117 ( .A(n108), .Z(n155) );
  GTECH_OA21 U118 ( .A(wdata_i[28]), .B(n6), .C(n7), .Z(n153) );
  GTECH_OAI2N2 U119 ( .A(n109), .B(n156), .C(n157), .D(wdata_i[29]), .Z(n114)
         );
  GTECH_OAI21 U120 ( .A(n3), .B(n158), .C(n5), .Z(n157) );
  GTECH_NOT U121 ( .A(n109), .Z(n158) );
  GTECH_OA21 U122 ( .A(wdata_i[29]), .B(n6), .C(n7), .Z(n156) );
  GTECH_OAI2N2 U123 ( .A(n110), .B(n159), .C(n160), .D(wdata_i[30]), .Z(n113)
         );
  GTECH_OAI21 U124 ( .A(n3), .B(n161), .C(n5), .Z(n160) );
  GTECH_NOT U125 ( .A(n110), .Z(n161) );
  GTECH_OA21 U126 ( .A(wdata_i[30]), .B(n6), .C(n7), .Z(n159) );
  GTECH_OAI2N2 U127 ( .A(n111), .B(n162), .C(n163), .D(wdata_i[31]), .Z(n112)
         );
  GTECH_OAI21 U128 ( .A(n3), .B(n164), .C(n5), .Z(n163) );
  GTECH_NOT U129 ( .A(n111), .Z(n164) );
  GTECH_OR3 U130 ( .A(n165), .B(n166), .C(n167), .Z(n3) );
  GTECH_NOT U131 ( .A(addr_i[2]), .Z(n167) );
  GTECH_OA21 U132 ( .A(wdata_i[31]), .B(n6), .C(n7), .Z(n162) );
  GTECH_OA21 U133 ( .A(n168), .B(n165), .C(n169), .Z(n7) );
  GTECH_NAND3 U134 ( .A(n5), .B(n170), .C(n6), .Z(n169) );
  GTECH_OR2 U135 ( .A(n168), .B(reset_i), .Z(n5) );
  GTECH_OR2 U136 ( .A(n166), .B(n165), .Z(n6) );
  GTECH_OAI21 U137 ( .A(addr_i[2]), .B(n168), .C(n170), .Z(n165) );
  GTECH_NOT U138 ( .A(reset_i), .Z(n170) );
  GTECH_OR2 U139 ( .A(addr_i[3]), .B(n166), .Z(n168) );
  GTECH_NAND3 U140 ( .A(n171), .B(wr_i), .C(n172), .Z(n166) );
  GTECH_NOR4 U141 ( .A(addr_i[7]), .B(addr_i[6]), .C(addr_i[5]), .D(addr_i[4]), 
        .Z(n172) );
  GTECH_NOR2 U142 ( .A(addr_i[1]), .B(addr_i[0]), .Z(n171) );
endmodule


module per_timer_DW01_inc_0 ( A, SUM );
  input [31:0] A;
  output [31:0] SUM;

  wire   [31:2] carry;

  GTECH_ADD_AB U1_1_1 ( .A(A[1]), .B(A[0]), .COUT(carry[2]), .S(SUM[1]) );
  GTECH_ADD_AB U1_1_2 ( .A(A[2]), .B(carry[2]), .COUT(carry[3]), .S(SUM[2]) );
  GTECH_ADD_AB U1_1_3 ( .A(A[3]), .B(carry[3]), .COUT(carry[4]), .S(SUM[3]) );
  GTECH_ADD_AB U1_1_4 ( .A(A[4]), .B(carry[4]), .COUT(carry[5]), .S(SUM[4]) );
  GTECH_ADD_AB U1_1_5 ( .A(A[5]), .B(carry[5]), .COUT(carry[6]), .S(SUM[5]) );
  GTECH_ADD_AB U1_1_6 ( .A(A[6]), .B(carry[6]), .COUT(carry[7]), .S(SUM[6]) );
  GTECH_ADD_AB U1_1_7 ( .A(A[7]), .B(carry[7]), .COUT(carry[8]), .S(SUM[7]) );
  GTECH_ADD_AB U1_1_8 ( .A(A[8]), .B(carry[8]), .COUT(carry[9]), .S(SUM[8]) );
  GTECH_ADD_AB U1_1_9 ( .A(A[9]), .B(carry[9]), .COUT(carry[10]), .S(SUM[9])
         );
  GTECH_ADD_AB U1_1_10 ( .A(A[10]), .B(carry[10]), .COUT(carry[11]), .S(
        SUM[10]) );
  GTECH_ADD_AB U1_1_11 ( .A(A[11]), .B(carry[11]), .COUT(carry[12]), .S(
        SUM[11]) );
  GTECH_ADD_AB U1_1_12 ( .A(A[12]), .B(carry[12]), .COUT(carry[13]), .S(
        SUM[12]) );
  GTECH_ADD_AB U1_1_13 ( .A(A[13]), .B(carry[13]), .COUT(carry[14]), .S(
        SUM[13]) );
  GTECH_ADD_AB U1_1_14 ( .A(A[14]), .B(carry[14]), .COUT(carry[15]), .S(
        SUM[14]) );
  GTECH_ADD_AB U1_1_15 ( .A(A[15]), .B(carry[15]), .COUT(carry[16]), .S(
        SUM[15]) );
  GTECH_ADD_AB U1_1_16 ( .A(A[16]), .B(carry[16]), .COUT(carry[17]), .S(
        SUM[16]) );
  GTECH_ADD_AB U1_1_17 ( .A(A[17]), .B(carry[17]), .COUT(carry[18]), .S(
        SUM[17]) );
  GTECH_ADD_AB U1_1_18 ( .A(A[18]), .B(carry[18]), .COUT(carry[19]), .S(
        SUM[18]) );
  GTECH_ADD_AB U1_1_19 ( .A(A[19]), .B(carry[19]), .COUT(carry[20]), .S(
        SUM[19]) );
  GTECH_ADD_AB U1_1_20 ( .A(A[20]), .B(carry[20]), .COUT(carry[21]), .S(
        SUM[20]) );
  GTECH_ADD_AB U1_1_21 ( .A(A[21]), .B(carry[21]), .COUT(carry[22]), .S(
        SUM[21]) );
  GTECH_ADD_AB U1_1_22 ( .A(A[22]), .B(carry[22]), .COUT(carry[23]), .S(
        SUM[22]) );
  GTECH_ADD_AB U1_1_23 ( .A(A[23]), .B(carry[23]), .COUT(carry[24]), .S(
        SUM[23]) );
  GTECH_ADD_AB U1_1_24 ( .A(A[24]), .B(carry[24]), .COUT(carry[25]), .S(
        SUM[24]) );
  GTECH_ADD_AB U1_1_25 ( .A(A[25]), .B(carry[25]), .COUT(carry[26]), .S(
        SUM[25]) );
  GTECH_ADD_AB U1_1_26 ( .A(A[26]), .B(carry[26]), .COUT(carry[27]), .S(
        SUM[26]) );
  GTECH_ADD_AB U1_1_27 ( .A(A[27]), .B(carry[27]), .COUT(carry[28]), .S(
        SUM[27]) );
  GTECH_ADD_AB U1_1_28 ( .A(A[28]), .B(carry[28]), .COUT(carry[29]), .S(
        SUM[28]) );
  GTECH_ADD_AB U1_1_29 ( .A(A[29]), .B(carry[29]), .COUT(carry[30]), .S(
        SUM[29]) );
  GTECH_ADD_AB U1_1_30 ( .A(A[30]), .B(carry[30]), .COUT(carry[31]), .S(
        SUM[30]) );
  GTECH_NOT U1 ( .A(A[0]), .Z(SUM[0]) );
  GTECH_XOR2 U2 ( .A(carry[31]), .B(A[31]), .Z(SUM[31]) );
endmodule


module per_timer ( clk_i, reset_i, addr_i, wdata_i, rdata_o, size_i, rd_i, 
        wr_i );
  input [31:0] addr_i;
  input [31:0] wdata_i;
  output [31:0] rdata_o;
  input [1:0] size_i;
  input clk_i, reset_i, rd_i, wr_i;
  wire   N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43, N44,
         N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57, N58,
         N59, N60, N61, N62, n115, n116, n117, n118, n119, n120, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n148, n149, n2, n3, n4, n5, n6, n7, n8, n9, n10,
         n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24,
         n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38,
         n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52,
         n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66,
         n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80,
         n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94,
         n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105, n106,
         n107, n108, n109, n110, n111, n112, n113, n114, n150, n151, n152,
         n153, n154;
  wire   [31:0] timer_count_r;
  wire   [31:0] timer_compare_r;
  wire   [2:0] csr_w;
  tri   clk_i;
  tri   reset_i;
  tri   [31:0] addr_i;
  tri   [31:0] wdata_i;
  tri   wr_i;
  assign rdata_o[3] = rdata_o[31];
  assign rdata_o[4] = rdata_o[31];
  assign rdata_o[5] = rdata_o[31];
  assign rdata_o[6] = rdata_o[31];
  assign rdata_o[7] = rdata_o[31];
  assign rdata_o[8] = rdata_o[31];
  assign rdata_o[9] = rdata_o[31];
  assign rdata_o[10] = rdata_o[31];
  assign rdata_o[11] = rdata_o[31];
  assign rdata_o[12] = rdata_o[31];
  assign rdata_o[13] = rdata_o[31];
  assign rdata_o[14] = rdata_o[31];
  assign rdata_o[15] = rdata_o[31];
  assign rdata_o[16] = rdata_o[31];
  assign rdata_o[17] = rdata_o[31];
  assign rdata_o[18] = rdata_o[31];
  assign rdata_o[19] = rdata_o[31];
  assign rdata_o[20] = rdata_o[31];
  assign rdata_o[21] = rdata_o[31];
  assign rdata_o[22] = rdata_o[31];
  assign rdata_o[23] = rdata_o[31];
  assign rdata_o[24] = rdata_o[31];
  assign rdata_o[25] = rdata_o[31];
  assign rdata_o[26] = rdata_o[31];
  assign rdata_o[27] = rdata_o[31];
  assign rdata_o[28] = rdata_o[31];
  assign rdata_o[29] = rdata_o[31];
  assign rdata_o[30] = rdata_o[31];

  GTECH_FD1 timer_enabled_r_reg ( .D(n149), .CP(clk_i), .Q(csr_w[0]), .QN(n115) );
  GTECH_FD1 timer_compare_r_reg_31_ ( .D(n148), .CP(clk_i), .Q(
        timer_compare_r[31]) );
  GTECH_FD1 timer_compare_r_reg_30_ ( .D(n147), .CP(clk_i), .Q(
        timer_compare_r[30]) );
  GTECH_FD1 timer_compare_r_reg_29_ ( .D(n146), .CP(clk_i), .Q(
        timer_compare_r[29]) );
  GTECH_FD1 timer_compare_r_reg_28_ ( .D(n145), .CP(clk_i), .Q(
        timer_compare_r[28]) );
  GTECH_FD1 timer_compare_r_reg_27_ ( .D(n144), .CP(clk_i), .Q(
        timer_compare_r[27]) );
  GTECH_FD1 timer_compare_r_reg_26_ ( .D(n143), .CP(clk_i), .Q(
        timer_compare_r[26]) );
  GTECH_FD1 timer_compare_r_reg_25_ ( .D(n142), .CP(clk_i), .Q(
        timer_compare_r[25]) );
  GTECH_FD1 timer_compare_r_reg_24_ ( .D(n141), .CP(clk_i), .Q(
        timer_compare_r[24]) );
  GTECH_FD1 timer_compare_r_reg_23_ ( .D(n140), .CP(clk_i), .Q(
        timer_compare_r[23]) );
  GTECH_FD1 timer_compare_r_reg_22_ ( .D(n139), .CP(clk_i), .Q(
        timer_compare_r[22]) );
  GTECH_FD1 timer_compare_r_reg_21_ ( .D(n138), .CP(clk_i), .Q(
        timer_compare_r[21]) );
  GTECH_FD1 timer_compare_r_reg_20_ ( .D(n137), .CP(clk_i), .Q(
        timer_compare_r[20]) );
  GTECH_FD1 timer_compare_r_reg_19_ ( .D(n136), .CP(clk_i), .Q(
        timer_compare_r[19]) );
  GTECH_FD1 timer_compare_r_reg_18_ ( .D(n135), .CP(clk_i), .Q(
        timer_compare_r[18]) );
  GTECH_FD1 timer_compare_r_reg_17_ ( .D(n134), .CP(clk_i), .Q(
        timer_compare_r[17]) );
  GTECH_FD1 timer_compare_r_reg_16_ ( .D(n133), .CP(clk_i), .Q(
        timer_compare_r[16]) );
  GTECH_FD1 timer_compare_r_reg_15_ ( .D(n132), .CP(clk_i), .Q(
        timer_compare_r[15]) );
  GTECH_FD1 timer_compare_r_reg_14_ ( .D(n131), .CP(clk_i), .Q(
        timer_compare_r[14]) );
  GTECH_FD1 timer_compare_r_reg_13_ ( .D(n130), .CP(clk_i), .Q(
        timer_compare_r[13]) );
  GTECH_FD1 timer_compare_r_reg_12_ ( .D(n129), .CP(clk_i), .Q(
        timer_compare_r[12]) );
  GTECH_FD1 timer_compare_r_reg_11_ ( .D(n128), .CP(clk_i), .Q(
        timer_compare_r[11]) );
  GTECH_FD1 timer_compare_r_reg_10_ ( .D(n127), .CP(clk_i), .Q(
        timer_compare_r[10]) );
  GTECH_FD1 timer_compare_r_reg_9_ ( .D(n126), .CP(clk_i), .Q(
        timer_compare_r[9]) );
  GTECH_FD1 timer_compare_r_reg_8_ ( .D(n125), .CP(clk_i), .Q(
        timer_compare_r[8]) );
  GTECH_FD1 timer_compare_r_reg_7_ ( .D(n124), .CP(clk_i), .Q(
        timer_compare_r[7]) );
  GTECH_FD1 timer_compare_r_reg_6_ ( .D(n123), .CP(clk_i), .Q(
        timer_compare_r[6]) );
  GTECH_FD1 timer_compare_r_reg_5_ ( .D(n122), .CP(clk_i), .Q(
        timer_compare_r[5]) );
  GTECH_FD1 timer_compare_r_reg_4_ ( .D(n121), .CP(clk_i), .Q(
        timer_compare_r[4]) );
  GTECH_FD1 timer_compare_r_reg_3_ ( .D(n120), .CP(clk_i), .Q(
        timer_compare_r[3]) );
  GTECH_FD1 timer_compare_r_reg_2_ ( .D(n119), .CP(clk_i), .Q(
        timer_compare_r[2]) );
  GTECH_FD1 timer_compare_r_reg_1_ ( .D(n118), .CP(clk_i), .Q(
        timer_compare_r[1]) );
  GTECH_FD1 timer_compare_r_reg_0_ ( .D(n117), .CP(clk_i), .Q(
        timer_compare_r[0]) );
  GTECH_FD1 timer_overflow_r_reg ( .D(n116), .CP(clk_i), .Q(csr_w[2]) );
  GTECH_FD1 timer_count_r_reg_0_ ( .D(n154), .CP(clk_i), .Q(timer_count_r[0])
         );
  GTECH_FD1 timer_count_r_reg_1_ ( .D(n153), .CP(clk_i), .Q(timer_count_r[1])
         );
  GTECH_FD1 timer_count_r_reg_2_ ( .D(n152), .CP(clk_i), .Q(timer_count_r[2])
         );
  GTECH_FD1 timer_count_r_reg_3_ ( .D(n151), .CP(clk_i), .Q(timer_count_r[3])
         );
  GTECH_FD1 timer_count_r_reg_4_ ( .D(n150), .CP(clk_i), .Q(timer_count_r[4])
         );
  GTECH_FD1 timer_count_r_reg_5_ ( .D(n114), .CP(clk_i), .Q(timer_count_r[5])
         );
  GTECH_FD1 timer_count_r_reg_6_ ( .D(n113), .CP(clk_i), .Q(timer_count_r[6])
         );
  GTECH_FD1 timer_count_r_reg_7_ ( .D(n112), .CP(clk_i), .Q(timer_count_r[7])
         );
  GTECH_FD1 timer_count_r_reg_8_ ( .D(n111), .CP(clk_i), .Q(timer_count_r[8])
         );
  GTECH_FD1 timer_count_r_reg_9_ ( .D(n110), .CP(clk_i), .Q(timer_count_r[9])
         );
  GTECH_FD1 timer_count_r_reg_10_ ( .D(n109), .CP(clk_i), .Q(timer_count_r[10]) );
  GTECH_FD1 timer_count_r_reg_11_ ( .D(n108), .CP(clk_i), .Q(timer_count_r[11]) );
  GTECH_FD1 timer_count_r_reg_12_ ( .D(n107), .CP(clk_i), .Q(timer_count_r[12]) );
  GTECH_FD1 timer_count_r_reg_13_ ( .D(n106), .CP(clk_i), .Q(timer_count_r[13]) );
  GTECH_FD1 timer_count_r_reg_14_ ( .D(n105), .CP(clk_i), .Q(timer_count_r[14]) );
  GTECH_FD1 timer_count_r_reg_15_ ( .D(n104), .CP(clk_i), .Q(timer_count_r[15]) );
  GTECH_FD1 timer_count_r_reg_16_ ( .D(n103), .CP(clk_i), .Q(timer_count_r[16]) );
  GTECH_FD1 timer_count_r_reg_17_ ( .D(n102), .CP(clk_i), .Q(timer_count_r[17]) );
  GTECH_FD1 timer_count_r_reg_18_ ( .D(n101), .CP(clk_i), .Q(timer_count_r[18]) );
  GTECH_FD1 timer_count_r_reg_19_ ( .D(n100), .CP(clk_i), .Q(timer_count_r[19]) );
  GTECH_FD1 timer_count_r_reg_20_ ( .D(n99), .CP(clk_i), .Q(timer_count_r[20])
         );
  GTECH_FD1 timer_count_r_reg_21_ ( .D(n98), .CP(clk_i), .Q(timer_count_r[21])
         );
  GTECH_FD1 timer_count_r_reg_22_ ( .D(n97), .CP(clk_i), .Q(timer_count_r[22])
         );
  GTECH_FD1 timer_count_r_reg_23_ ( .D(n96), .CP(clk_i), .Q(timer_count_r[23])
         );
  GTECH_FD1 timer_count_r_reg_24_ ( .D(n95), .CP(clk_i), .Q(timer_count_r[24])
         );
  GTECH_FD1 timer_count_r_reg_25_ ( .D(n94), .CP(clk_i), .Q(timer_count_r[25])
         );
  GTECH_FD1 timer_count_r_reg_26_ ( .D(n93), .CP(clk_i), .Q(timer_count_r[26])
         );
  GTECH_FD1 timer_count_r_reg_27_ ( .D(n92), .CP(clk_i), .Q(timer_count_r[27])
         );
  GTECH_FD1 timer_count_r_reg_28_ ( .D(n91), .CP(clk_i), .Q(timer_count_r[28])
         );
  GTECH_FD1 timer_count_r_reg_29_ ( .D(n90), .CP(clk_i), .Q(timer_count_r[29])
         );
  GTECH_FD1 timer_count_r_reg_30_ ( .D(n89), .CP(clk_i), .Q(timer_count_r[30])
         );
  GTECH_FD1 timer_count_r_reg_31_ ( .D(n88), .CP(clk_i), .Q(timer_count_r[31])
         );
  GTECH_FD1 reg_data_r_reg_2_ ( .D(csr_w[2]), .CP(clk_i), .Q(rdata_o[2]) );
  GTECH_FD1 reg_data_r_reg_1_ ( .D(n115), .CP(clk_i), .Q(rdata_o[1]) );
  GTECH_FD1 reg_data_r_reg_0_ ( .D(csr_w[0]), .CP(clk_i), .Q(rdata_o[0]) );
  per_timer_DW01_inc_0 add_100 ( .A(timer_count_r), .SUM({N62, N61, N60, N59, 
        N58, N57, N56, N55, N54, N53, N52, N51, N50, N49, N48, N47, N46, N45, 
        N44, N43, N42, N41, N40, N39, N38, N37, N36, N35, N34, N33, N32, N31})
         );
  GTECH_ZERO U3 ( .Z(rdata_o[31]) );
  GTECH_NOT U4 ( .A(n2), .Z(n88) );
  GTECH_AOI222 U5 ( .A(N62), .B(n3), .C(n4), .D(timer_count_r[31]), .E(
        wdata_i[31]), .F(n5), .Z(n2) );
  GTECH_NOT U6 ( .A(n6), .Z(n89) );
  GTECH_AOI222 U7 ( .A(N61), .B(n3), .C(n4), .D(timer_count_r[30]), .E(
        wdata_i[30]), .F(n5), .Z(n6) );
  GTECH_NOT U8 ( .A(n7), .Z(n90) );
  GTECH_AOI222 U9 ( .A(N60), .B(n3), .C(n4), .D(timer_count_r[29]), .E(
        wdata_i[29]), .F(n5), .Z(n7) );
  GTECH_NOT U10 ( .A(n8), .Z(n91) );
  GTECH_AOI222 U11 ( .A(N59), .B(n3), .C(n4), .D(timer_count_r[28]), .E(
        wdata_i[28]), .F(n5), .Z(n8) );
  GTECH_NOT U12 ( .A(n9), .Z(n92) );
  GTECH_AOI222 U13 ( .A(N58), .B(n3), .C(n4), .D(timer_count_r[27]), .E(
        wdata_i[27]), .F(n5), .Z(n9) );
  GTECH_NOT U14 ( .A(n10), .Z(n93) );
  GTECH_AOI222 U15 ( .A(N57), .B(n3), .C(n4), .D(timer_count_r[26]), .E(
        wdata_i[26]), .F(n5), .Z(n10) );
  GTECH_NOT U16 ( .A(n11), .Z(n94) );
  GTECH_AOI222 U17 ( .A(N56), .B(n3), .C(n4), .D(timer_count_r[25]), .E(
        wdata_i[25]), .F(n5), .Z(n11) );
  GTECH_NOT U18 ( .A(n12), .Z(n95) );
  GTECH_AOI222 U19 ( .A(N55), .B(n3), .C(n4), .D(timer_count_r[24]), .E(
        wdata_i[24]), .F(n5), .Z(n12) );
  GTECH_NOT U20 ( .A(n13), .Z(n96) );
  GTECH_AOI222 U21 ( .A(N54), .B(n3), .C(n4), .D(timer_count_r[23]), .E(
        wdata_i[23]), .F(n5), .Z(n13) );
  GTECH_NOT U22 ( .A(n14), .Z(n97) );
  GTECH_AOI222 U23 ( .A(N53), .B(n3), .C(n4), .D(timer_count_r[22]), .E(
        wdata_i[22]), .F(n5), .Z(n14) );
  GTECH_NOT U24 ( .A(n15), .Z(n98) );
  GTECH_AOI222 U25 ( .A(N52), .B(n3), .C(n4), .D(timer_count_r[21]), .E(
        wdata_i[21]), .F(n5), .Z(n15) );
  GTECH_NOT U26 ( .A(n16), .Z(n99) );
  GTECH_AOI222 U27 ( .A(N51), .B(n3), .C(n4), .D(timer_count_r[20]), .E(
        wdata_i[20]), .F(n5), .Z(n16) );
  GTECH_NOT U28 ( .A(n17), .Z(n100) );
  GTECH_AOI222 U29 ( .A(N50), .B(n3), .C(n4), .D(timer_count_r[19]), .E(
        wdata_i[19]), .F(n5), .Z(n17) );
  GTECH_NOT U30 ( .A(n18), .Z(n101) );
  GTECH_AOI222 U31 ( .A(N49), .B(n3), .C(n4), .D(timer_count_r[18]), .E(
        wdata_i[18]), .F(n5), .Z(n18) );
  GTECH_NOT U32 ( .A(n19), .Z(n102) );
  GTECH_AOI222 U33 ( .A(N48), .B(n3), .C(n4), .D(timer_count_r[17]), .E(
        wdata_i[17]), .F(n5), .Z(n19) );
  GTECH_NOT U34 ( .A(n20), .Z(n103) );
  GTECH_AOI222 U35 ( .A(N47), .B(n3), .C(n4), .D(timer_count_r[16]), .E(
        wdata_i[16]), .F(n5), .Z(n20) );
  GTECH_NOT U36 ( .A(n21), .Z(n104) );
  GTECH_AOI222 U37 ( .A(N46), .B(n3), .C(n4), .D(timer_count_r[15]), .E(
        wdata_i[15]), .F(n5), .Z(n21) );
  GTECH_NOT U38 ( .A(n22), .Z(n105) );
  GTECH_AOI222 U39 ( .A(N45), .B(n3), .C(n4), .D(timer_count_r[14]), .E(
        wdata_i[14]), .F(n5), .Z(n22) );
  GTECH_NOT U40 ( .A(n23), .Z(n106) );
  GTECH_AOI222 U41 ( .A(N44), .B(n3), .C(n4), .D(timer_count_r[13]), .E(
        wdata_i[13]), .F(n5), .Z(n23) );
  GTECH_NOT U42 ( .A(n24), .Z(n107) );
  GTECH_AOI222 U43 ( .A(N43), .B(n3), .C(n4), .D(timer_count_r[12]), .E(
        wdata_i[12]), .F(n5), .Z(n24) );
  GTECH_NOT U44 ( .A(n25), .Z(n108) );
  GTECH_AOI222 U45 ( .A(N42), .B(n3), .C(n4), .D(timer_count_r[11]), .E(
        wdata_i[11]), .F(n5), .Z(n25) );
  GTECH_NOT U46 ( .A(n26), .Z(n109) );
  GTECH_AOI222 U47 ( .A(N41), .B(n3), .C(n4), .D(timer_count_r[10]), .E(
        wdata_i[10]), .F(n5), .Z(n26) );
  GTECH_NOT U48 ( .A(n27), .Z(n110) );
  GTECH_AOI222 U49 ( .A(N40), .B(n3), .C(n4), .D(timer_count_r[9]), .E(
        wdata_i[9]), .F(n5), .Z(n27) );
  GTECH_NOT U50 ( .A(n28), .Z(n111) );
  GTECH_AOI222 U51 ( .A(N39), .B(n3), .C(n4), .D(timer_count_r[8]), .E(
        wdata_i[8]), .F(n5), .Z(n28) );
  GTECH_NOT U52 ( .A(n29), .Z(n112) );
  GTECH_AOI222 U53 ( .A(N38), .B(n3), .C(n4), .D(timer_count_r[7]), .E(
        wdata_i[7]), .F(n5), .Z(n29) );
  GTECH_NOT U54 ( .A(n30), .Z(n113) );
  GTECH_AOI222 U55 ( .A(N37), .B(n3), .C(n4), .D(timer_count_r[6]), .E(
        wdata_i[6]), .F(n5), .Z(n30) );
  GTECH_NOT U56 ( .A(n31), .Z(n114) );
  GTECH_AOI222 U57 ( .A(N36), .B(n3), .C(n4), .D(timer_count_r[5]), .E(
        wdata_i[5]), .F(n5), .Z(n31) );
  GTECH_NOT U58 ( .A(n32), .Z(n150) );
  GTECH_AOI222 U59 ( .A(N35), .B(n3), .C(n4), .D(timer_count_r[4]), .E(
        wdata_i[4]), .F(n5), .Z(n32) );
  GTECH_NOT U60 ( .A(n33), .Z(n151) );
  GTECH_AOI222 U61 ( .A(N34), .B(n3), .C(n4), .D(timer_count_r[3]), .E(
        wdata_i[3]), .F(n5), .Z(n33) );
  GTECH_NOT U62 ( .A(n34), .Z(n152) );
  GTECH_AOI222 U63 ( .A(N33), .B(n3), .C(n4), .D(timer_count_r[2]), .E(
        wdata_i[2]), .F(n5), .Z(n34) );
  GTECH_NOT U64 ( .A(n35), .Z(n153) );
  GTECH_AOI222 U65 ( .A(N32), .B(n3), .C(n4), .D(timer_count_r[1]), .E(
        wdata_i[1]), .F(n5), .Z(n35) );
  GTECH_NOT U66 ( .A(n36), .Z(n154) );
  GTECH_AOI222 U67 ( .A(N31), .B(n3), .C(n4), .D(timer_count_r[0]), .E(
        wdata_i[0]), .F(n5), .Z(n36) );
  GTECH_NOR4 U68 ( .A(n37), .B(n5), .C(n3), .D(reset_i), .Z(n4) );
  GTECH_AND_NOT U69 ( .A(n38), .B(reset_i), .Z(n5) );
  GTECH_NOR4 U70 ( .A(n38), .B(n39), .C(n115), .D(reset_i), .Z(n3) );
  GTECH_NOR3 U71 ( .A(n40), .B(addr_i[3]), .C(n41), .Z(n38) );
  GTECH_AND2 U72 ( .A(n42), .B(n43), .Z(n149) );
  GTECH_AO21 U73 ( .A(wdata_i[0]), .B(n44), .C(n45), .Z(n42) );
  GTECH_AOI21 U74 ( .A(wdata_i[1]), .B(n44), .C(n115), .Z(n45) );
  GTECH_AO22 U75 ( .A(timer_compare_r[31]), .B(n46), .C(wdata_i[31]), .D(n47), 
        .Z(n148) );
  GTECH_AO22 U76 ( .A(timer_compare_r[30]), .B(n46), .C(wdata_i[30]), .D(n47), 
        .Z(n147) );
  GTECH_AO22 U77 ( .A(timer_compare_r[29]), .B(n46), .C(wdata_i[29]), .D(n47), 
        .Z(n146) );
  GTECH_AO22 U78 ( .A(timer_compare_r[28]), .B(n46), .C(wdata_i[28]), .D(n47), 
        .Z(n145) );
  GTECH_AO22 U79 ( .A(timer_compare_r[27]), .B(n46), .C(wdata_i[27]), .D(n47), 
        .Z(n144) );
  GTECH_AO22 U80 ( .A(timer_compare_r[26]), .B(n46), .C(wdata_i[26]), .D(n47), 
        .Z(n143) );
  GTECH_AO22 U81 ( .A(timer_compare_r[25]), .B(n46), .C(wdata_i[25]), .D(n47), 
        .Z(n142) );
  GTECH_AO22 U82 ( .A(timer_compare_r[24]), .B(n46), .C(wdata_i[24]), .D(n47), 
        .Z(n141) );
  GTECH_AO22 U83 ( .A(timer_compare_r[23]), .B(n46), .C(wdata_i[23]), .D(n47), 
        .Z(n140) );
  GTECH_AO22 U84 ( .A(timer_compare_r[22]), .B(n46), .C(wdata_i[22]), .D(n47), 
        .Z(n139) );
  GTECH_AO22 U85 ( .A(timer_compare_r[21]), .B(n46), .C(wdata_i[21]), .D(n47), 
        .Z(n138) );
  GTECH_AO22 U86 ( .A(timer_compare_r[20]), .B(n46), .C(wdata_i[20]), .D(n47), 
        .Z(n137) );
  GTECH_AO22 U87 ( .A(timer_compare_r[19]), .B(n46), .C(wdata_i[19]), .D(n47), 
        .Z(n136) );
  GTECH_AO22 U88 ( .A(timer_compare_r[18]), .B(n46), .C(wdata_i[18]), .D(n47), 
        .Z(n135) );
  GTECH_AO22 U89 ( .A(timer_compare_r[17]), .B(n46), .C(wdata_i[17]), .D(n47), 
        .Z(n134) );
  GTECH_AO22 U90 ( .A(timer_compare_r[16]), .B(n46), .C(wdata_i[16]), .D(n47), 
        .Z(n133) );
  GTECH_AO22 U91 ( .A(timer_compare_r[15]), .B(n46), .C(wdata_i[15]), .D(n47), 
        .Z(n132) );
  GTECH_AO22 U92 ( .A(timer_compare_r[14]), .B(n46), .C(wdata_i[14]), .D(n47), 
        .Z(n131) );
  GTECH_AO22 U93 ( .A(timer_compare_r[13]), .B(n46), .C(wdata_i[13]), .D(n47), 
        .Z(n130) );
  GTECH_AO22 U94 ( .A(timer_compare_r[12]), .B(n46), .C(wdata_i[12]), .D(n47), 
        .Z(n129) );
  GTECH_AO22 U95 ( .A(timer_compare_r[11]), .B(n46), .C(wdata_i[11]), .D(n47), 
        .Z(n128) );
  GTECH_AO22 U96 ( .A(timer_compare_r[10]), .B(n46), .C(wdata_i[10]), .D(n47), 
        .Z(n127) );
  GTECH_AO22 U97 ( .A(timer_compare_r[9]), .B(n46), .C(wdata_i[9]), .D(n47), 
        .Z(n126) );
  GTECH_AO22 U98 ( .A(timer_compare_r[8]), .B(n46), .C(wdata_i[8]), .D(n47), 
        .Z(n125) );
  GTECH_AO22 U99 ( .A(timer_compare_r[7]), .B(n46), .C(wdata_i[7]), .D(n47), 
        .Z(n124) );
  GTECH_AO22 U100 ( .A(timer_compare_r[6]), .B(n46), .C(wdata_i[6]), .D(n47), 
        .Z(n123) );
  GTECH_AO22 U101 ( .A(timer_compare_r[5]), .B(n46), .C(wdata_i[5]), .D(n47), 
        .Z(n122) );
  GTECH_AO22 U102 ( .A(timer_compare_r[4]), .B(n46), .C(wdata_i[4]), .D(n47), 
        .Z(n121) );
  GTECH_AO22 U103 ( .A(timer_compare_r[3]), .B(n46), .C(wdata_i[3]), .D(n47), 
        .Z(n120) );
  GTECH_AO22 U104 ( .A(wdata_i[2]), .B(n47), .C(timer_compare_r[2]), .D(n46), 
        .Z(n119) );
  GTECH_AO22 U105 ( .A(wdata_i[1]), .B(n47), .C(timer_compare_r[1]), .D(n46), 
        .Z(n118) );
  GTECH_AO22 U106 ( .A(wdata_i[0]), .B(n47), .C(timer_compare_r[0]), .D(n46), 
        .Z(n117) );
  GTECH_NOR2 U107 ( .A(reset_i), .B(n47), .Z(n46) );
  GTECH_AND4 U108 ( .A(addr_i[3]), .B(n48), .C(n41), .D(n43), .Z(n47) );
  GTECH_NOT U109 ( .A(reset_i), .Z(n43) );
  GTECH_NOT U110 ( .A(addr_i[2]), .Z(n41) );
  GTECH_NOT U111 ( .A(n40), .Z(n48) );
  GTECH_OA21 U112 ( .A(n37), .B(csr_w[2]), .C(n49), .Z(n116) );
  GTECH_AOI21 U113 ( .A(wdata_i[2]), .B(n44), .C(reset_i), .Z(n49) );
  GTECH_NOR3 U114 ( .A(addr_i[2]), .B(addr_i[3]), .C(n40), .Z(n44) );
  GTECH_NAND3 U115 ( .A(n50), .B(wr_i), .C(n51), .Z(n40) );
  GTECH_NOR4 U116 ( .A(addr_i[7]), .B(addr_i[6]), .C(addr_i[5]), .D(addr_i[4]), 
        .Z(n51) );
  GTECH_NOR2 U117 ( .A(addr_i[1]), .B(addr_i[0]), .Z(n50) );
  GTECH_AND_NOT U118 ( .A(n39), .B(n115), .Z(n37) );
  GTECH_AND4 U119 ( .A(n52), .B(n53), .C(n54), .D(n55), .Z(n39) );
  GTECH_AND8 U120 ( .A(n56), .B(n57), .C(n58), .D(n59), .E(n60), .F(n61), .G(
        n62), .H(n63), .Z(n55) );
  GTECH_XNOR2 U121 ( .A(timer_compare_r[30]), .B(timer_count_r[30]), .Z(n63)
         );
  GTECH_XNOR2 U122 ( .A(timer_compare_r[29]), .B(timer_count_r[29]), .Z(n62)
         );
  GTECH_XNOR2 U123 ( .A(timer_compare_r[28]), .B(timer_count_r[28]), .Z(n61)
         );
  GTECH_XNOR2 U124 ( .A(timer_compare_r[27]), .B(timer_count_r[27]), .Z(n60)
         );
  GTECH_XNOR2 U125 ( .A(timer_compare_r[26]), .B(timer_count_r[26]), .Z(n59)
         );
  GTECH_XNOR2 U126 ( .A(timer_compare_r[25]), .B(timer_count_r[25]), .Z(n58)
         );
  GTECH_XNOR2 U127 ( .A(timer_compare_r[24]), .B(timer_count_r[24]), .Z(n57)
         );
  GTECH_XNOR2 U128 ( .A(timer_compare_r[23]), .B(timer_count_r[23]), .Z(n56)
         );
  GTECH_AND8 U129 ( .A(n64), .B(n65), .C(n66), .D(n67), .E(n68), .F(n69), .G(
        n70), .H(n71), .Z(n54) );
  GTECH_XNOR2 U130 ( .A(timer_compare_r[22]), .B(timer_count_r[22]), .Z(n71)
         );
  GTECH_XNOR2 U131 ( .A(timer_compare_r[21]), .B(timer_count_r[21]), .Z(n70)
         );
  GTECH_XNOR2 U132 ( .A(timer_compare_r[20]), .B(timer_count_r[20]), .Z(n69)
         );
  GTECH_XNOR2 U133 ( .A(timer_compare_r[19]), .B(timer_count_r[19]), .Z(n68)
         );
  GTECH_XNOR2 U134 ( .A(timer_compare_r[18]), .B(timer_count_r[18]), .Z(n67)
         );
  GTECH_XNOR2 U135 ( .A(timer_compare_r[17]), .B(timer_count_r[17]), .Z(n66)
         );
  GTECH_XNOR2 U136 ( .A(timer_compare_r[16]), .B(timer_count_r[16]), .Z(n65)
         );
  GTECH_XNOR2 U137 ( .A(timer_compare_r[15]), .B(timer_count_r[15]), .Z(n64)
         );
  GTECH_AND8 U138 ( .A(n72), .B(n73), .C(n74), .D(n75), .E(n76), .F(n77), .G(
        n78), .H(n79), .Z(n53) );
  GTECH_XNOR2 U139 ( .A(timer_compare_r[14]), .B(timer_count_r[14]), .Z(n79)
         );
  GTECH_XNOR2 U140 ( .A(timer_compare_r[13]), .B(timer_count_r[13]), .Z(n78)
         );
  GTECH_XNOR2 U141 ( .A(timer_compare_r[12]), .B(timer_count_r[12]), .Z(n77)
         );
  GTECH_XNOR2 U142 ( .A(timer_compare_r[11]), .B(timer_count_r[11]), .Z(n76)
         );
  GTECH_XNOR2 U143 ( .A(timer_compare_r[10]), .B(timer_count_r[10]), .Z(n75)
         );
  GTECH_XNOR2 U144 ( .A(timer_compare_r[9]), .B(timer_count_r[9]), .Z(n74) );
  GTECH_XNOR2 U145 ( .A(timer_compare_r[8]), .B(timer_count_r[8]), .Z(n73) );
  GTECH_XNOR2 U146 ( .A(timer_compare_r[7]), .B(timer_count_r[7]), .Z(n72) );
  GTECH_AND8 U147 ( .A(n80), .B(n81), .C(n82), .D(n83), .E(n84), .F(n85), .G(
        n86), .H(n87), .Z(n52) );
  GTECH_XNOR2 U148 ( .A(timer_compare_r[6]), .B(timer_count_r[6]), .Z(n87) );
  GTECH_XNOR2 U149 ( .A(timer_compare_r[5]), .B(timer_count_r[5]), .Z(n86) );
  GTECH_XNOR2 U150 ( .A(timer_compare_r[4]), .B(timer_count_r[4]), .Z(n85) );
  GTECH_XNOR2 U151 ( .A(timer_compare_r[3]), .B(timer_count_r[3]), .Z(n84) );
  GTECH_XNOR2 U152 ( .A(timer_compare_r[2]), .B(timer_count_r[2]), .Z(n83) );
  GTECH_XNOR2 U153 ( .A(timer_compare_r[31]), .B(timer_count_r[31]), .Z(n82)
         );
  GTECH_XNOR2 U154 ( .A(timer_compare_r[0]), .B(timer_count_r[0]), .Z(n81) );
  GTECH_XNOR2 U155 ( .A(timer_compare_r[1]), .B(timer_count_r[1]), .Z(n80) );
endmodule


module soc ( clk_i, reset_i, lock_o, uart_rx_i, uart_tx_o, gpio_in_i, 
        gpio_out_o );
  input [31:0] gpio_in_i;
  output [31:0] gpio_out_o;
  input clk_i, reset_i, uart_rx_i;
  output lock_o, uart_tx_o;
  wire   N2, N7, N12, N18, n4, n5, n6, SYNOPSYS_UNCONNECTED_1,
         SYNOPSYS_UNCONNECTED_2, SYNOPSYS_UNCONNECTED_3,
         SYNOPSYS_UNCONNECTED_4, SYNOPSYS_UNCONNECTED_5,
         SYNOPSYS_UNCONNECTED_6, SYNOPSYS_UNCONNECTED_7,
         SYNOPSYS_UNCONNECTED_8, SYNOPSYS_UNCONNECTED_9,
         SYNOPSYS_UNCONNECTED_10, SYNOPSYS_UNCONNECTED_11,
         SYNOPSYS_UNCONNECTED_12, SYNOPSYS_UNCONNECTED_13,
         SYNOPSYS_UNCONNECTED_14, SYNOPSYS_UNCONNECTED_15,
         SYNOPSYS_UNCONNECTED_16, SYNOPSYS_UNCONNECTED_17,
         SYNOPSYS_UNCONNECTED_18, SYNOPSYS_UNCONNECTED_19,
         SYNOPSYS_UNCONNECTED_20, SYNOPSYS_UNCONNECTED_21,
         SYNOPSYS_UNCONNECTED_22, SYNOPSYS_UNCONNECTED_23,
         SYNOPSYS_UNCONNECTED_24, SYNOPSYS_UNCONNECTED_25,
         SYNOPSYS_UNCONNECTED_26, SYNOPSYS_UNCONNECTED_27,
         SYNOPSYS_UNCONNECTED_28, SYNOPSYS_UNCONNECTED_29,
         SYNOPSYS_UNCONNECTED_30, SYNOPSYS_UNCONNECTED_31,
         SYNOPSYS_UNCONNECTED_32, SYNOPSYS_UNCONNECTED_33,
         SYNOPSYS_UNCONNECTED_34, SYNOPSYS_UNCONNECTED_35,
         SYNOPSYS_UNCONNECTED_36, SYNOPSYS_UNCONNECTED_37,
         SYNOPSYS_UNCONNECTED_38, SYNOPSYS_UNCONNECTED_39,
         SYNOPSYS_UNCONNECTED_40, SYNOPSYS_UNCONNECTED_41,
         SYNOPSYS_UNCONNECTED_42, SYNOPSYS_UNCONNECTED_43,
         SYNOPSYS_UNCONNECTED_44, SYNOPSYS_UNCONNECTED_45,
         SYNOPSYS_UNCONNECTED_46, SYNOPSYS_UNCONNECTED_47,
         SYNOPSYS_UNCONNECTED_48, SYNOPSYS_UNCONNECTED_49,
         SYNOPSYS_UNCONNECTED_50, SYNOPSYS_UNCONNECTED_51,
         SYNOPSYS_UNCONNECTED_52, SYNOPSYS_UNCONNECTED_53;
  tri   clk_i;
  tri   reset_i;
  tri   lock_o;
  tri   [31:28] daddr_w;
  tri   uart_wr_w;
  tri   gpio_wr_w;
  tri   timer_wr_w;
  tri   uart_rd_w;
  tri   gpio_rd_w;
  tri   timer_rd_w;
  tri   [1:0] uart_size_w;
  tri   [1:0] gpio_size_w;
  tri   [1:0] timer_size_w;
  tri   [7:0] uart_rdata_w;
  tri   [31:0] gpio_rdata_w;
  tri   [2:0] timer_rdata_w;
  tri   [31:0] uart_wdata_w;
  tri   [31:0] gpio_wdata_w;
  tri   [31:0] timer_wdata_w;
  tri   [31:0] uart_addr_w;
  tri   [31:0] gpio_addr_w;
  tri   [31:0] timer_addr_w;
  tri   N3;
  tri   N8;
  tri   N13;
  tri   N19;
  tri   mem_wr_w;
  tri   mem_wdata_w_9_;
  tri   mem_wdata_w_8_;
  tri   mem_wdata_w_7_;
  tri   mem_wdata_w_6_;
  tri   mem_wdata_w_5_;
  tri   mem_wdata_w_4_;
  tri   mem_wdata_w_3_;
  tri   mem_wdata_w_31_;
  tri   mem_wdata_w_30_;
  tri   mem_wdata_w_2_;
  tri   mem_wdata_w_29_;
  tri   mem_wdata_w_28_;
  tri   mem_wdata_w_27_;
  tri   mem_wdata_w_26_;
  tri   mem_wdata_w_25_;
  tri   mem_wdata_w_24_;
  tri   mem_wdata_w_23_;
  tri   mem_wdata_w_22_;
  tri   mem_wdata_w_21_;
  tri   mem_wdata_w_20_;
  tri   mem_wdata_w_1_;
  tri   mem_wdata_w_19_;
  tri   mem_wdata_w_18_;
  tri   mem_wdata_w_17_;
  tri   mem_wdata_w_16_;
  tri   mem_wdata_w_15_;
  tri   mem_wdata_w_14_;
  tri   mem_wdata_w_13_;
  tri   mem_wdata_w_12_;
  tri   mem_wdata_w_11_;
  tri   mem_wdata_w_10_;
  tri   mem_wdata_w_0_;
  tri   mem_size_w_1_;
  tri   mem_size_w_0_;
  tri   mem_rdata_w_9_;
  tri   mem_rdata_w_8_;
  tri   mem_rdata_w_7_;
  tri   mem_rdata_w_6_;
  tri   mem_rdata_w_5_;
  tri   mem_rdata_w_4_;
  tri   mem_rdata_w_3_;
  tri   mem_rdata_w_31_;
  tri   mem_rdata_w_30_;
  tri   mem_rdata_w_2_;
  tri   mem_rdata_w_29_;
  tri   mem_rdata_w_28_;
  tri   mem_rdata_w_27_;
  tri   mem_rdata_w_26_;
  tri   mem_rdata_w_25_;
  tri   mem_rdata_w_24_;
  tri   mem_rdata_w_23_;
  tri   mem_rdata_w_22_;
  tri   mem_rdata_w_21_;
  tri   mem_rdata_w_20_;
  tri   mem_rdata_w_1_;
  tri   mem_rdata_w_19_;
  tri   mem_rdata_w_18_;
  tri   mem_rdata_w_17_;
  tri   mem_rdata_w_16_;
  tri   mem_rdata_w_15_;
  tri   mem_rdata_w_14_;
  tri   mem_rdata_w_13_;
  tri   mem_rdata_w_12_;
  tri   mem_rdata_w_11_;
  tri   mem_rdata_w_10_;
  tri   mem_rdata_w_0_;
  tri   mem_rd_w;
  tri   mem_addr_w_9_;
  tri   mem_addr_w_8_;
  tri   mem_addr_w_7_;
  tri   mem_addr_w_6_;
  tri   mem_addr_w_5_;
  tri   mem_addr_w_4_;
  tri   mem_addr_w_3_;
  tri   mem_addr_w_31_;
  tri   mem_addr_w_30_;
  tri   mem_addr_w_2_;
  tri   mem_addr_w_29_;
  tri   mem_addr_w_28_;
  tri   mem_addr_w_27_;
  tri   mem_addr_w_26_;
  tri   mem_addr_w_25_;
  tri   mem_addr_w_24_;
  tri   mem_addr_w_23_;
  tri   mem_addr_w_22_;
  tri   mem_addr_w_21_;
  tri   mem_addr_w_20_;
  tri   mem_addr_w_1_;
  tri   mem_addr_w_19_;
  tri   mem_addr_w_18_;
  tri   mem_addr_w_17_;
  tri   mem_addr_w_16_;
  tri   mem_addr_w_15_;
  tri   mem_addr_w_14_;
  tri   mem_addr_w_13_;
  tri   mem_addr_w_12_;
  tri   mem_addr_w_11_;
  tri   mem_addr_w_10_;
  tri   mem_addr_w_0_;
  tri   irdata_w_9_;
  tri   irdata_w_8_;
  tri   irdata_w_7_;
  tri   irdata_w_6_;
  tri   irdata_w_5_;
  tri   irdata_w_4_;
  tri   irdata_w_3_;
  tri   irdata_w_31_;
  tri   irdata_w_30_;
  tri   irdata_w_2_;
  tri   irdata_w_29_;
  tri   irdata_w_28_;
  tri   irdata_w_27_;
  tri   irdata_w_26_;
  tri   irdata_w_25_;
  tri   irdata_w_24_;
  tri   irdata_w_23_;
  tri   irdata_w_22_;
  tri   irdata_w_21_;
  tri   irdata_w_20_;
  tri   irdata_w_1_;
  tri   irdata_w_19_;
  tri   irdata_w_18_;
  tri   irdata_w_17_;
  tri   irdata_w_16_;
  tri   irdata_w_15_;
  tri   irdata_w_14_;
  tri   irdata_w_13_;
  tri   irdata_w_12_;
  tri   irdata_w_11_;
  tri   irdata_w_10_;
  tri   irdata_w_0_;
  tri   ird_w;
  tri   iaddr_w_9_;
  tri   iaddr_w_8_;
  tri   iaddr_w_7_;
  tri   iaddr_w_6_;
  tri   iaddr_w_5_;
  tri   iaddr_w_4_;
  tri   iaddr_w_3_;
  tri   iaddr_w_31_;
  tri   iaddr_w_30_;
  tri   iaddr_w_2_;
  tri   iaddr_w_29_;
  tri   iaddr_w_28_;
  tri   iaddr_w_27_;
  tri   iaddr_w_26_;
  tri   iaddr_w_25_;
  tri   iaddr_w_24_;
  tri   iaddr_w_23_;
  tri   iaddr_w_22_;
  tri   iaddr_w_21_;
  tri   iaddr_w_20_;
  tri   iaddr_w_1_;
  tri   iaddr_w_19_;
  tri   iaddr_w_18_;
  tri   iaddr_w_17_;
  tri   iaddr_w_16_;
  tri   iaddr_w_15_;
  tri   iaddr_w_14_;
  tri   iaddr_w_13_;
  tri   iaddr_w_12_;
  tri   iaddr_w_11_;
  tri   iaddr_w_10_;
  tri   iaddr_w_0_;
  tri   dwr_w;
  tri   dwdata_w_9_;
  tri   dwdata_w_8_;
  tri   dwdata_w_7_;
  tri   dwdata_w_6_;
  tri   dwdata_w_5_;
  tri   dwdata_w_4_;
  tri   dwdata_w_3_;
  tri   dwdata_w_31_;
  tri   dwdata_w_30_;
  tri   dwdata_w_2_;
  tri   dwdata_w_29_;
  tri   dwdata_w_28_;
  tri   dwdata_w_27_;
  tri   dwdata_w_26_;
  tri   dwdata_w_25_;
  tri   dwdata_w_24_;
  tri   dwdata_w_23_;
  tri   dwdata_w_22_;
  tri   dwdata_w_21_;
  tri   dwdata_w_20_;
  tri   dwdata_w_1_;
  tri   dwdata_w_19_;
  tri   dwdata_w_18_;
  tri   dwdata_w_17_;
  tri   dwdata_w_16_;
  tri   dwdata_w_15_;
  tri   dwdata_w_14_;
  tri   dwdata_w_13_;
  tri   dwdata_w_12_;
  tri   dwdata_w_11_;
  tri   dwdata_w_10_;
  tri   dwdata_w_0_;
  tri   dsize_w_1_;
  tri   dsize_w_0_;
  tri   drdata_w_9_;
  tri   drdata_w_8_;
  tri   drdata_w_7_;
  tri   drdata_w_6_;
  tri   drdata_w_5_;
  tri   drdata_w_4_;
  tri   drdata_w_3_;
  tri   drdata_w_31_;
  tri   drdata_w_30_;
  tri   drdata_w_2_;
  tri   drdata_w_29_;
  tri   drdata_w_28_;
  tri   drdata_w_27_;
  tri   drdata_w_26_;
  tri   drdata_w_25_;
  tri   drdata_w_24_;
  tri   drdata_w_23_;
  tri   drdata_w_22_;
  tri   drdata_w_21_;
  tri   drdata_w_20_;
  tri   drdata_w_1_;
  tri   drdata_w_19_;
  tri   drdata_w_18_;
  tri   drdata_w_17_;
  tri   drdata_w_16_;
  tri   drdata_w_15_;
  tri   drdata_w_14_;
  tri   drdata_w_13_;
  tri   drdata_w_12_;
  tri   drdata_w_11_;
  tri   drdata_w_10_;
  tri   drdata_w_0_;
  tri   drd_w;
  tri   daddr_w_9_;
  tri   daddr_w_8_;
  tri   daddr_w_7_;
  tri   daddr_w_6_;
  tri   daddr_w_5_;
  tri   daddr_w_4_;
  tri   daddr_w_3_;
  tri   daddr_w_2_;
  tri   daddr_w_27_;
  tri   daddr_w_26_;
  tri   daddr_w_25_;
  tri   daddr_w_24_;
  tri   daddr_w_23_;
  tri   daddr_w_22_;
  tri   daddr_w_21_;
  tri   daddr_w_20_;
  tri   daddr_w_1_;
  tri   daddr_w_19_;
  tri   daddr_w_18_;
  tri   daddr_w_17_;
  tri   daddr_w_16_;
  tri   daddr_w_15_;
  tri   daddr_w_14_;
  tri   daddr_w_13_;
  tri   daddr_w_12_;
  tri   daddr_w_11_;
  tri   daddr_w_10_;
  tri   daddr_w_0_;
  tri   n7;
  tri   n8;
  tri   n9;
  tri   n10;
  tri   n11;
  tri   n12;
  tri   n13;
  tri   n14;
  tri   n15;
  tri   n16;
  tri   n17;
  tri   n18;
  tri   n19;
  tri   n20;
  tri   n21;
  tri   n22;
  tri   n23;
  tri   n24;
  tri   n25;
  tri   n26;
  tri   n27;
  tri   n28;
  tri   n29;
  tri   n30;
  tri   n31;
  tri   n32;
  tri   n33;
  tri   n34;
  tri   n35;
  tri   n36;
  tri   n37;
  tri   n38;
  tri   n39;
  tri   n40;
  tri   n41;
  tri   n42;
  tri   n43;
  tri   n44;
  tri   n45;
  tri   n46;
  tri   n47;
  tri   n48;
  tri   n49;
  tri   n50;
  tri   n51;
  tri   n52;
  tri   n53;
  tri   n54;
  tri   n55;
  tri   n56;
  tri   n57;
  tri   n58;
  tri   n59;

  riscv_core core_i ( .clk_i(clk_i), .reset_i(reset_i), .lock_o(lock_o), 
        .iaddr_o({iaddr_w_31_, iaddr_w_30_, iaddr_w_29_, iaddr_w_28_, 
        iaddr_w_27_, iaddr_w_26_, iaddr_w_25_, iaddr_w_24_, iaddr_w_23_, 
        iaddr_w_22_, iaddr_w_21_, iaddr_w_20_, iaddr_w_19_, iaddr_w_18_, 
        iaddr_w_17_, iaddr_w_16_, iaddr_w_15_, iaddr_w_14_, iaddr_w_13_, 
        iaddr_w_12_, iaddr_w_11_, iaddr_w_10_, iaddr_w_9_, iaddr_w_8_, 
        iaddr_w_7_, iaddr_w_6_, iaddr_w_5_, iaddr_w_4_, iaddr_w_3_, iaddr_w_2_, 
        iaddr_w_1_, iaddr_w_0_}), .irdata_i({irdata_w_31_, irdata_w_30_, 
        irdata_w_29_, irdata_w_28_, irdata_w_27_, irdata_w_26_, irdata_w_25_, 
        irdata_w_24_, irdata_w_23_, irdata_w_22_, irdata_w_21_, irdata_w_20_, 
        irdata_w_19_, irdata_w_18_, irdata_w_17_, irdata_w_16_, irdata_w_15_, 
        irdata_w_14_, irdata_w_13_, irdata_w_12_, irdata_w_11_, irdata_w_10_, 
        irdata_w_9_, irdata_w_8_, irdata_w_7_, irdata_w_6_, irdata_w_5_, 
        irdata_w_4_, irdata_w_3_, irdata_w_2_, irdata_w_1_, irdata_w_0_}), 
        .ird_o(ird_w), .daddr_o({daddr_w, daddr_w_27_, daddr_w_26_, 
        daddr_w_25_, daddr_w_24_, daddr_w_23_, daddr_w_22_, daddr_w_21_, 
        daddr_w_20_, daddr_w_19_, daddr_w_18_, daddr_w_17_, daddr_w_16_, 
        daddr_w_15_, daddr_w_14_, daddr_w_13_, daddr_w_12_, daddr_w_11_, 
        daddr_w_10_, daddr_w_9_, daddr_w_8_, daddr_w_7_, daddr_w_6_, 
        daddr_w_5_, daddr_w_4_, daddr_w_3_, daddr_w_2_, daddr_w_1_, daddr_w_0_}), .dwdata_o({dwdata_w_31_, dwdata_w_30_, dwdata_w_29_, dwdata_w_28_, 
        dwdata_w_27_, dwdata_w_26_, dwdata_w_25_, dwdata_w_24_, dwdata_w_23_, 
        dwdata_w_22_, dwdata_w_21_, dwdata_w_20_, dwdata_w_19_, dwdata_w_18_, 
        dwdata_w_17_, dwdata_w_16_, dwdata_w_15_, dwdata_w_14_, dwdata_w_13_, 
        dwdata_w_12_, dwdata_w_11_, dwdata_w_10_, dwdata_w_9_, dwdata_w_8_, 
        dwdata_w_7_, dwdata_w_6_, dwdata_w_5_, dwdata_w_4_, dwdata_w_3_, 
        dwdata_w_2_, dwdata_w_1_, dwdata_w_0_}), .drdata_i({drdata_w_31_, 
        drdata_w_30_, drdata_w_29_, drdata_w_28_, drdata_w_27_, drdata_w_26_, 
        drdata_w_25_, drdata_w_24_, drdata_w_23_, drdata_w_22_, drdata_w_21_, 
        drdata_w_20_, drdata_w_19_, drdata_w_18_, drdata_w_17_, drdata_w_16_, 
        drdata_w_15_, drdata_w_14_, drdata_w_13_, drdata_w_12_, drdata_w_11_, 
        drdata_w_10_, drdata_w_9_, drdata_w_8_, drdata_w_7_, drdata_w_6_, 
        drdata_w_5_, drdata_w_4_, drdata_w_3_, drdata_w_2_, drdata_w_1_, 
        drdata_w_0_}), .dsize_o({dsize_w_1_, dsize_w_0_}), .drd_o(drd_w), 
        .dwr_o(dwr_w) );
  bus_mux bus_mux_i ( .clk_i(clk_i), .reset_i(reset_i), .ss_i({N3, N8, N13, 
        N19}), .m_addr_i({daddr_w, daddr_w_27_, daddr_w_26_, daddr_w_25_, 
        daddr_w_24_, daddr_w_23_, daddr_w_22_, daddr_w_21_, daddr_w_20_, 
        daddr_w_19_, daddr_w_18_, daddr_w_17_, daddr_w_16_, daddr_w_15_, 
        daddr_w_14_, daddr_w_13_, daddr_w_12_, daddr_w_11_, daddr_w_10_, 
        daddr_w_9_, daddr_w_8_, daddr_w_7_, daddr_w_6_, daddr_w_5_, daddr_w_4_, 
        daddr_w_3_, daddr_w_2_, daddr_w_1_, daddr_w_0_}), .m_wdata_i({
        dwdata_w_31_, dwdata_w_30_, dwdata_w_29_, dwdata_w_28_, dwdata_w_27_, 
        dwdata_w_26_, dwdata_w_25_, dwdata_w_24_, dwdata_w_23_, dwdata_w_22_, 
        dwdata_w_21_, dwdata_w_20_, dwdata_w_19_, dwdata_w_18_, dwdata_w_17_, 
        dwdata_w_16_, dwdata_w_15_, dwdata_w_14_, dwdata_w_13_, dwdata_w_12_, 
        dwdata_w_11_, dwdata_w_10_, dwdata_w_9_, dwdata_w_8_, dwdata_w_7_, 
        dwdata_w_6_, dwdata_w_5_, dwdata_w_4_, dwdata_w_3_, dwdata_w_2_, 
        dwdata_w_1_, dwdata_w_0_}), .m_rdata_o({drdata_w_31_, drdata_w_30_, 
        drdata_w_29_, drdata_w_28_, drdata_w_27_, drdata_w_26_, drdata_w_25_, 
        drdata_w_24_, drdata_w_23_, drdata_w_22_, drdata_w_21_, drdata_w_20_, 
        drdata_w_19_, drdata_w_18_, drdata_w_17_, drdata_w_16_, drdata_w_15_, 
        drdata_w_14_, drdata_w_13_, drdata_w_12_, drdata_w_11_, drdata_w_10_, 
        drdata_w_9_, drdata_w_8_, drdata_w_7_, drdata_w_6_, drdata_w_5_, 
        drdata_w_4_, drdata_w_3_, drdata_w_2_, drdata_w_1_, drdata_w_0_}), 
        .m_size_i({dsize_w_1_, dsize_w_0_}), .m_rd_i(drd_w), .m_wr_i(dwr_w), 
        .s_addr_o({mem_addr_w_31_, mem_addr_w_30_, mem_addr_w_29_, 
        mem_addr_w_28_, mem_addr_w_27_, mem_addr_w_26_, mem_addr_w_25_, 
        mem_addr_w_24_, mem_addr_w_23_, mem_addr_w_22_, mem_addr_w_21_, 
        mem_addr_w_20_, mem_addr_w_19_, mem_addr_w_18_, mem_addr_w_17_, 
        mem_addr_w_16_, mem_addr_w_15_, mem_addr_w_14_, mem_addr_w_13_, 
        mem_addr_w_12_, mem_addr_w_11_, mem_addr_w_10_, mem_addr_w_9_, 
        mem_addr_w_8_, mem_addr_w_7_, mem_addr_w_6_, mem_addr_w_5_, 
        mem_addr_w_4_, mem_addr_w_3_, mem_addr_w_2_, mem_addr_w_1_, 
        mem_addr_w_0_, uart_addr_w, gpio_addr_w, timer_addr_w}), .s_wdata_o({
        mem_wdata_w_31_, mem_wdata_w_30_, mem_wdata_w_29_, mem_wdata_w_28_, 
        mem_wdata_w_27_, mem_wdata_w_26_, mem_wdata_w_25_, mem_wdata_w_24_, 
        mem_wdata_w_23_, mem_wdata_w_22_, mem_wdata_w_21_, mem_wdata_w_20_, 
        mem_wdata_w_19_, mem_wdata_w_18_, mem_wdata_w_17_, mem_wdata_w_16_, 
        mem_wdata_w_15_, mem_wdata_w_14_, mem_wdata_w_13_, mem_wdata_w_12_, 
        mem_wdata_w_11_, mem_wdata_w_10_, mem_wdata_w_9_, mem_wdata_w_8_, 
        mem_wdata_w_7_, mem_wdata_w_6_, mem_wdata_w_5_, mem_wdata_w_4_, 
        mem_wdata_w_3_, mem_wdata_w_2_, mem_wdata_w_1_, mem_wdata_w_0_, 
        uart_wdata_w, gpio_wdata_w, timer_wdata_w}), .s_rdata_i({
        mem_rdata_w_31_, mem_rdata_w_30_, mem_rdata_w_29_, mem_rdata_w_28_, 
        mem_rdata_w_27_, mem_rdata_w_26_, mem_rdata_w_25_, mem_rdata_w_24_, 
        mem_rdata_w_23_, mem_rdata_w_22_, mem_rdata_w_21_, mem_rdata_w_20_, 
        mem_rdata_w_19_, mem_rdata_w_18_, mem_rdata_w_17_, mem_rdata_w_16_, 
        mem_rdata_w_15_, mem_rdata_w_14_, mem_rdata_w_13_, mem_rdata_w_12_, 
        mem_rdata_w_11_, mem_rdata_w_10_, mem_rdata_w_9_, mem_rdata_w_8_, 
        mem_rdata_w_7_, mem_rdata_w_6_, mem_rdata_w_5_, mem_rdata_w_4_, 
        mem_rdata_w_3_, mem_rdata_w_2_, mem_rdata_w_1_, mem_rdata_w_0_, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        uart_rdata_w, gpio_rdata_w, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        timer_rdata_w}), .s_size_o({mem_size_w_1_, mem_size_w_0_, uart_size_w, 
        gpio_size_w, timer_size_w}), .s_rd_o({mem_rd_w, uart_rd_w, gpio_rd_w, 
        timer_rd_w}), .s_wr_o({mem_wr_w, uart_wr_w, gpio_wr_w, timer_wr_w}) );
  memory memory_i ( .clk_i(clk_i), .reset_i(reset_i), .iaddr_i({iaddr_w_31_, 
        iaddr_w_30_, iaddr_w_29_, iaddr_w_28_, iaddr_w_27_, iaddr_w_26_, 
        iaddr_w_25_, iaddr_w_24_, iaddr_w_23_, iaddr_w_22_, iaddr_w_21_, 
        iaddr_w_20_, iaddr_w_19_, iaddr_w_18_, iaddr_w_17_, iaddr_w_16_, 
        iaddr_w_15_, iaddr_w_14_, iaddr_w_13_, iaddr_w_12_, iaddr_w_11_, 
        iaddr_w_10_, iaddr_w_9_, iaddr_w_8_, iaddr_w_7_, iaddr_w_6_, 
        iaddr_w_5_, iaddr_w_4_, iaddr_w_3_, iaddr_w_2_, iaddr_w_1_, iaddr_w_0_}), .irdata_o({irdata_w_31_, irdata_w_30_, irdata_w_29_, irdata_w_28_, 
        irdata_w_27_, irdata_w_26_, irdata_w_25_, irdata_w_24_, irdata_w_23_, 
        irdata_w_22_, irdata_w_21_, irdata_w_20_, irdata_w_19_, irdata_w_18_, 
        irdata_w_17_, irdata_w_16_, irdata_w_15_, irdata_w_14_, irdata_w_13_, 
        irdata_w_12_, irdata_w_11_, irdata_w_10_, irdata_w_9_, irdata_w_8_, 
        irdata_w_7_, irdata_w_6_, irdata_w_5_, irdata_w_4_, irdata_w_3_, 
        irdata_w_2_, irdata_w_1_, irdata_w_0_}), .ird_i(ird_w), .daddr_i({
        mem_addr_w_31_, mem_addr_w_30_, mem_addr_w_29_, mem_addr_w_28_, 
        mem_addr_w_27_, mem_addr_w_26_, mem_addr_w_25_, mem_addr_w_24_, 
        mem_addr_w_23_, mem_addr_w_22_, mem_addr_w_21_, mem_addr_w_20_, 
        mem_addr_w_19_, mem_addr_w_18_, mem_addr_w_17_, mem_addr_w_16_, 
        mem_addr_w_15_, mem_addr_w_14_, mem_addr_w_13_, mem_addr_w_12_, 
        mem_addr_w_11_, mem_addr_w_10_, mem_addr_w_9_, mem_addr_w_8_, 
        mem_addr_w_7_, mem_addr_w_6_, mem_addr_w_5_, mem_addr_w_4_, 
        mem_addr_w_3_, mem_addr_w_2_, mem_addr_w_1_, mem_addr_w_0_}), 
        .dwdata_i({mem_wdata_w_31_, mem_wdata_w_30_, mem_wdata_w_29_, 
        mem_wdata_w_28_, mem_wdata_w_27_, mem_wdata_w_26_, mem_wdata_w_25_, 
        mem_wdata_w_24_, mem_wdata_w_23_, mem_wdata_w_22_, mem_wdata_w_21_, 
        mem_wdata_w_20_, mem_wdata_w_19_, mem_wdata_w_18_, mem_wdata_w_17_, 
        mem_wdata_w_16_, mem_wdata_w_15_, mem_wdata_w_14_, mem_wdata_w_13_, 
        mem_wdata_w_12_, mem_wdata_w_11_, mem_wdata_w_10_, mem_wdata_w_9_, 
        mem_wdata_w_8_, mem_wdata_w_7_, mem_wdata_w_6_, mem_wdata_w_5_, 
        mem_wdata_w_4_, mem_wdata_w_3_, mem_wdata_w_2_, mem_wdata_w_1_, 
        mem_wdata_w_0_}), .drdata_o({mem_rdata_w_31_, mem_rdata_w_30_, 
        mem_rdata_w_29_, mem_rdata_w_28_, mem_rdata_w_27_, mem_rdata_w_26_, 
        mem_rdata_w_25_, mem_rdata_w_24_, mem_rdata_w_23_, mem_rdata_w_22_, 
        mem_rdata_w_21_, mem_rdata_w_20_, mem_rdata_w_19_, mem_rdata_w_18_, 
        mem_rdata_w_17_, mem_rdata_w_16_, mem_rdata_w_15_, mem_rdata_w_14_, 
        mem_rdata_w_13_, mem_rdata_w_12_, mem_rdata_w_11_, mem_rdata_w_10_, 
        mem_rdata_w_9_, mem_rdata_w_8_, mem_rdata_w_7_, mem_rdata_w_6_, 
        mem_rdata_w_5_, mem_rdata_w_4_, mem_rdata_w_3_, mem_rdata_w_2_, 
        mem_rdata_w_1_, mem_rdata_w_0_}), .dsize_i({mem_size_w_1_, 
        mem_size_w_0_}), .drd_i(mem_rd_w), .dwr_i(mem_wr_w) );
  per_uart per_uart_i ( .clk_i(clk_i), .reset_i(reset_i), .addr_i(uart_addr_w), 
        .wdata_i(uart_wdata_w), .rdata_o({SYNOPSYS_UNCONNECTED_1, 
        SYNOPSYS_UNCONNECTED_2, SYNOPSYS_UNCONNECTED_3, SYNOPSYS_UNCONNECTED_4, 
        SYNOPSYS_UNCONNECTED_5, SYNOPSYS_UNCONNECTED_6, SYNOPSYS_UNCONNECTED_7, 
        SYNOPSYS_UNCONNECTED_8, SYNOPSYS_UNCONNECTED_9, 
        SYNOPSYS_UNCONNECTED_10, SYNOPSYS_UNCONNECTED_11, 
        SYNOPSYS_UNCONNECTED_12, SYNOPSYS_UNCONNECTED_13, 
        SYNOPSYS_UNCONNECTED_14, SYNOPSYS_UNCONNECTED_15, 
        SYNOPSYS_UNCONNECTED_16, SYNOPSYS_UNCONNECTED_17, 
        SYNOPSYS_UNCONNECTED_18, SYNOPSYS_UNCONNECTED_19, 
        SYNOPSYS_UNCONNECTED_20, SYNOPSYS_UNCONNECTED_21, 
        SYNOPSYS_UNCONNECTED_22, SYNOPSYS_UNCONNECTED_23, 
        SYNOPSYS_UNCONNECTED_24, uart_rdata_w}), .size_i(uart_size_w), .rd_i(
        uart_rd_w), .wr_i(uart_wr_w), .uart_rx_i(uart_rx_i), .uart_tx_o(
        uart_tx_o) );
  per_gpio per_gpio_i ( .clk_i(clk_i), .reset_i(reset_i), .addr_i(gpio_addr_w), 
        .wdata_i(gpio_wdata_w), .rdata_o(gpio_rdata_w), .size_i(gpio_size_w), 
        .rd_i(gpio_rd_w), .wr_i(gpio_wr_w), .gpio_in_i(gpio_in_i), 
        .gpio_out_o(gpio_out_o) );
  per_timer per_timer_i ( .clk_i(clk_i), .reset_i(reset_i), .addr_i(
        timer_addr_w), .wdata_i(timer_wdata_w), .rdata_o({
        SYNOPSYS_UNCONNECTED_25, SYNOPSYS_UNCONNECTED_26, 
        SYNOPSYS_UNCONNECTED_27, SYNOPSYS_UNCONNECTED_28, 
        SYNOPSYS_UNCONNECTED_29, SYNOPSYS_UNCONNECTED_30, 
        SYNOPSYS_UNCONNECTED_31, SYNOPSYS_UNCONNECTED_32, 
        SYNOPSYS_UNCONNECTED_33, SYNOPSYS_UNCONNECTED_34, 
        SYNOPSYS_UNCONNECTED_35, SYNOPSYS_UNCONNECTED_36, 
        SYNOPSYS_UNCONNECTED_37, SYNOPSYS_UNCONNECTED_38, 
        SYNOPSYS_UNCONNECTED_39, SYNOPSYS_UNCONNECTED_40, 
        SYNOPSYS_UNCONNECTED_41, SYNOPSYS_UNCONNECTED_42, 
        SYNOPSYS_UNCONNECTED_43, SYNOPSYS_UNCONNECTED_44, 
        SYNOPSYS_UNCONNECTED_45, SYNOPSYS_UNCONNECTED_46, 
        SYNOPSYS_UNCONNECTED_47, SYNOPSYS_UNCONNECTED_48, 
        SYNOPSYS_UNCONNECTED_49, SYNOPSYS_UNCONNECTED_50, 
        SYNOPSYS_UNCONNECTED_51, SYNOPSYS_UNCONNECTED_52, 
        SYNOPSYS_UNCONNECTED_53, timer_rdata_w}), .size_i(timer_size_w), 
        .rd_i(timer_rd_w), .wr_i(timer_wr_w) );
  GTECH_NAND3 U12 ( .A(n4), .B(n5), .C(daddr_w[28]), .Z(N7) );
  GTECH_NAND3 U13 ( .A(n6), .B(n5), .C(n4), .Z(N2) );
  GTECH_NOT U14 ( .A(daddr_w[29]), .Z(n5) );
  GTECH_NAND3 U15 ( .A(daddr_w[28]), .B(n4), .C(daddr_w[29]), .Z(N18) );
  GTECH_NAND3 U16 ( .A(n4), .B(n6), .C(daddr_w[29]), .Z(N12) );
  GTECH_NOT U17 ( .A(daddr_w[28]), .Z(n6) );
  GTECH_NOR2 U18 ( .A(daddr_w[30]), .B(daddr_w[31]), .Z(n4) );
  GTECH_NOT U19 ( .A(N2), .Z(N3) );
  GTECH_NOT U20 ( .A(N7), .Z(N8) );
  GTECH_NOT U21 ( .A(N12), .Z(N13) );
  GTECH_NOT U22 ( .A(N18), .Z(N19) );
endmodule


/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Mon May 10 13:09:57 2021
/////////////////////////////////////////////////////////////


module per_uart_DW01_incdec_0 ( A, INC_DEC, SUM );
  input [15:0] A;
  output [15:0] SUM;
  input INC_DEC;

  wire   [15:2] carry;

  FAHHDLX U1_15 ( .A(A[15]), .B(INC_DEC), .CI(carry[15]), .S(SUM[15]) );
  FAHHDLX U1_14 ( .A(A[14]), .B(INC_DEC), .CI(carry[14]), .CO(carry[15]), .S(
        SUM[14]) );
  FAHHDLX U1_13 ( .A(A[13]), .B(INC_DEC), .CI(carry[13]), .CO(carry[14]), .S(
        SUM[13]) );
  FAHHDLX U1_12 ( .A(A[12]), .B(INC_DEC), .CI(carry[12]), .CO(carry[13]), .S(
        SUM[12]) );
  FAHHDLX U1_11 ( .A(A[11]), .B(INC_DEC), .CI(carry[11]), .CO(carry[12]), .S(
        SUM[11]) );
  FAHHDLX U1_10 ( .A(A[10]), .B(INC_DEC), .CI(carry[10]), .CO(carry[11]), .S(
        SUM[10]) );
  FAHHDLX U1_9 ( .A(A[9]), .B(INC_DEC), .CI(carry[9]), .CO(carry[10]), .S(
        SUM[9]) );
  FAHHDLX U1_8 ( .A(A[8]), .B(INC_DEC), .CI(carry[8]), .CO(carry[9]), .S(
        SUM[8]) );
  FAHHDLX U1_7 ( .A(A[7]), .B(INC_DEC), .CI(carry[7]), .CO(carry[8]), .S(
        SUM[7]) );
  FAHHDLX U1_6 ( .A(A[6]), .B(INC_DEC), .CI(carry[6]), .CO(carry[7]), .S(
        SUM[6]) );
  FAHHDLX U1_5 ( .A(A[5]), .B(INC_DEC), .CI(carry[5]), .CO(carry[6]), .S(
        SUM[5]) );
  FAHHDLX U1_4 ( .A(A[4]), .B(INC_DEC), .CI(carry[4]), .CO(carry[5]), .S(
        SUM[4]) );
  FAHHDLX U1_3 ( .A(A[3]), .B(INC_DEC), .CI(carry[3]), .CO(carry[4]), .S(
        SUM[3]) );
  FAHHDLX U1_2 ( .A(A[2]), .B(INC_DEC), .CI(carry[2]), .CO(carry[3]), .S(
        SUM[2]) );
  FAHHDLX U1_1 ( .A(A[1]), .B(INC_DEC), .CI(A[0]), .CO(carry[2]), .S(SUM[1])
         );
  INVHD1X U1 ( .A(A[0]), .Z(SUM[0]) );
endmodule


module per_uart_DW01_inc_0 ( A, SUM );
  input [15:0] A;
  output [15:0] SUM;

  wire   [15:2] carry;

  HAHDMX U1_1_14 ( .A(A[14]), .B(carry[14]), .CO(carry[15]), .S(SUM[14]) );
  HAHDMX U1_1_13 ( .A(A[13]), .B(carry[13]), .CO(carry[14]), .S(SUM[13]) );
  HAHDMX U1_1_12 ( .A(A[12]), .B(carry[12]), .CO(carry[13]), .S(SUM[12]) );
  HAHDMX U1_1_11 ( .A(A[11]), .B(carry[11]), .CO(carry[12]), .S(SUM[11]) );
  HAHDMX U1_1_10 ( .A(A[10]), .B(carry[10]), .CO(carry[11]), .S(SUM[10]) );
  HAHDMX U1_1_9 ( .A(A[9]), .B(carry[9]), .CO(carry[10]), .S(SUM[9]) );
  HAHDMX U1_1_8 ( .A(A[8]), .B(carry[8]), .CO(carry[9]), .S(SUM[8]) );
  HAHDMX U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  HAHDMX U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  HAHDMX U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  HAHDMX U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  HAHDMX U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  HAHDMX U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  HAHDMX U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  INVHD1X U1 ( .A(A[0]), .Z(SUM[0]) );
  XOR2CLKHD1X U2 ( .A(carry[15]), .B(A[15]), .Z(SUM[15]) );
endmodule


module per_uart ( clk_i, reset_i, addr_i, wdata_i, rdata_o, size_i, rd_i, wr_i, 
        uart_rx_i, uart_tx_o );
  input [31:0] addr_i;
  input [31:0] wdata_i;
  output [31:0] rdata_o;
  input [1:0] size_i;
  input clk_i, reset_i, rd_i, wr_i, uart_rx_i;
  output uart_tx_o;
  wire   N39, N40, N41, N42, N43, N44, N45, N46, N47, N48, N49, N50, N51, N52,
         N53, N54, rx_shifter_r_8_, N159, N160, N161, N162, N163, N164, N165,
         N166, N167, N168, N169, N170, N171, N172, N173, N174, csr_w_0_, N232,
         N233, n9, n10, n11, n12, n13, n14, n75, n76, n77, n78, n79, n80, n81,
         n82, n204, n205, n206, n207, n208, n209, n210, n211, n212, n213, n214,
         n215, n216, n217, n218, n219, n220, n221, n222, n223, n224, n225,
         n226, n227, n228, n229, n230, n231, n232, n233, n234, n235, n236,
         n237, n238, n239, n240, n241, n242, n243, n244, n245, n246, n247,
         n248, n249, n250, n251, n252, n253, n254, n255, n256, n257, n1, n2,
         n3, n4, n5, n6, n7, n8, n15, n16, n17, n18, n19, n20, n21, n22, n23,
         n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37,
         n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51,
         n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65,
         n66, n67, n68, n69, n70, n71, n72, n73, n74, n83, n84, n85, n86, n87,
         n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
         n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166,
         n167, n168, n169, n170, n171, n172, n173, n174, n175, n176, n177,
         n178, n179, n180, n181, n182, n183, n184, n185, n186, n187, n188,
         n189, n190, n191, n192, n193, n194, n195, n196, n197, n198, n199,
         n200, n201, n202, n203, n258, n259, n260, n261, n262, n263, n264,
         n265, n266, n267, n268, n269, n270, n271, n272, n273, n274, n275,
         n276, n277;
  wire   [15:0] br_r;
  wire   [2:0] tx_bit_cnt_r;
  wire   [15:0] tx_br_cnt_r;
  wire   [2:0] rx_bit_cnt_r;
  wire   [15:0] rx_br_cnt_r;
  tri   clk_i;
  tri   reset_i;
  tri   [31:0] addr_i;
  tri   [31:0] wdata_i;
  tri   [31:0] rdata_o;
  tri   rd_i;
  tri   wr_i;
  assign rdata_o[31] = 1'b0;
  assign rdata_o[30] = 1'b0;
  assign rdata_o[29] = 1'b0;
  assign rdata_o[28] = 1'b0;
  assign rdata_o[27] = 1'b0;
  assign rdata_o[26] = 1'b0;
  assign rdata_o[25] = 1'b0;
  assign rdata_o[24] = 1'b0;
  assign rdata_o[23] = 1'b0;
  assign rdata_o[22] = 1'b0;
  assign rdata_o[21] = 1'b0;
  assign rdata_o[20] = 1'b0;
  assign rdata_o[19] = 1'b0;
  assign rdata_o[18] = 1'b0;
  assign rdata_o[17] = 1'b0;
  assign rdata_o[16] = 1'b0;
  assign rdata_o[15] = 1'b0;
  assign rdata_o[14] = 1'b0;
  assign rdata_o[13] = 1'b0;
  assign rdata_o[12] = 1'b0;
  assign rdata_o[11] = 1'b0;
  assign rdata_o[10] = 1'b0;
  assign rdata_o[9] = 1'b0;
  assign rdata_o[8] = 1'b0;

  per_uart_DW01_incdec_0 r118 ( .A(rx_br_cnt_r), .INC_DEC(n276), .SUM({N174, 
        N173, N172, N171, N170, N169, N168, N167, N166, N165, N164, N163, N162, 
        N161, N160, N159}) );
  per_uart_DW01_inc_0 add_102 ( .A(tx_br_cnt_r), .SUM({N54, N53, N52, N51, N50, 
        N49, N48, N47, N46, N45, N44, N43, N42, N41, N40, N39}) );
  FFDQHD1X br_r_reg_15_ ( .D(n267), .CK(clk_i), .Q(br_r[15]) );
  FFDQHD1X br_r_reg_14_ ( .D(n268), .CK(clk_i), .Q(br_r[14]) );
  FFDQHD1X br_r_reg_13_ ( .D(n269), .CK(clk_i), .Q(br_r[13]) );
  FFDQHD1X br_r_reg_12_ ( .D(n270), .CK(clk_i), .Q(br_r[12]) );
  FFDQHD1X br_r_reg_11_ ( .D(n271), .CK(clk_i), .Q(br_r[11]) );
  FFDQHD1X br_r_reg_10_ ( .D(n272), .CK(clk_i), .Q(br_r[10]) );
  FFDQHD1X br_r_reg_9_ ( .D(n273), .CK(clk_i), .Q(br_r[9]) );
  FFDQHD1X br_r_reg_8_ ( .D(n274), .CK(clk_i), .Q(br_r[8]) );
  FFDQHD1X br_r_reg_7_ ( .D(n257), .CK(clk_i), .Q(br_r[7]) );
  FFDQHD1X br_r_reg_6_ ( .D(n256), .CK(clk_i), .Q(br_r[6]) );
  FFDQHD1X br_r_reg_5_ ( .D(n255), .CK(clk_i), .Q(br_r[5]) );
  FFDQHD1X br_r_reg_4_ ( .D(n254), .CK(clk_i), .Q(br_r[4]) );
  FFDQHD1X br_r_reg_3_ ( .D(n253), .CK(clk_i), .Q(br_r[3]) );
  FFDQHD1X br_r_reg_2_ ( .D(n252), .CK(clk_i), .Q(br_r[2]) );
  FFDQHD1X br_r_reg_1_ ( .D(n251), .CK(clk_i), .Q(br_r[1]) );
  FFDQHD1X br_r_reg_0_ ( .D(n250), .CK(clk_i), .Q(br_r[0]) );
  FFDHD1X rx_shifter_r_reg_7_ ( .D(n205), .CK(clk_i), .Q(n26), .QN(n75) );
  FFDHD1X rx_shifter_r_reg_6_ ( .D(n206), .CK(clk_i), .Q(n25), .QN(n76) );
  FFDHD1X rx_shifter_r_reg_5_ ( .D(n207), .CK(clk_i), .Q(n24), .QN(n77) );
  FFDHD1X rx_shifter_r_reg_4_ ( .D(n208), .CK(clk_i), .Q(n23), .QN(n78) );
  FFDHD1X rx_shifter_r_reg_3_ ( .D(n209), .CK(clk_i), .Q(n22), .QN(n79) );
  FFDHD1X rx_shifter_r_reg_2_ ( .D(n210), .CK(clk_i), .Q(n21), .QN(n80) );
  FFDHD1X rx_shifter_r_reg_1_ ( .D(n211), .CK(clk_i), .Q(n20), .QN(n81) );
  FFDHD1X rx_shifter_r_reg_0_ ( .D(n212), .CK(clk_i), .Q(n19), .QN(n82) );
  FFDHD1X tx_shifter_r_reg_9_ ( .D(n236), .CK(clk_i), .QN(n1) );
  FFDHD1X tx_shifter_r_reg_8_ ( .D(n237), .CK(clk_i), .QN(n16) );
  FFDHD1X tx_shifter_r_reg_7_ ( .D(n238), .CK(clk_i), .QN(n5) );
  FFDHD1X tx_shifter_r_reg_6_ ( .D(n239), .CK(clk_i), .QN(n15) );
  FFDHD1X tx_shifter_r_reg_5_ ( .D(n240), .CK(clk_i), .QN(n4) );
  FFDHD1X tx_shifter_r_reg_4_ ( .D(n241), .CK(clk_i), .QN(n8) );
  FFDHD1X tx_shifter_r_reg_3_ ( .D(n242), .CK(clk_i), .QN(n3) );
  FFDHD1X tx_shifter_r_reg_2_ ( .D(n243), .CK(clk_i), .QN(n7) );
  FFDHD1X tx_shifter_r_reg_1_ ( .D(n244), .CK(clk_i), .QN(n2) );
  FFDHD1X rx_done_r_reg ( .D(n227), .CK(clk_i), .QN(n6) );
  FFDQHD1X rx_shifter_r_reg_8_ ( .D(n232), .CK(clk_i), .Q(rx_shifter_r_8_) );
  FFDQHD1X tx_shifter_r_reg_0_ ( .D(n235), .CK(clk_i), .Q(uart_tx_o) );
  FFDHD1X rx_ready_r_reg ( .D(n204), .CK(clk_i), .Q(n194) );
  FFDHD1X tx_bit_cnt_r_reg_3_ ( .D(n249), .CK(clk_i), .Q(n192), .QN(n17) );
  FFDHD1X rx_bit_cnt_r_reg_3_ ( .D(n231), .CK(clk_i), .Q(n193), .QN(n18) );
  FFEDHD1X rx_data_r_reg_1_ ( .D(n20), .E(n275), .CK(clk_i), .Q(n195) );
  FFEDHD1X rx_data_r_reg_0_ ( .D(n19), .E(n275), .CK(clk_i), .Q(n196) );
  FFDQHD1X tx_ready_r_reg ( .D(n245), .CK(clk_i), .Q(csr_w_0_) );
  FFDQHD1X tx_bit_cnt_r_reg_2_ ( .D(n246), .CK(clk_i), .Q(tx_bit_cnt_r[2]) );
  FFDQHD1X rx_bit_cnt_r_reg_2_ ( .D(n228), .CK(clk_i), .Q(rx_bit_cnt_r[2]) );
  FFDQHD1X tx_br_cnt_r_reg_15_ ( .D(n198), .CK(clk_i), .Q(tx_br_cnt_r[15]) );
  FFDQHD1X tx_br_cnt_r_reg_1_ ( .D(n266), .CK(clk_i), .Q(tx_br_cnt_r[1]) );
  FFDQHD1X tx_br_cnt_r_reg_2_ ( .D(n265), .CK(clk_i), .Q(tx_br_cnt_r[2]) );
  FFDQHD1X tx_br_cnt_r_reg_3_ ( .D(n264), .CK(clk_i), .Q(tx_br_cnt_r[3]) );
  FFDQHD1X tx_br_cnt_r_reg_4_ ( .D(n263), .CK(clk_i), .Q(tx_br_cnt_r[4]) );
  FFDQHD1X tx_br_cnt_r_reg_5_ ( .D(n262), .CK(clk_i), .Q(tx_br_cnt_r[5]) );
  FFDQHD1X tx_br_cnt_r_reg_6_ ( .D(n261), .CK(clk_i), .Q(tx_br_cnt_r[6]) );
  FFDQHD1X tx_br_cnt_r_reg_7_ ( .D(n260), .CK(clk_i), .Q(tx_br_cnt_r[7]) );
  FFDQHD1X tx_br_cnt_r_reg_8_ ( .D(n259), .CK(clk_i), .Q(tx_br_cnt_r[8]) );
  FFDQHD1X tx_br_cnt_r_reg_9_ ( .D(n258), .CK(clk_i), .Q(tx_br_cnt_r[9]) );
  FFDQHD1X tx_br_cnt_r_reg_10_ ( .D(n203), .CK(clk_i), .Q(tx_br_cnt_r[10]) );
  FFDQHD1X tx_br_cnt_r_reg_11_ ( .D(n202), .CK(clk_i), .Q(tx_br_cnt_r[11]) );
  FFDQHD1X tx_br_cnt_r_reg_12_ ( .D(n201), .CK(clk_i), .Q(tx_br_cnt_r[12]) );
  FFDQHD1X tx_br_cnt_r_reg_13_ ( .D(n200), .CK(clk_i), .Q(tx_br_cnt_r[13]) );
  FFDQHD1X tx_br_cnt_r_reg_14_ ( .D(n199), .CK(clk_i), .Q(tx_br_cnt_r[14]) );
  FFDQHD1X rx_bit_cnt_r_reg_0_ ( .D(n230), .CK(clk_i), .Q(rx_bit_cnt_r[0]) );
  FFDQHD1X tx_bit_cnt_r_reg_0_ ( .D(n248), .CK(clk_i), .Q(tx_bit_cnt_r[0]) );
  FFDQHD1X tx_bit_cnt_r_reg_1_ ( .D(n247), .CK(clk_i), .Q(tx_bit_cnt_r[1]) );
  FFDQHD1X rx_bit_cnt_r_reg_1_ ( .D(n229), .CK(clk_i), .Q(rx_bit_cnt_r[1]) );
  FFDQHD1X tx_br_cnt_r_reg_0_ ( .D(n197), .CK(clk_i), .Q(tx_br_cnt_r[0]) );
  FFDHD1X rx_br_cnt_r_reg_11_ ( .D(n216), .CK(clk_i), .Q(rx_br_cnt_r[11]) );
  FFDHD1X rx_br_cnt_r_reg_8_ ( .D(n219), .CK(clk_i), .Q(rx_br_cnt_r[8]) );
  FFDHD1X rx_br_cnt_r_reg_4_ ( .D(n223), .CK(clk_i), .Q(rx_br_cnt_r[4]) );
  FFDHD1X rx_br_cnt_r_reg_13_ ( .D(n214), .CK(clk_i), .Q(rx_br_cnt_r[13]) );
  FFDHD1X rx_br_cnt_r_reg_6_ ( .D(n221), .CK(clk_i), .Q(rx_br_cnt_r[6]) );
  FFDHD1X rx_br_cnt_r_reg_2_ ( .D(n225), .CK(clk_i), .Q(rx_br_cnt_r[2]) );
  FFDHD1X rx_br_cnt_r_reg_14_ ( .D(n213), .CK(clk_i), .Q(rx_br_cnt_r[14]) );
  FFDHD1X rx_br_cnt_r_reg_10_ ( .D(n217), .CK(clk_i), .Q(rx_br_cnt_r[10]) );
  FFDHD1X rx_br_cnt_r_reg_7_ ( .D(n220), .CK(clk_i), .Q(rx_br_cnt_r[7]) );
  FFDHD1X rx_br_cnt_r_reg_3_ ( .D(n224), .CK(clk_i), .Q(rx_br_cnt_r[3]) );
  FFDHD1X rx_br_cnt_r_reg_15_ ( .D(n233), .CK(clk_i), .Q(rx_br_cnt_r[15]) );
  FFDHD1X rx_br_cnt_r_reg_12_ ( .D(n215), .CK(clk_i), .Q(rx_br_cnt_r[12]) );
  FFDHD1X rx_br_cnt_r_reg_9_ ( .D(n218), .CK(clk_i), .Q(rx_br_cnt_r[9]) );
  FFDHD1X rx_br_cnt_r_reg_5_ ( .D(n222), .CK(clk_i), .Q(rx_br_cnt_r[5]) );
  FFDHD1X rx_br_cnt_r_reg_1_ ( .D(n226), .CK(clk_i), .Q(rx_br_cnt_r[1]) );
  FFDQHD1X rx_br_cnt_r_reg_0_ ( .D(n234), .CK(clk_i), .Q(rx_br_cnt_r[0]) );
  FFDQHD1X reg_data_r_reg_1_ ( .D(N233), .CK(clk_i), .Q(rdata_o[1]) );
  FFDQHD1X reg_data_r_reg_0_ ( .D(N232), .CK(clk_i), .Q(rdata_o[0]) );
  FFDCRHDLX reg_data_r_reg_7_ ( .D(n277), .RN(n14), .CK(clk_i), .Q(rdata_o[7])
         );
  FFDCRHDLX reg_data_r_reg_6_ ( .D(n277), .RN(n13), .CK(clk_i), .Q(rdata_o[6])
         );
  FFDCRHDLX reg_data_r_reg_5_ ( .D(n277), .RN(n12), .CK(clk_i), .Q(rdata_o[5])
         );
  FFDCRHDLX reg_data_r_reg_4_ ( .D(n277), .RN(n11), .CK(clk_i), .Q(rdata_o[4])
         );
  FFDCRHDLX reg_data_r_reg_3_ ( .D(n277), .RN(n10), .CK(clk_i), .Q(rdata_o[3])
         );
  FFDCRHDLX reg_data_r_reg_2_ ( .D(n277), .RN(n9), .CK(clk_i), .Q(rdata_o[2])
         );
  FFEDHD1X rx_data_r_reg_7_ ( .D(n26), .E(n275), .CK(clk_i), .Q(n14) );
  FFEDHD1X rx_data_r_reg_6_ ( .D(n25), .E(n275), .CK(clk_i), .Q(n13) );
  FFEDHD1X rx_data_r_reg_5_ ( .D(n24), .E(n275), .CK(clk_i), .Q(n12) );
  FFEDHD1X rx_data_r_reg_4_ ( .D(n23), .E(n275), .CK(clk_i), .Q(n11) );
  FFEDHD1X rx_data_r_reg_3_ ( .D(n22), .E(n275), .CK(clk_i), .Q(n10) );
  FFEDHD1X rx_data_r_reg_2_ ( .D(n21), .E(n275), .CK(clk_i), .Q(n9) );
  NAND3HDMX U15 ( .A(n165), .B(n127), .C(uart_rx_i), .Z(n122) );
  NAND2HD1X U16 ( .A(n57), .B(n83), .Z(n48) );
  NAND4HDMX U29 ( .A(wr_i), .B(addr_i[2]), .C(n84), .D(n85), .Z(n57) );
  NOR2HD1X U30 ( .A(n45), .B(reset_i), .Z(n28) );
  NOR4HD2X U31 ( .A(n193), .B(rx_bit_cnt_r[0]), .C(rx_bit_cnt_r[1]), .D(
        rx_bit_cnt_r[2]), .Z(n276) );
  INVCLKHD1X U32 ( .A(n27), .Z(n197) );
  AOI22HDLX U33 ( .A(n28), .B(tx_br_cnt_r[0]), .C(N39), .D(n29), .Z(n27) );
  INVCLKHD1X U34 ( .A(n30), .Z(n198) );
  AOI22HDLX U35 ( .A(n28), .B(tx_br_cnt_r[15]), .C(N54), .D(n29), .Z(n30) );
  INVCLKHD1X U36 ( .A(n31), .Z(n199) );
  AOI22HDLX U37 ( .A(n28), .B(tx_br_cnt_r[14]), .C(N53), .D(n29), .Z(n31) );
  INVCLKHD1X U38 ( .A(n32), .Z(n200) );
  AOI22HDLX U39 ( .A(n28), .B(tx_br_cnt_r[13]), .C(N52), .D(n29), .Z(n32) );
  INVCLKHD1X U40 ( .A(n33), .Z(n201) );
  AOI22HDLX U41 ( .A(n28), .B(tx_br_cnt_r[12]), .C(N51), .D(n29), .Z(n33) );
  INVCLKHD1X U42 ( .A(n34), .Z(n202) );
  AOI22HDLX U43 ( .A(n28), .B(tx_br_cnt_r[11]), .C(N50), .D(n29), .Z(n34) );
  INVCLKHD1X U44 ( .A(n35), .Z(n203) );
  AOI22HDLX U45 ( .A(n28), .B(tx_br_cnt_r[10]), .C(N49), .D(n29), .Z(n35) );
  INVCLKHD1X U46 ( .A(n36), .Z(n258) );
  AOI22HDLX U47 ( .A(n28), .B(tx_br_cnt_r[9]), .C(N48), .D(n29), .Z(n36) );
  INVCLKHD1X U48 ( .A(n37), .Z(n259) );
  AOI22HDLX U49 ( .A(n28), .B(tx_br_cnt_r[8]), .C(N47), .D(n29), .Z(n37) );
  INVCLKHD1X U50 ( .A(n38), .Z(n260) );
  AOI22HDLX U51 ( .A(n28), .B(tx_br_cnt_r[7]), .C(N46), .D(n29), .Z(n38) );
  INVCLKHD1X U52 ( .A(n39), .Z(n261) );
  AOI22HDLX U53 ( .A(n28), .B(tx_br_cnt_r[6]), .C(N45), .D(n29), .Z(n39) );
  INVCLKHD1X U54 ( .A(n40), .Z(n262) );
  AOI22HDLX U55 ( .A(n28), .B(tx_br_cnt_r[5]), .C(N44), .D(n29), .Z(n40) );
  INVCLKHD1X U56 ( .A(n41), .Z(n263) );
  AOI22HDLX U57 ( .A(n28), .B(tx_br_cnt_r[4]), .C(N43), .D(n29), .Z(n41) );
  INVCLKHD1X U58 ( .A(n42), .Z(n264) );
  AOI22HDLX U59 ( .A(n28), .B(tx_br_cnt_r[3]), .C(N42), .D(n29), .Z(n42) );
  INVCLKHD1X U60 ( .A(n43), .Z(n265) );
  AOI22HDLX U61 ( .A(n28), .B(tx_br_cnt_r[2]), .C(N41), .D(n29), .Z(n43) );
  INVCLKHD1X U62 ( .A(n44), .Z(n266) );
  AOI22HDLX U63 ( .A(n28), .B(tx_br_cnt_r[1]), .C(N40), .D(n29), .Z(n44) );
  NOR2B1HD1X U64 ( .AN(n45), .B(n46), .Z(n29) );
  OAI22B2HDLX U65 ( .C(n47), .D(n48), .AN(wdata_i[15]), .BN(n49), .Z(n267) );
  OAI22B2HDLX U66 ( .C(n50), .D(n48), .AN(wdata_i[14]), .BN(n49), .Z(n268) );
  OAI22B2HDLX U67 ( .C(n51), .D(n48), .AN(wdata_i[13]), .BN(n49), .Z(n269) );
  OAI22B2HDLX U68 ( .C(n52), .D(n48), .AN(wdata_i[12]), .BN(n49), .Z(n270) );
  OAI22B2HDLX U69 ( .C(n53), .D(n48), .AN(wdata_i[11]), .BN(n49), .Z(n271) );
  OAI22B2HDLX U70 ( .C(n54), .D(n48), .AN(wdata_i[10]), .BN(n49), .Z(n272) );
  OAI22B2HDLX U71 ( .C(n55), .D(n48), .AN(wdata_i[9]), .BN(n49), .Z(n273) );
  OAI22B2HDLX U72 ( .C(n56), .D(n48), .AN(wdata_i[8]), .BN(n49), .Z(n274) );
  INVCLKHD1X U73 ( .A(n57), .Z(n49) );
  INVCLKHD1X U74 ( .A(n58), .Z(n275) );
  OAI22HDLX U75 ( .A(n57), .B(n59), .C(n60), .D(n48), .Z(n257) );
  OAI22HDLX U76 ( .A(n57), .B(n61), .C(n62), .D(n48), .Z(n256) );
  OAI22HDLX U77 ( .A(n57), .B(n63), .C(n64), .D(n48), .Z(n255) );
  OAI22HDLX U78 ( .A(n57), .B(n65), .C(n66), .D(n48), .Z(n254) );
  OAI22HDLX U79 ( .A(n57), .B(n67), .C(n68), .D(n48), .Z(n253) );
  OAI22HDLX U80 ( .A(n57), .B(n69), .C(n70), .D(n48), .Z(n252) );
  OAI22HDLX U81 ( .A(n57), .B(n71), .C(n72), .D(n48), .Z(n251) );
  OAI22HDLX U82 ( .A(n57), .B(n73), .C(n74), .D(n48), .Z(n250) );
  NOR2HDUX U83 ( .A(reset_i), .B(addr_i[3]), .Z(n84) );
  OAI21HDLX U84 ( .A(n86), .B(n17), .C(n87), .Z(n249) );
  AOI21HDLX U85 ( .A(tx_bit_cnt_r[2]), .B(n45), .C(n88), .Z(n86) );
  OAI21B2HD1X U86 ( .AN(tx_bit_cnt_r[0]), .BN(n89), .C(n90), .Z(n248) );
  NAND2HDUX U87 ( .A(n91), .B(n87), .Z(n247) );
  MUX2HDLX U88 ( .A(n90), .B(n92), .S0(tx_bit_cnt_r[1]), .Z(n91) );
  MUX2HDLX U89 ( .A(n93), .B(n88), .S0(tx_bit_cnt_r[2]), .Z(n246) );
  OAI21B2HD1X U90 ( .AN(n45), .BN(tx_bit_cnt_r[1]), .C(n92), .Z(n88) );
  AOI21HDLX U91 ( .A(n45), .B(tx_bit_cnt_r[0]), .C(n89), .Z(n92) );
  NOR2HDUX U92 ( .A(tx_bit_cnt_r[1]), .B(n90), .Z(n93) );
  OR2HD1X U93 ( .A(n94), .B(tx_bit_cnt_r[0]), .Z(n90) );
  OR2HD1X U94 ( .A(n95), .B(reset_i), .Z(n245) );
  MUX2HDLX U95 ( .A(n96), .B(n87), .S0(csr_w_0_), .Z(n95) );
  OAI222HDLX U96 ( .A(n97), .B(n2), .C(n94), .D(n7), .E(n73), .F(n87), .Z(n244) );
  INVCLKHD1X U97 ( .A(wdata_i[0]), .Z(n73) );
  OAI222HDLX U98 ( .A(n97), .B(n7), .C(n94), .D(n3), .E(n71), .F(n87), .Z(n243) );
  INVCLKHD1X U99 ( .A(wdata_i[1]), .Z(n71) );
  OAI222HDLX U100 ( .A(n97), .B(n3), .C(n94), .D(n8), .E(n69), .F(n87), .Z(
        n242) );
  INVCLKHD1X U101 ( .A(wdata_i[2]), .Z(n69) );
  OAI222HDLX U102 ( .A(n97), .B(n8), .C(n94), .D(n4), .E(n67), .F(n87), .Z(
        n241) );
  INVCLKHD1X U103 ( .A(wdata_i[3]), .Z(n67) );
  OAI222HDLX U104 ( .A(n97), .B(n4), .C(n94), .D(n15), .E(n65), .F(n87), .Z(
        n240) );
  INVCLKHD1X U105 ( .A(wdata_i[4]), .Z(n65) );
  OAI222HDLX U106 ( .A(n97), .B(n15), .C(n94), .D(n5), .E(n63), .F(n87), .Z(
        n239) );
  INVCLKHD1X U107 ( .A(wdata_i[5]), .Z(n63) );
  OAI222HDLX U108 ( .A(n97), .B(n5), .C(n94), .D(n16), .E(n61), .F(n87), .Z(
        n238) );
  INVCLKHD1X U109 ( .A(wdata_i[6]), .Z(n61) );
  OAI222HDLX U110 ( .A(n97), .B(n16), .C(n94), .D(n1), .E(n59), .F(n87), .Z(
        n237) );
  INVCLKHD1X U111 ( .A(wdata_i[7]), .Z(n59) );
  OAI211HD1X U112 ( .A(n97), .B(n1), .C(n94), .D(n87), .Z(n236) );
  OAI211HD1X U113 ( .A(n94), .B(n2), .C(n83), .D(n98), .Z(n235) );
  NAND2HDUX U114 ( .A(uart_tx_o), .B(n89), .Z(n98) );
  INVCLKHD1X U115 ( .A(n97), .Z(n89) );
  NAND2HDUX U116 ( .A(n45), .B(n97), .Z(n94) );
  OAI21HDLX U117 ( .A(n99), .B(n28), .C(n87), .Z(n97) );
  NAND4HDLX U118 ( .A(csr_w_0_), .B(n96), .C(n277), .D(wr_i), .Z(n87) );
  NOR2HDUX U119 ( .A(reset_i), .B(n46), .Z(n99) );
  NOR4HDLX U120 ( .A(n100), .B(n101), .C(n102), .D(n103), .Z(n46) );
  NAND4HDLX U121 ( .A(n104), .B(n105), .C(n106), .D(n107), .Z(n103) );
  XOR2CLKHD1X U122 ( .A(tx_br_cnt_r[3]), .B(n68), .Z(n107) );
  XOR2CLKHD1X U123 ( .A(tx_br_cnt_r[4]), .B(n66), .Z(n106) );
  XOR2CLKHD1X U124 ( .A(tx_br_cnt_r[5]), .B(n64), .Z(n105) );
  XOR2CLKHD1X U125 ( .A(tx_br_cnt_r[6]), .B(n62), .Z(n104) );
  NAND4HDLX U126 ( .A(n108), .B(n109), .C(n110), .D(n111), .Z(n102) );
  XOR2CLKHD1X U127 ( .A(n74), .B(tx_br_cnt_r[0]), .Z(n111) );
  XOR2CLKHD1X U128 ( .A(tx_br_cnt_r[15]), .B(n47), .Z(n110) );
  XOR2CLKHD1X U129 ( .A(tx_br_cnt_r[1]), .B(n72), .Z(n109) );
  XOR2CLKHD1X U130 ( .A(tx_br_cnt_r[2]), .B(n70), .Z(n108) );
  NAND4HDLX U131 ( .A(n112), .B(n113), .C(n114), .D(n115), .Z(n101) );
  XOR2CLKHD1X U132 ( .A(tx_br_cnt_r[11]), .B(n53), .Z(n115) );
  XOR2CLKHD1X U133 ( .A(tx_br_cnt_r[12]), .B(n52), .Z(n114) );
  XOR2CLKHD1X U134 ( .A(tx_br_cnt_r[13]), .B(n51), .Z(n113) );
  XOR2CLKHD1X U135 ( .A(tx_br_cnt_r[14]), .B(n50), .Z(n112) );
  NAND4HDLX U136 ( .A(n116), .B(n117), .C(n118), .D(n119), .Z(n100) );
  XOR2CLKHD1X U137 ( .A(tx_br_cnt_r[10]), .B(n54), .Z(n119) );
  XOR2CLKHD1X U138 ( .A(tx_br_cnt_r[7]), .B(n60), .Z(n118) );
  XOR2CLKHD1X U139 ( .A(tx_br_cnt_r[8]), .B(n56), .Z(n117) );
  XOR2CLKHD1X U140 ( .A(tx_br_cnt_r[9]), .B(n55), .Z(n116) );
  NOR2HDUX U141 ( .A(reset_i), .B(n96), .Z(n45) );
  NOR4HDLX U142 ( .A(n192), .B(tx_bit_cnt_r[0]), .C(tx_bit_cnt_r[1]), .D(
        tx_bit_cnt_r[2]), .Z(n96) );
  OAI221HD1X U143 ( .A(n120), .B(n121), .C(n72), .D(n122), .E(n123), .Z(n234)
         );
  AOI21HDLX U144 ( .A(N159), .B(n124), .C(reset_i), .Z(n123) );
  INVCLKHD1X U145 ( .A(rx_br_cnt_r[0]), .Z(n120) );
  INVCLKHD1X U146 ( .A(n125), .Z(n233) );
  AOI22HDLX U147 ( .A(rx_br_cnt_r[15]), .B(n126), .C(N174), .D(n124), .Z(n125)
         );
  OAI22HDLX U148 ( .A(n127), .B(n128), .C(n129), .D(n130), .Z(n232) );
  OAI22HDLX U149 ( .A(n131), .B(n132), .C(n133), .D(n18), .Z(n231) );
  AOI21HDLX U150 ( .A(rx_bit_cnt_r[2]), .B(n134), .C(n135), .Z(n133) );
  NAND2B1HD1X U151 ( .AN(n136), .B(n137), .Z(n230) );
  MUX2HDLX U152 ( .A(n138), .B(rx_bit_cnt_r[0]), .S0(n131), .Z(n136) );
  MUXI2HD1X U153 ( .A(n137), .B(n139), .S0(rx_bit_cnt_r[1]), .Z(n229) );
  MUX2HDLX U154 ( .A(n140), .B(n135), .S0(rx_bit_cnt_r[2]), .Z(n228) );
  OAI21B2HD1X U155 ( .AN(n134), .BN(rx_bit_cnt_r[1]), .C(n139), .Z(n135) );
  AOI21HDLX U156 ( .A(n134), .B(rx_bit_cnt_r[0]), .C(n131), .Z(n139) );
  INVCLKHD1X U157 ( .A(n141), .Z(n131) );
  NOR2HDUX U158 ( .A(rx_bit_cnt_r[1]), .B(n137), .Z(n140) );
  NAND3B1HDLX U159 ( .AN(rx_bit_cnt_r[0]), .B(n141), .C(n134), .Z(n137) );
  OAI21HDLX U160 ( .A(n132), .B(n142), .C(n143), .Z(n141) );
  OAI21HDLX U161 ( .A(n144), .B(n6), .C(n58), .Z(n227) );
  OAI21HDLX U162 ( .A(n70), .B(n122), .C(n145), .Z(n226) );
  AOI22HDLX U163 ( .A(N160), .B(n124), .C(n126), .D(rx_br_cnt_r[1]), .Z(n145)
         );
  OAI21HDLX U164 ( .A(n68), .B(n122), .C(n146), .Z(n225) );
  AOI22HDLX U165 ( .A(N161), .B(n124), .C(n126), .D(rx_br_cnt_r[2]), .Z(n146)
         );
  OAI21HDLX U166 ( .A(n66), .B(n122), .C(n147), .Z(n224) );
  AOI22HDLX U167 ( .A(N162), .B(n124), .C(n126), .D(rx_br_cnt_r[3]), .Z(n147)
         );
  OAI21HDLX U168 ( .A(n64), .B(n122), .C(n148), .Z(n223) );
  AOI22HDLX U169 ( .A(N163), .B(n124), .C(n126), .D(rx_br_cnt_r[4]), .Z(n148)
         );
  OAI21HDLX U170 ( .A(n62), .B(n122), .C(n149), .Z(n222) );
  AOI22HDLX U171 ( .A(N164), .B(n124), .C(n126), .D(rx_br_cnt_r[5]), .Z(n149)
         );
  OAI21HDLX U172 ( .A(n60), .B(n122), .C(n150), .Z(n221) );
  AOI22HDLX U173 ( .A(N165), .B(n124), .C(n126), .D(rx_br_cnt_r[6]), .Z(n150)
         );
  OAI21HDLX U174 ( .A(n56), .B(n122), .C(n151), .Z(n220) );
  AOI22HDLX U175 ( .A(N166), .B(n124), .C(n126), .D(rx_br_cnt_r[7]), .Z(n151)
         );
  OAI21HDLX U176 ( .A(n55), .B(n122), .C(n152), .Z(n219) );
  AOI22HDLX U177 ( .A(N167), .B(n124), .C(n126), .D(rx_br_cnt_r[8]), .Z(n152)
         );
  OAI21HDLX U178 ( .A(n54), .B(n122), .C(n153), .Z(n218) );
  AOI22HDLX U179 ( .A(N168), .B(n124), .C(n126), .D(rx_br_cnt_r[9]), .Z(n153)
         );
  OAI21HDLX U180 ( .A(n53), .B(n122), .C(n154), .Z(n217) );
  AOI22HDLX U181 ( .A(N169), .B(n124), .C(n126), .D(rx_br_cnt_r[10]), .Z(n154)
         );
  OAI21HDLX U182 ( .A(n52), .B(n122), .C(n155), .Z(n216) );
  AOI22HDLX U183 ( .A(N170), .B(n124), .C(n126), .D(rx_br_cnt_r[11]), .Z(n155)
         );
  OAI21HDLX U184 ( .A(n51), .B(n122), .C(n156), .Z(n215) );
  AOI22HDLX U185 ( .A(N171), .B(n124), .C(n126), .D(rx_br_cnt_r[12]), .Z(n156)
         );
  OAI21HDLX U186 ( .A(n50), .B(n122), .C(n157), .Z(n214) );
  AOI22HDLX U187 ( .A(N172), .B(n124), .C(n126), .D(rx_br_cnt_r[13]), .Z(n157)
         );
  OAI21HDLX U188 ( .A(n47), .B(n122), .C(n158), .Z(n213) );
  AOI22HDLX U189 ( .A(N173), .B(n124), .C(n126), .D(rx_br_cnt_r[14]), .Z(n158)
         );
  AOI21B2HD1X U190 ( .AN(n159), .BN(n138), .C(n126), .Z(n124) );
  INVCLKHD1X U191 ( .A(n121), .Z(n126) );
  NAND4HDLX U192 ( .A(n160), .B(n122), .C(n144), .D(n83), .Z(n121) );
  NAND2HDUX U193 ( .A(n138), .B(n142), .Z(n160) );
  OR4HD1X U194 ( .A(n161), .B(n162), .C(n163), .D(n164), .Z(n142) );
  OR4HD1X U195 ( .A(rx_br_cnt_r[2]), .B(rx_br_cnt_r[3]), .C(rx_br_cnt_r[4]), 
        .D(rx_br_cnt_r[5]), .Z(n164) );
  OR4HD1X U196 ( .A(rx_br_cnt_r[6]), .B(rx_br_cnt_r[7]), .C(rx_br_cnt_r[8]), 
        .D(rx_br_cnt_r[9]), .Z(n163) );
  OR4HD1X U197 ( .A(rx_br_cnt_r[0]), .B(rx_br_cnt_r[10]), .C(rx_br_cnt_r[11]), 
        .D(rx_br_cnt_r[12]), .Z(n162) );
  OR4HD1X U198 ( .A(rx_br_cnt_r[13]), .B(rx_br_cnt_r[14]), .C(rx_br_cnt_r[15]), 
        .D(rx_br_cnt_r[1]), .Z(n161) );
  INVCLKHD1X U199 ( .A(n132), .Z(n138) );
  NAND3HDLX U200 ( .A(n127), .B(n129), .C(n165), .Z(n132) );
  INVCLKHD1X U201 ( .A(uart_rx_i), .Z(n129) );
  NOR2HDUX U202 ( .A(n166), .B(n144), .Z(n159) );
  INVCLKHD1X U203 ( .A(n134), .Z(n144) );
  OAI22HDLX U204 ( .A(n82), .B(n128), .C(n81), .D(n130), .Z(n212) );
  OAI22HDLX U205 ( .A(n81), .B(n128), .C(n80), .D(n130), .Z(n211) );
  OAI22HDLX U206 ( .A(n80), .B(n128), .C(n79), .D(n130), .Z(n210) );
  OAI22HDLX U207 ( .A(n79), .B(n128), .C(n78), .D(n130), .Z(n209) );
  OAI22HDLX U208 ( .A(n78), .B(n128), .C(n77), .D(n130), .Z(n208) );
  OAI22HDLX U209 ( .A(n77), .B(n128), .C(n76), .D(n130), .Z(n207) );
  OAI22HDLX U210 ( .A(n76), .B(n128), .C(n75), .D(n130), .Z(n206) );
  OAI22HDLX U211 ( .A(n75), .B(n128), .C(n127), .D(n130), .Z(n205) );
  NAND2HDUX U212 ( .A(n134), .B(n128), .Z(n130) );
  INVCLKHD1X U213 ( .A(rx_shifter_r_8_), .Z(n127) );
  NAND2HDUX U214 ( .A(n143), .B(n58), .Z(n128) );
  NAND2HDUX U215 ( .A(rx_shifter_r_8_), .B(n165), .Z(n58) );
  AND3HD1X U216 ( .A(n276), .B(n83), .C(n6), .Z(n165) );
  INVCLKHD1X U217 ( .A(reset_i), .Z(n83) );
  AOI21HDLX U218 ( .A(n134), .B(n166), .C(reset_i), .Z(n143) );
  NOR4HDLX U219 ( .A(n167), .B(n168), .C(n169), .D(n170), .Z(n166) );
  NAND4HDLX U220 ( .A(n171), .B(n172), .C(n173), .D(n174), .Z(n170) );
  XOR2CLKHD1X U221 ( .A(rx_br_cnt_r[3]), .B(n68), .Z(n174) );
  INVCLKHD1X U222 ( .A(br_r[3]), .Z(n68) );
  XOR2CLKHD1X U223 ( .A(rx_br_cnt_r[4]), .B(n66), .Z(n173) );
  INVCLKHD1X U224 ( .A(br_r[4]), .Z(n66) );
  XOR2CLKHD1X U225 ( .A(rx_br_cnt_r[5]), .B(n64), .Z(n172) );
  INVCLKHD1X U226 ( .A(br_r[5]), .Z(n64) );
  XOR2CLKHD1X U227 ( .A(rx_br_cnt_r[6]), .B(n62), .Z(n171) );
  INVCLKHD1X U228 ( .A(br_r[6]), .Z(n62) );
  NAND4HDLX U229 ( .A(n175), .B(n176), .C(n177), .D(n178), .Z(n169) );
  XOR2CLKHD1X U230 ( .A(n74), .B(rx_br_cnt_r[0]), .Z(n178) );
  INVCLKHD1X U231 ( .A(br_r[0]), .Z(n74) );
  XOR2CLKHD1X U232 ( .A(rx_br_cnt_r[15]), .B(n47), .Z(n177) );
  INVCLKHD1X U233 ( .A(br_r[15]), .Z(n47) );
  XOR2CLKHD1X U234 ( .A(rx_br_cnt_r[1]), .B(n72), .Z(n176) );
  INVCLKHD1X U235 ( .A(br_r[1]), .Z(n72) );
  XOR2CLKHD1X U236 ( .A(rx_br_cnt_r[2]), .B(n70), .Z(n175) );
  INVCLKHD1X U237 ( .A(br_r[2]), .Z(n70) );
  NAND4HDLX U238 ( .A(n179), .B(n180), .C(n181), .D(n182), .Z(n168) );
  XOR2CLKHD1X U239 ( .A(rx_br_cnt_r[11]), .B(n53), .Z(n182) );
  INVCLKHD1X U240 ( .A(br_r[11]), .Z(n53) );
  XOR2CLKHD1X U241 ( .A(rx_br_cnt_r[12]), .B(n52), .Z(n181) );
  INVCLKHD1X U242 ( .A(br_r[12]), .Z(n52) );
  XOR2CLKHD1X U243 ( .A(rx_br_cnt_r[13]), .B(n51), .Z(n180) );
  INVCLKHD1X U244 ( .A(br_r[13]), .Z(n51) );
  XOR2CLKHD1X U245 ( .A(rx_br_cnt_r[14]), .B(n50), .Z(n179) );
  INVCLKHD1X U246 ( .A(br_r[14]), .Z(n50) );
  NAND4HDLX U247 ( .A(n183), .B(n184), .C(n185), .D(n186), .Z(n167) );
  XOR2CLKHD1X U248 ( .A(rx_br_cnt_r[10]), .B(n54), .Z(n186) );
  INVCLKHD1X U249 ( .A(br_r[10]), .Z(n54) );
  XOR2CLKHD1X U250 ( .A(rx_br_cnt_r[7]), .B(n60), .Z(n185) );
  INVCLKHD1X U251 ( .A(br_r[7]), .Z(n60) );
  XOR2CLKHD1X U252 ( .A(rx_br_cnt_r[8]), .B(n56), .Z(n184) );
  INVCLKHD1X U253 ( .A(br_r[8]), .Z(n56) );
  XOR2CLKHD1X U254 ( .A(rx_br_cnt_r[9]), .B(n55), .Z(n183) );
  INVCLKHD1X U255 ( .A(br_r[9]), .Z(n55) );
  NOR2HDUX U256 ( .A(n276), .B(reset_i), .Z(n134) );
  AOI21HDLX U257 ( .A(n187), .B(n6), .C(reset_i), .Z(n204) );
  OAI21HDLX U258 ( .A(n188), .B(n189), .C(n194), .Z(n187) );
  INVCLKHD1X U259 ( .A(rd_i), .Z(n188) );
  MUX2HDLX U260 ( .A(n194), .B(n195), .S0(n277), .Z(N233) );
  MUX2HDLX U261 ( .A(csr_w_0_), .B(n196), .S0(n277), .Z(N232) );
  NOR2HDUX U262 ( .A(n189), .B(reset_i), .Z(n277) );
  NAND3B1HDLX U263 ( .AN(addr_i[2]), .B(n85), .C(addr_i[3]), .Z(n189) );
  AND2CLKHD1X U264 ( .A(n190), .B(n191), .Z(n85) );
  NOR3HD1X U265 ( .A(addr_i[5]), .B(addr_i[7]), .C(addr_i[6]), .Z(n191) );
  NOR3HD1X U266 ( .A(addr_i[0]), .B(addr_i[4]), .C(addr_i[1]), .Z(n190) );
endmodule


module per_gpio ( clk_i, reset_i, addr_i, wdata_i, rdata_o, size_i, rd_i, wr_i, 
        gpio_in_i, gpio_out_o );
  input [31:0] addr_i;
  input [31:0] wdata_i;
  output [31:0] rdata_o;
  input [1:0] size_i;
  input [31:0] gpio_in_i;
  output [31:0] gpio_out_o;
  input clk_i, reset_i, rd_i, wr_i;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n144, n145, n146, n147, n148, n149, n150, n151, n152, n153,
         n154, n155, n156, n157, n158, n159, n160, n161, n162, n163, n164,
         n165, n166, n167, n168, n169, n170, n171, n172, n173, n174, n175, n33,
         n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47,
         n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61,
         n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75,
         n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89,
         n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102,
         n103, n104, n105, n106, n107, n108, n109, n110, n111, n112, n113,
         n114, n115, n116, n117, n118, n119, n120, n121, n122, n123, n124,
         n125, n126, n127, n128, n129, n130, n131, n132, n133, n134, n135,
         n136, n137;
  tri   clk_i;
  tri   reset_i;
  tri   [31:0] addr_i;
  tri   [31:0] wdata_i;
  tri   [31:0] rdata_o;
  tri   wr_i;

  FFDHD1X gpio_out_r_reg_31_ ( .D(n144), .CK(clk_i), .Q(gpio_out_o[31]), .QN(
        n1) );
  FFDHD1X gpio_out_r_reg_30_ ( .D(n145), .CK(clk_i), .Q(gpio_out_o[30]), .QN(
        n2) );
  FFDHD1X gpio_out_r_reg_29_ ( .D(n146), .CK(clk_i), .Q(gpio_out_o[29]), .QN(
        n3) );
  FFDHD1X gpio_out_r_reg_28_ ( .D(n147), .CK(clk_i), .Q(gpio_out_o[28]), .QN(
        n4) );
  FFDHD1X gpio_out_r_reg_27_ ( .D(n148), .CK(clk_i), .Q(gpio_out_o[27]), .QN(
        n5) );
  FFDHD1X gpio_out_r_reg_26_ ( .D(n149), .CK(clk_i), .Q(gpio_out_o[26]), .QN(
        n6) );
  FFDHD1X gpio_out_r_reg_25_ ( .D(n150), .CK(clk_i), .Q(gpio_out_o[25]), .QN(
        n7) );
  FFDHD1X gpio_out_r_reg_24_ ( .D(n151), .CK(clk_i), .Q(gpio_out_o[24]), .QN(
        n8) );
  FFDHD1X gpio_out_r_reg_23_ ( .D(n152), .CK(clk_i), .Q(gpio_out_o[23]), .QN(
        n9) );
  FFDHD1X gpio_out_r_reg_22_ ( .D(n153), .CK(clk_i), .Q(gpio_out_o[22]), .QN(
        n10) );
  FFDHD1X gpio_out_r_reg_21_ ( .D(n154), .CK(clk_i), .Q(gpio_out_o[21]), .QN(
        n11) );
  FFDHD1X gpio_out_r_reg_20_ ( .D(n155), .CK(clk_i), .Q(gpio_out_o[20]), .QN(
        n12) );
  FFDHD1X gpio_out_r_reg_19_ ( .D(n156), .CK(clk_i), .Q(gpio_out_o[19]), .QN(
        n13) );
  FFDHD1X gpio_out_r_reg_18_ ( .D(n157), .CK(clk_i), .Q(gpio_out_o[18]), .QN(
        n14) );
  FFDHD1X gpio_out_r_reg_17_ ( .D(n158), .CK(clk_i), .Q(gpio_out_o[17]), .QN(
        n15) );
  FFDHD1X gpio_out_r_reg_16_ ( .D(n159), .CK(clk_i), .Q(gpio_out_o[16]), .QN(
        n16) );
  FFDHD1X gpio_out_r_reg_15_ ( .D(n160), .CK(clk_i), .Q(gpio_out_o[15]), .QN(
        n17) );
  FFDHD1X gpio_out_r_reg_14_ ( .D(n161), .CK(clk_i), .Q(gpio_out_o[14]), .QN(
        n18) );
  FFDHD1X gpio_out_r_reg_13_ ( .D(n162), .CK(clk_i), .Q(gpio_out_o[13]), .QN(
        n19) );
  FFDHD1X gpio_out_r_reg_12_ ( .D(n163), .CK(clk_i), .Q(gpio_out_o[12]), .QN(
        n20) );
  FFDHD1X gpio_out_r_reg_11_ ( .D(n164), .CK(clk_i), .Q(gpio_out_o[11]), .QN(
        n21) );
  FFDHD1X gpio_out_r_reg_10_ ( .D(n165), .CK(clk_i), .Q(gpio_out_o[10]), .QN(
        n22) );
  FFDHD1X gpio_out_r_reg_9_ ( .D(n166), .CK(clk_i), .Q(gpio_out_o[9]), .QN(n23) );
  FFDHD1X gpio_out_r_reg_8_ ( .D(n167), .CK(clk_i), .Q(gpio_out_o[8]), .QN(n24) );
  FFDHD1X gpio_out_r_reg_7_ ( .D(n168), .CK(clk_i), .Q(gpio_out_o[7]), .QN(n25) );
  FFDHD1X gpio_out_r_reg_6_ ( .D(n169), .CK(clk_i), .Q(gpio_out_o[6]), .QN(n26) );
  FFDHD1X gpio_out_r_reg_5_ ( .D(n170), .CK(clk_i), .Q(gpio_out_o[5]), .QN(n27) );
  FFDHD1X gpio_out_r_reg_4_ ( .D(n171), .CK(clk_i), .Q(gpio_out_o[4]), .QN(n28) );
  FFDHD1X gpio_out_r_reg_3_ ( .D(n172), .CK(clk_i), .Q(gpio_out_o[3]), .QN(n29) );
  FFDHD1X gpio_out_r_reg_2_ ( .D(n173), .CK(clk_i), .Q(gpio_out_o[2]), .QN(n30) );
  FFDHD1X gpio_out_r_reg_1_ ( .D(n174), .CK(clk_i), .Q(gpio_out_o[1]), .QN(n31) );
  FFDHD1X gpio_out_r_reg_0_ ( .D(n175), .CK(clk_i), .Q(gpio_out_o[0]), .QN(n32) );
  FFDQHD1X reg_data_r_reg_31_ ( .D(gpio_in_i[31]), .CK(clk_i), .Q(rdata_o[31])
         );
  FFDQHD1X reg_data_r_reg_30_ ( .D(gpio_in_i[30]), .CK(clk_i), .Q(rdata_o[30])
         );
  FFDQHD1X reg_data_r_reg_29_ ( .D(gpio_in_i[29]), .CK(clk_i), .Q(rdata_o[29])
         );
  FFDQHD1X reg_data_r_reg_28_ ( .D(gpio_in_i[28]), .CK(clk_i), .Q(rdata_o[28])
         );
  FFDQHD1X reg_data_r_reg_27_ ( .D(gpio_in_i[27]), .CK(clk_i), .Q(rdata_o[27])
         );
  FFDQHD1X reg_data_r_reg_26_ ( .D(gpio_in_i[26]), .CK(clk_i), .Q(rdata_o[26])
         );
  FFDQHD1X reg_data_r_reg_25_ ( .D(gpio_in_i[25]), .CK(clk_i), .Q(rdata_o[25])
         );
  FFDQHD1X reg_data_r_reg_24_ ( .D(gpio_in_i[24]), .CK(clk_i), .Q(rdata_o[24])
         );
  FFDQHD1X reg_data_r_reg_23_ ( .D(gpio_in_i[23]), .CK(clk_i), .Q(rdata_o[23])
         );
  FFDQHD1X reg_data_r_reg_22_ ( .D(gpio_in_i[22]), .CK(clk_i), .Q(rdata_o[22])
         );
  FFDQHD1X reg_data_r_reg_21_ ( .D(gpio_in_i[21]), .CK(clk_i), .Q(rdata_o[21])
         );
  FFDQHD1X reg_data_r_reg_20_ ( .D(gpio_in_i[20]), .CK(clk_i), .Q(rdata_o[20])
         );
  FFDQHD1X reg_data_r_reg_19_ ( .D(gpio_in_i[19]), .CK(clk_i), .Q(rdata_o[19])
         );
  FFDQHD1X reg_data_r_reg_18_ ( .D(gpio_in_i[18]), .CK(clk_i), .Q(rdata_o[18])
         );
  FFDQHD1X reg_data_r_reg_17_ ( .D(gpio_in_i[17]), .CK(clk_i), .Q(rdata_o[17])
         );
  FFDQHD1X reg_data_r_reg_16_ ( .D(gpio_in_i[16]), .CK(clk_i), .Q(rdata_o[16])
         );
  FFDQHD1X reg_data_r_reg_15_ ( .D(gpio_in_i[15]), .CK(clk_i), .Q(rdata_o[15])
         );
  FFDQHD1X reg_data_r_reg_14_ ( .D(gpio_in_i[14]), .CK(clk_i), .Q(rdata_o[14])
         );
  FFDQHD1X reg_data_r_reg_13_ ( .D(gpio_in_i[13]), .CK(clk_i), .Q(rdata_o[13])
         );
  FFDQHD1X reg_data_r_reg_12_ ( .D(gpio_in_i[12]), .CK(clk_i), .Q(rdata_o[12])
         );
  FFDQHD1X reg_data_r_reg_11_ ( .D(gpio_in_i[11]), .CK(clk_i), .Q(rdata_o[11])
         );
  FFDQHD1X reg_data_r_reg_10_ ( .D(gpio_in_i[10]), .CK(clk_i), .Q(rdata_o[10])
         );
  FFDQHD1X reg_data_r_reg_9_ ( .D(gpio_in_i[9]), .CK(clk_i), .Q(rdata_o[9]) );
  FFDQHD1X reg_data_r_reg_8_ ( .D(gpio_in_i[8]), .CK(clk_i), .Q(rdata_o[8]) );
  FFDQHD1X reg_data_r_reg_7_ ( .D(gpio_in_i[7]), .CK(clk_i), .Q(rdata_o[7]) );
  FFDQHD1X reg_data_r_reg_6_ ( .D(gpio_in_i[6]), .CK(clk_i), .Q(rdata_o[6]) );
  FFDQHD1X reg_data_r_reg_5_ ( .D(gpio_in_i[5]), .CK(clk_i), .Q(rdata_o[5]) );
  FFDQHD1X reg_data_r_reg_4_ ( .D(gpio_in_i[4]), .CK(clk_i), .Q(rdata_o[4]) );
  FFDQHD1X reg_data_r_reg_3_ ( .D(gpio_in_i[3]), .CK(clk_i), .Q(rdata_o[3]) );
  FFDQHD1X reg_data_r_reg_2_ ( .D(gpio_in_i[2]), .CK(clk_i), .Q(rdata_o[2]) );
  FFDQHD1X reg_data_r_reg_1_ ( .D(gpio_in_i[1]), .CK(clk_i), .Q(rdata_o[1]) );
  FFDQHD1X reg_data_r_reg_0_ ( .D(gpio_in_i[0]), .CK(clk_i), .Q(rdata_o[0]) );
  OAI32HD1X U3 ( .A(n36), .B(reset_i), .C(n39), .D(addr_i[3]), .E(n133), .Z(
        n37) );
  NOR3B1HD2X U4 ( .AN(n134), .B(addr_i[3]), .C(reset_i), .Z(n39) );
  NOR2B1HD2X U5 ( .AN(n134), .B(n133), .Z(n36) );
  OAI22HDLX U6 ( .A(n33), .B(n34), .C(n32), .D(n35), .Z(n175) );
  AOI21HDLX U7 ( .A(n36), .B(n34), .C(n37), .Z(n35) );
  INVCLKHD1X U8 ( .A(wdata_i[0]), .Z(n34) );
  AOI21HDLX U9 ( .A(n32), .B(n38), .C(n39), .Z(n33) );
  OAI22HDLX U10 ( .A(n40), .B(n41), .C(n31), .D(n42), .Z(n174) );
  AOI21HDLX U11 ( .A(n36), .B(n41), .C(n37), .Z(n42) );
  INVCLKHD1X U12 ( .A(wdata_i[1]), .Z(n41) );
  AOI21HDLX U13 ( .A(n31), .B(n38), .C(n39), .Z(n40) );
  OAI22HDLX U14 ( .A(n43), .B(n44), .C(n30), .D(n45), .Z(n173) );
  AOI21HDLX U15 ( .A(n36), .B(n44), .C(n37), .Z(n45) );
  INVCLKHD1X U16 ( .A(wdata_i[2]), .Z(n44) );
  AOI21HDLX U17 ( .A(n30), .B(n38), .C(n39), .Z(n43) );
  OAI22HDLX U18 ( .A(n46), .B(n47), .C(n29), .D(n48), .Z(n172) );
  AOI21HDLX U19 ( .A(n36), .B(n47), .C(n37), .Z(n48) );
  INVCLKHD1X U20 ( .A(wdata_i[3]), .Z(n47) );
  AOI21HDLX U21 ( .A(n29), .B(n38), .C(n39), .Z(n46) );
  OAI22HDLX U22 ( .A(n49), .B(n50), .C(n28), .D(n51), .Z(n171) );
  AOI21HDLX U23 ( .A(n36), .B(n50), .C(n37), .Z(n51) );
  INVCLKHD1X U24 ( .A(wdata_i[4]), .Z(n50) );
  AOI21HDLX U25 ( .A(n28), .B(n38), .C(n39), .Z(n49) );
  OAI22HDLX U26 ( .A(n52), .B(n53), .C(n27), .D(n54), .Z(n170) );
  AOI21HDLX U27 ( .A(n36), .B(n53), .C(n37), .Z(n54) );
  INVCLKHD1X U28 ( .A(wdata_i[5]), .Z(n53) );
  AOI21HDLX U29 ( .A(n27), .B(n38), .C(n39), .Z(n52) );
  OAI22HDLX U30 ( .A(n55), .B(n56), .C(n26), .D(n57), .Z(n169) );
  AOI21HDLX U31 ( .A(n36), .B(n56), .C(n37), .Z(n57) );
  INVCLKHD1X U32 ( .A(wdata_i[6]), .Z(n56) );
  AOI21HDLX U33 ( .A(n26), .B(n38), .C(n39), .Z(n55) );
  OAI22HDLX U34 ( .A(n58), .B(n59), .C(n25), .D(n60), .Z(n168) );
  AOI21HDLX U35 ( .A(n36), .B(n59), .C(n37), .Z(n60) );
  INVCLKHD1X U36 ( .A(wdata_i[7]), .Z(n59) );
  AOI21HDLX U37 ( .A(n25), .B(n38), .C(n39), .Z(n58) );
  OAI22HDLX U38 ( .A(n61), .B(n62), .C(n24), .D(n63), .Z(n167) );
  AOI21HDLX U39 ( .A(n36), .B(n62), .C(n37), .Z(n63) );
  INVCLKHD1X U40 ( .A(wdata_i[8]), .Z(n62) );
  AOI21HDLX U41 ( .A(n24), .B(n38), .C(n39), .Z(n61) );
  OAI22HDLX U42 ( .A(n64), .B(n65), .C(n23), .D(n66), .Z(n166) );
  AOI21HDLX U43 ( .A(n36), .B(n65), .C(n37), .Z(n66) );
  INVCLKHD1X U44 ( .A(wdata_i[9]), .Z(n65) );
  AOI21HDLX U45 ( .A(n23), .B(n38), .C(n39), .Z(n64) );
  OAI22HDLX U46 ( .A(n67), .B(n68), .C(n22), .D(n69), .Z(n165) );
  AOI21HDLX U47 ( .A(n36), .B(n68), .C(n37), .Z(n69) );
  INVCLKHD1X U48 ( .A(wdata_i[10]), .Z(n68) );
  AOI21HDLX U49 ( .A(n22), .B(n38), .C(n39), .Z(n67) );
  OAI22HDLX U50 ( .A(n70), .B(n71), .C(n21), .D(n72), .Z(n164) );
  AOI21HDLX U51 ( .A(n36), .B(n71), .C(n37), .Z(n72) );
  INVCLKHD1X U52 ( .A(wdata_i[11]), .Z(n71) );
  AOI21HDLX U53 ( .A(n21), .B(n38), .C(n39), .Z(n70) );
  OAI22HDLX U54 ( .A(n73), .B(n74), .C(n20), .D(n75), .Z(n163) );
  AOI21HDLX U55 ( .A(n36), .B(n74), .C(n37), .Z(n75) );
  INVCLKHD1X U56 ( .A(wdata_i[12]), .Z(n74) );
  AOI21HDLX U57 ( .A(n20), .B(n38), .C(n39), .Z(n73) );
  OAI22HDLX U58 ( .A(n76), .B(n77), .C(n19), .D(n78), .Z(n162) );
  AOI21HDLX U59 ( .A(n36), .B(n77), .C(n37), .Z(n78) );
  INVCLKHD1X U60 ( .A(wdata_i[13]), .Z(n77) );
  AOI21HDLX U61 ( .A(n19), .B(n38), .C(n39), .Z(n76) );
  OAI22HDLX U62 ( .A(n79), .B(n80), .C(n18), .D(n81), .Z(n161) );
  AOI21HDLX U63 ( .A(n36), .B(n80), .C(n37), .Z(n81) );
  INVCLKHD1X U64 ( .A(wdata_i[14]), .Z(n80) );
  AOI21HDLX U65 ( .A(n18), .B(n38), .C(n39), .Z(n79) );
  OAI22HDLX U66 ( .A(n82), .B(n83), .C(n17), .D(n84), .Z(n160) );
  AOI21HDLX U67 ( .A(n36), .B(n83), .C(n37), .Z(n84) );
  INVCLKHD1X U68 ( .A(wdata_i[15]), .Z(n83) );
  AOI21HDLX U69 ( .A(n17), .B(n38), .C(n39), .Z(n82) );
  OAI22HDLX U70 ( .A(n85), .B(n86), .C(n16), .D(n87), .Z(n159) );
  AOI21HDLX U71 ( .A(n36), .B(n86), .C(n37), .Z(n87) );
  INVCLKHD1X U72 ( .A(wdata_i[16]), .Z(n86) );
  AOI21HDLX U73 ( .A(n16), .B(n38), .C(n39), .Z(n85) );
  OAI22HDLX U74 ( .A(n88), .B(n89), .C(n15), .D(n90), .Z(n158) );
  AOI21HDLX U75 ( .A(n36), .B(n89), .C(n37), .Z(n90) );
  INVCLKHD1X U76 ( .A(wdata_i[17]), .Z(n89) );
  AOI21HDLX U77 ( .A(n15), .B(n38), .C(n39), .Z(n88) );
  OAI22HDLX U78 ( .A(n91), .B(n92), .C(n14), .D(n93), .Z(n157) );
  AOI21HDLX U79 ( .A(n36), .B(n92), .C(n37), .Z(n93) );
  INVCLKHD1X U80 ( .A(wdata_i[18]), .Z(n92) );
  AOI21HDLX U81 ( .A(n14), .B(n38), .C(n39), .Z(n91) );
  OAI22HDLX U82 ( .A(n94), .B(n95), .C(n13), .D(n96), .Z(n156) );
  AOI21HDLX U83 ( .A(n36), .B(n95), .C(n37), .Z(n96) );
  INVCLKHD1X U84 ( .A(wdata_i[19]), .Z(n95) );
  AOI21HDLX U85 ( .A(n13), .B(n38), .C(n39), .Z(n94) );
  OAI22HDLX U86 ( .A(n97), .B(n98), .C(n12), .D(n99), .Z(n155) );
  AOI21HDLX U87 ( .A(n36), .B(n98), .C(n37), .Z(n99) );
  INVCLKHD1X U88 ( .A(wdata_i[20]), .Z(n98) );
  AOI21HDLX U89 ( .A(n12), .B(n38), .C(n39), .Z(n97) );
  OAI22HDLX U90 ( .A(n100), .B(n101), .C(n11), .D(n102), .Z(n154) );
  AOI21HDLX U91 ( .A(n36), .B(n101), .C(n37), .Z(n102) );
  INVCLKHD1X U92 ( .A(wdata_i[21]), .Z(n101) );
  AOI21HDLX U93 ( .A(n11), .B(n38), .C(n39), .Z(n100) );
  OAI22HDLX U94 ( .A(n103), .B(n104), .C(n10), .D(n105), .Z(n153) );
  AOI21HDLX U95 ( .A(n36), .B(n104), .C(n37), .Z(n105) );
  INVCLKHD1X U96 ( .A(wdata_i[22]), .Z(n104) );
  AOI21HDLX U97 ( .A(n10), .B(n38), .C(n39), .Z(n103) );
  OAI22HDLX U98 ( .A(n106), .B(n107), .C(n9), .D(n108), .Z(n152) );
  AOI21HDLX U99 ( .A(n36), .B(n107), .C(n37), .Z(n108) );
  INVCLKHD1X U100 ( .A(wdata_i[23]), .Z(n107) );
  AOI21HDLX U101 ( .A(n9), .B(n38), .C(n39), .Z(n106) );
  OAI22HDLX U102 ( .A(n109), .B(n110), .C(n8), .D(n111), .Z(n151) );
  AOI21HDLX U103 ( .A(n36), .B(n110), .C(n37), .Z(n111) );
  INVCLKHD1X U104 ( .A(wdata_i[24]), .Z(n110) );
  AOI21HDLX U105 ( .A(n8), .B(n38), .C(n39), .Z(n109) );
  OAI22HDLX U106 ( .A(n112), .B(n113), .C(n7), .D(n114), .Z(n150) );
  AOI21HDLX U107 ( .A(n36), .B(n113), .C(n37), .Z(n114) );
  INVCLKHD1X U108 ( .A(wdata_i[25]), .Z(n113) );
  AOI21HDLX U109 ( .A(n7), .B(n38), .C(n39), .Z(n112) );
  OAI22HDLX U110 ( .A(n115), .B(n116), .C(n6), .D(n117), .Z(n149) );
  AOI21HDLX U111 ( .A(n36), .B(n116), .C(n37), .Z(n117) );
  INVCLKHD1X U112 ( .A(wdata_i[26]), .Z(n116) );
  AOI21HDLX U113 ( .A(n6), .B(n38), .C(n39), .Z(n115) );
  OAI22HDLX U114 ( .A(n118), .B(n119), .C(n5), .D(n120), .Z(n148) );
  AOI21HDLX U115 ( .A(n36), .B(n119), .C(n37), .Z(n120) );
  INVCLKHD1X U116 ( .A(wdata_i[27]), .Z(n119) );
  AOI21HDLX U117 ( .A(n5), .B(n38), .C(n39), .Z(n118) );
  OAI22HDLX U118 ( .A(n121), .B(n122), .C(n4), .D(n123), .Z(n147) );
  AOI21HDLX U119 ( .A(n36), .B(n122), .C(n37), .Z(n123) );
  INVCLKHD1X U120 ( .A(wdata_i[28]), .Z(n122) );
  AOI21HDLX U121 ( .A(n4), .B(n38), .C(n39), .Z(n121) );
  OAI22HDLX U122 ( .A(n124), .B(n125), .C(n3), .D(n126), .Z(n146) );
  AOI21HDLX U123 ( .A(n36), .B(n125), .C(n37), .Z(n126) );
  INVCLKHD1X U124 ( .A(wdata_i[29]), .Z(n125) );
  AOI21HDLX U125 ( .A(n3), .B(n38), .C(n39), .Z(n124) );
  OAI22HDLX U126 ( .A(n127), .B(n128), .C(n2), .D(n129), .Z(n145) );
  AOI21HDLX U127 ( .A(n36), .B(n128), .C(n37), .Z(n129) );
  INVCLKHD1X U128 ( .A(wdata_i[30]), .Z(n128) );
  AOI21HDLX U129 ( .A(n2), .B(n38), .C(n39), .Z(n127) );
  OAI22HDLX U130 ( .A(n130), .B(n131), .C(n1), .D(n132), .Z(n144) );
  AOI21HDLX U131 ( .A(n36), .B(n131), .C(n37), .Z(n132) );
  INVCLKHD1X U132 ( .A(n135), .Z(n133) );
  INVCLKHD1X U133 ( .A(wdata_i[31]), .Z(n131) );
  AOI21HDLX U134 ( .A(n1), .B(n38), .C(n39), .Z(n130) );
  AND3HD1X U135 ( .A(n135), .B(n134), .C(addr_i[2]), .Z(n38) );
  AND3HD1X U136 ( .A(n136), .B(wr_i), .C(n137), .Z(n134) );
  NOR4HDLX U137 ( .A(addr_i[7]), .B(addr_i[6]), .C(addr_i[5]), .D(addr_i[4]), 
        .Z(n137) );
  NOR2HDUX U138 ( .A(addr_i[1]), .B(addr_i[0]), .Z(n136) );
  AOI21B2HD1X U139 ( .AN(addr_i[3]), .BN(addr_i[2]), .C(reset_i), .Z(n135) );
endmodule


module per_timer_DW01_inc_0 ( A, SUM );
  input [31:0] A;
  output [31:0] SUM;

  wire   [31:2] carry;

  HAHDMX U1_1_30 ( .A(A[30]), .B(carry[30]), .CO(carry[31]), .S(SUM[30]) );
  HAHDMX U1_1_29 ( .A(A[29]), .B(carry[29]), .CO(carry[30]), .S(SUM[29]) );
  HAHDMX U1_1_28 ( .A(A[28]), .B(carry[28]), .CO(carry[29]), .S(SUM[28]) );
  HAHDMX U1_1_27 ( .A(A[27]), .B(carry[27]), .CO(carry[28]), .S(SUM[27]) );
  HAHDMX U1_1_26 ( .A(A[26]), .B(carry[26]), .CO(carry[27]), .S(SUM[26]) );
  HAHDMX U1_1_25 ( .A(A[25]), .B(carry[25]), .CO(carry[26]), .S(SUM[25]) );
  HAHDMX U1_1_24 ( .A(A[24]), .B(carry[24]), .CO(carry[25]), .S(SUM[24]) );
  HAHDMX U1_1_23 ( .A(A[23]), .B(carry[23]), .CO(carry[24]), .S(SUM[23]) );
  HAHDMX U1_1_22 ( .A(A[22]), .B(carry[22]), .CO(carry[23]), .S(SUM[22]) );
  HAHDMX U1_1_21 ( .A(A[21]), .B(carry[21]), .CO(carry[22]), .S(SUM[21]) );
  HAHDMX U1_1_20 ( .A(A[20]), .B(carry[20]), .CO(carry[21]), .S(SUM[20]) );
  HAHDMX U1_1_19 ( .A(A[19]), .B(carry[19]), .CO(carry[20]), .S(SUM[19]) );
  HAHDMX U1_1_18 ( .A(A[18]), .B(carry[18]), .CO(carry[19]), .S(SUM[18]) );
  HAHDMX U1_1_17 ( .A(A[17]), .B(carry[17]), .CO(carry[18]), .S(SUM[17]) );
  HAHDMX U1_1_16 ( .A(A[16]), .B(carry[16]), .CO(carry[17]), .S(SUM[16]) );
  HAHDMX U1_1_15 ( .A(A[15]), .B(carry[15]), .CO(carry[16]), .S(SUM[15]) );
  HAHDMX U1_1_14 ( .A(A[14]), .B(carry[14]), .CO(carry[15]), .S(SUM[14]) );
  HAHDMX U1_1_13 ( .A(A[13]), .B(carry[13]), .CO(carry[14]), .S(SUM[13]) );
  HAHDMX U1_1_12 ( .A(A[12]), .B(carry[12]), .CO(carry[13]), .S(SUM[12]) );
  HAHDMX U1_1_11 ( .A(A[11]), .B(carry[11]), .CO(carry[12]), .S(SUM[11]) );
  HAHDMX U1_1_10 ( .A(A[10]), .B(carry[10]), .CO(carry[11]), .S(SUM[10]) );
  HAHDMX U1_1_9 ( .A(A[9]), .B(carry[9]), .CO(carry[10]), .S(SUM[9]) );
  HAHDMX U1_1_8 ( .A(A[8]), .B(carry[8]), .CO(carry[9]), .S(SUM[8]) );
  HAHDMX U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  HAHDMX U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  HAHDMX U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  HAHDMX U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  HAHDMX U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  HAHDMX U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  HAHDMX U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  INVHD1X U1 ( .A(A[0]), .Z(SUM[0]) );
  XOR2CLKHD1X U2 ( .A(carry[31]), .B(A[31]), .Z(SUM[31]) );
endmodule


module per_timer_DW01_cmp6_0 ( A, B, TC, LT, GT, EQ, LE, GE, NE );
  input [31:0] A;
  input [31:0] B;
  input TC;
  output LT, GT, EQ, LE, GE, NE;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
         n45, n46;

  INVHD1X U1 ( .A(A[1]), .Z(n2) );
  INVHD1X U2 ( .A(B[1]), .Z(n1) );
  AND2CLKHD1X U3 ( .A(n3), .B(n4), .Z(EQ) );
  NOR4HDLX U4 ( .A(n5), .B(n6), .C(n7), .D(n8), .Z(n4) );
  NAND4HDLX U5 ( .A(n9), .B(n10), .C(n11), .D(n12), .Z(n8) );
  XNOR2HD1X U6 ( .A(B[11]), .B(A[11]), .Z(n12) );
  XNOR2HD1X U7 ( .A(B[12]), .B(A[12]), .Z(n11) );
  XNOR2HD1X U8 ( .A(B[13]), .B(A[13]), .Z(n10) );
  XNOR2HD1X U9 ( .A(B[14]), .B(A[14]), .Z(n9) );
  NAND4HDLX U10 ( .A(n13), .B(n14), .C(n15), .D(n16), .Z(n7) );
  XNOR2HD1X U11 ( .A(B[7]), .B(A[7]), .Z(n16) );
  XNOR2HD1X U12 ( .A(B[8]), .B(A[8]), .Z(n15) );
  XNOR2HD1X U13 ( .A(B[9]), .B(A[9]), .Z(n14) );
  XNOR2HD1X U14 ( .A(B[10]), .B(A[10]), .Z(n13) );
  NAND4HDLX U15 ( .A(n17), .B(n18), .C(n19), .D(n20), .Z(n6) );
  XNOR2HD1X U16 ( .A(B[3]), .B(A[3]), .Z(n20) );
  XNOR2HD1X U17 ( .A(B[4]), .B(A[4]), .Z(n19) );
  XNOR2HD1X U18 ( .A(B[5]), .B(A[5]), .Z(n18) );
  XNOR2HD1X U19 ( .A(B[6]), .B(A[6]), .Z(n17) );
  NAND4HDLX U20 ( .A(n21), .B(n22), .C(n23), .D(n24), .Z(n5) );
  OAI22HDLX U21 ( .A(n25), .B(n2), .C(B[1]), .D(n25), .Z(n24) );
  NOR2B1HD1X U22 ( .AN(B[0]), .B(A[0]), .Z(n25) );
  OAI22HDLX U23 ( .A(A[1]), .B(n26), .C(n26), .D(n1), .Z(n23) );
  NOR2B1HD1X U24 ( .AN(A[0]), .B(B[0]), .Z(n26) );
  XNOR2HD1X U25 ( .A(B[31]), .B(A[31]), .Z(n22) );
  XNOR2HD1X U26 ( .A(B[2]), .B(A[2]), .Z(n21) );
  NOR4HDLX U27 ( .A(n27), .B(n28), .C(n29), .D(n30), .Z(n3) );
  NAND4HDLX U28 ( .A(n31), .B(n32), .C(n33), .D(n34), .Z(n30) );
  XNOR2HD1X U29 ( .A(B[27]), .B(A[27]), .Z(n34) );
  XNOR2HD1X U30 ( .A(B[28]), .B(A[28]), .Z(n33) );
  XNOR2HD1X U31 ( .A(B[29]), .B(A[29]), .Z(n32) );
  XNOR2HD1X U32 ( .A(B[30]), .B(A[30]), .Z(n31) );
  NAND4HDLX U33 ( .A(n35), .B(n36), .C(n37), .D(n38), .Z(n29) );
  XNOR2HD1X U34 ( .A(B[23]), .B(A[23]), .Z(n38) );
  XNOR2HD1X U35 ( .A(B[24]), .B(A[24]), .Z(n37) );
  XNOR2HD1X U36 ( .A(B[25]), .B(A[25]), .Z(n36) );
  XNOR2HD1X U37 ( .A(B[26]), .B(A[26]), .Z(n35) );
  NAND4HDLX U38 ( .A(n39), .B(n40), .C(n41), .D(n42), .Z(n28) );
  XNOR2HD1X U39 ( .A(B[19]), .B(A[19]), .Z(n42) );
  XNOR2HD1X U40 ( .A(B[20]), .B(A[20]), .Z(n41) );
  XNOR2HD1X U41 ( .A(B[21]), .B(A[21]), .Z(n40) );
  XNOR2HD1X U42 ( .A(B[22]), .B(A[22]), .Z(n39) );
  NAND4HDLX U43 ( .A(n43), .B(n44), .C(n45), .D(n46), .Z(n27) );
  XNOR2HD1X U44 ( .A(B[15]), .B(A[15]), .Z(n46) );
  XNOR2HD1X U45 ( .A(B[16]), .B(A[16]), .Z(n45) );
  XNOR2HD1X U46 ( .A(B[17]), .B(A[17]), .Z(n44) );
  XNOR2HD1X U47 ( .A(B[18]), .B(A[18]), .Z(n43) );
endmodule


module per_timer ( clk_i, reset_i, addr_i, wdata_i, rdata_o, size_i, rd_i, 
        wr_i );
  input [31:0] addr_i;
  input [31:0] wdata_i;
  output [31:0] rdata_o;
  input [1:0] size_i;
  input clk_i, reset_i, rd_i, wr_i;
  wire   N1, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43,
         N44, N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57,
         N58, N59, N60, N61, N62, n125, n126, n127, n128, n129, n130, n131,
         n132, n133, n134, n135, n136, n137, n138, n139, n140, n141, n142,
         n143, n144, n145, n146, n147, n148, n149, n150, n151, n152, n153,
         n154, n155, n156, n157, n158, n159, n160, n161, n162, n163, n164,
         n165, n166, n167, n168, n169, n170, n171, n172, n173, n174, n175,
         n176, n177, n178, n179, n180, n181, n182, n183, n184, n185, n186,
         n187, n188, n189, n190, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12,
         n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26,
         n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40,
         n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54,
         n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68,
         n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81, n82,
         n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95, n96,
         n97, n98, n99, n100, n101, n102, n103, n104, n105, n106, n107, n108,
         n109, n110, n111, n112, n113, n114, n115, n116;
  wire   [31:0] timer_count_r;
  wire   [31:0] timer_compare_r;
  wire   [2:0] csr_w;
  tri   clk_i;
  tri   reset_i;
  tri   [31:0] addr_i;
  tri   [31:0] wdata_i;
  tri   [31:0] rdata_o;
  tri   wr_i;
  assign rdata_o[31] = 1'b0;
  assign rdata_o[30] = 1'b0;
  assign rdata_o[29] = 1'b0;
  assign rdata_o[28] = 1'b0;
  assign rdata_o[27] = 1'b0;
  assign rdata_o[26] = 1'b0;
  assign rdata_o[25] = 1'b0;
  assign rdata_o[24] = 1'b0;
  assign rdata_o[23] = 1'b0;
  assign rdata_o[22] = 1'b0;
  assign rdata_o[21] = 1'b0;
  assign rdata_o[20] = 1'b0;
  assign rdata_o[19] = 1'b0;
  assign rdata_o[18] = 1'b0;
  assign rdata_o[17] = 1'b0;
  assign rdata_o[16] = 1'b0;
  assign rdata_o[15] = 1'b0;
  assign rdata_o[14] = 1'b0;
  assign rdata_o[13] = 1'b0;
  assign rdata_o[12] = 1'b0;
  assign rdata_o[11] = 1'b0;
  assign rdata_o[10] = 1'b0;
  assign rdata_o[9] = 1'b0;
  assign rdata_o[8] = 1'b0;
  assign rdata_o[7] = 1'b0;
  assign rdata_o[6] = 1'b0;
  assign rdata_o[5] = 1'b0;
  assign rdata_o[4] = 1'b0;
  assign rdata_o[3] = 1'b0;

  per_timer_DW01_inc_0 add_100 ( .A(timer_count_r), .SUM({N62, N61, N60, N59, 
        N58, N57, N56, N55, N54, N53, N52, N51, N50, N49, N48, N47, N46, N45, 
        N44, N43, N42, N41, N40, N39, N38, N37, N36, N35, N34, N33, N32, N31})
         );
  per_timer_DW01_cmp6_0 eq_63 ( .A(timer_count_r), .B(timer_compare_r), .TC(
        1'b0), .EQ(N1) );
  FFDQHD1X timer_overflow_r_reg ( .D(n157), .CK(clk_i), .Q(csr_w[2]) );
  FFDQHD1X timer_compare_r_reg_31_ ( .D(n189), .CK(clk_i), .Q(
        timer_compare_r[31]) );
  FFDQHD1X timer_compare_r_reg_30_ ( .D(n188), .CK(clk_i), .Q(
        timer_compare_r[30]) );
  FFDQHD1X timer_compare_r_reg_29_ ( .D(n187), .CK(clk_i), .Q(
        timer_compare_r[29]) );
  FFDQHD1X timer_compare_r_reg_28_ ( .D(n186), .CK(clk_i), .Q(
        timer_compare_r[28]) );
  FFDQHD1X timer_compare_r_reg_27_ ( .D(n185), .CK(clk_i), .Q(
        timer_compare_r[27]) );
  FFDQHD1X timer_compare_r_reg_26_ ( .D(n184), .CK(clk_i), .Q(
        timer_compare_r[26]) );
  FFDQHD1X timer_compare_r_reg_25_ ( .D(n183), .CK(clk_i), .Q(
        timer_compare_r[25]) );
  FFDQHD1X timer_compare_r_reg_24_ ( .D(n182), .CK(clk_i), .Q(
        timer_compare_r[24]) );
  FFDQHD1X timer_compare_r_reg_23_ ( .D(n181), .CK(clk_i), .Q(
        timer_compare_r[23]) );
  FFDQHD1X timer_compare_r_reg_22_ ( .D(n180), .CK(clk_i), .Q(
        timer_compare_r[22]) );
  FFDQHD1X timer_compare_r_reg_21_ ( .D(n179), .CK(clk_i), .Q(
        timer_compare_r[21]) );
  FFDQHD1X timer_compare_r_reg_20_ ( .D(n178), .CK(clk_i), .Q(
        timer_compare_r[20]) );
  FFDQHD1X timer_compare_r_reg_19_ ( .D(n177), .CK(clk_i), .Q(
        timer_compare_r[19]) );
  FFDQHD1X timer_compare_r_reg_18_ ( .D(n176), .CK(clk_i), .Q(
        timer_compare_r[18]) );
  FFDQHD1X timer_compare_r_reg_17_ ( .D(n175), .CK(clk_i), .Q(
        timer_compare_r[17]) );
  FFDQHD1X timer_compare_r_reg_16_ ( .D(n174), .CK(clk_i), .Q(
        timer_compare_r[16]) );
  FFDQHD1X timer_compare_r_reg_15_ ( .D(n173), .CK(clk_i), .Q(
        timer_compare_r[15]) );
  FFDQHD1X timer_compare_r_reg_14_ ( .D(n172), .CK(clk_i), .Q(
        timer_compare_r[14]) );
  FFDQHD1X timer_compare_r_reg_13_ ( .D(n171), .CK(clk_i), .Q(
        timer_compare_r[13]) );
  FFDQHD1X timer_compare_r_reg_12_ ( .D(n170), .CK(clk_i), .Q(
        timer_compare_r[12]) );
  FFDQHD1X timer_compare_r_reg_11_ ( .D(n169), .CK(clk_i), .Q(
        timer_compare_r[11]) );
  FFDQHD1X timer_compare_r_reg_10_ ( .D(n168), .CK(clk_i), .Q(
        timer_compare_r[10]) );
  FFDQHD1X timer_compare_r_reg_9_ ( .D(n167), .CK(clk_i), .Q(
        timer_compare_r[9]) );
  FFDQHD1X timer_compare_r_reg_8_ ( .D(n166), .CK(clk_i), .Q(
        timer_compare_r[8]) );
  FFDQHD1X timer_compare_r_reg_7_ ( .D(n165), .CK(clk_i), .Q(
        timer_compare_r[7]) );
  FFDQHD1X timer_compare_r_reg_6_ ( .D(n164), .CK(clk_i), .Q(
        timer_compare_r[6]) );
  FFDQHD1X timer_compare_r_reg_5_ ( .D(n163), .CK(clk_i), .Q(
        timer_compare_r[5]) );
  FFDQHD1X timer_compare_r_reg_4_ ( .D(n162), .CK(clk_i), .Q(
        timer_compare_r[4]) );
  FFDQHD1X timer_compare_r_reg_3_ ( .D(n161), .CK(clk_i), .Q(
        timer_compare_r[3]) );
  FFDQHD1X timer_compare_r_reg_2_ ( .D(n160), .CK(clk_i), .Q(
        timer_compare_r[2]) );
  FFDQHD1X timer_enabled_r_reg ( .D(n190), .CK(clk_i), .Q(csr_w[0]) );
  FFDQHD1X timer_compare_r_reg_0_ ( .D(n158), .CK(clk_i), .Q(
        timer_compare_r[0]) );
  FFDQHD1X timer_count_r_reg_31_ ( .D(n125), .CK(clk_i), .Q(timer_count_r[31])
         );
  FFDQHD1X timer_compare_r_reg_1_ ( .D(n159), .CK(clk_i), .Q(
        timer_compare_r[1]) );
  FFDQHD1X timer_count_r_reg_2_ ( .D(n154), .CK(clk_i), .Q(timer_count_r[2])
         );
  FFDQHD1X timer_count_r_reg_3_ ( .D(n153), .CK(clk_i), .Q(timer_count_r[3])
         );
  FFDQHD1X timer_count_r_reg_4_ ( .D(n152), .CK(clk_i), .Q(timer_count_r[4])
         );
  FFDQHD1X timer_count_r_reg_5_ ( .D(n151), .CK(clk_i), .Q(timer_count_r[5])
         );
  FFDQHD1X timer_count_r_reg_6_ ( .D(n150), .CK(clk_i), .Q(timer_count_r[6])
         );
  FFDQHD1X timer_count_r_reg_7_ ( .D(n149), .CK(clk_i), .Q(timer_count_r[7])
         );
  FFDQHD1X timer_count_r_reg_8_ ( .D(n148), .CK(clk_i), .Q(timer_count_r[8])
         );
  FFDQHD1X timer_count_r_reg_9_ ( .D(n147), .CK(clk_i), .Q(timer_count_r[9])
         );
  FFDQHD1X timer_count_r_reg_10_ ( .D(n146), .CK(clk_i), .Q(timer_count_r[10])
         );
  FFDQHD1X timer_count_r_reg_11_ ( .D(n145), .CK(clk_i), .Q(timer_count_r[11])
         );
  FFDQHD1X timer_count_r_reg_12_ ( .D(n144), .CK(clk_i), .Q(timer_count_r[12])
         );
  FFDQHD1X timer_count_r_reg_13_ ( .D(n143), .CK(clk_i), .Q(timer_count_r[13])
         );
  FFDQHD1X timer_count_r_reg_14_ ( .D(n142), .CK(clk_i), .Q(timer_count_r[14])
         );
  FFDQHD1X timer_count_r_reg_15_ ( .D(n141), .CK(clk_i), .Q(timer_count_r[15])
         );
  FFDQHD1X timer_count_r_reg_16_ ( .D(n140), .CK(clk_i), .Q(timer_count_r[16])
         );
  FFDQHD1X timer_count_r_reg_17_ ( .D(n139), .CK(clk_i), .Q(timer_count_r[17])
         );
  FFDQHD1X timer_count_r_reg_18_ ( .D(n138), .CK(clk_i), .Q(timer_count_r[18])
         );
  FFDQHD1X timer_count_r_reg_19_ ( .D(n137), .CK(clk_i), .Q(timer_count_r[19])
         );
  FFDQHD1X timer_count_r_reg_20_ ( .D(n136), .CK(clk_i), .Q(timer_count_r[20])
         );
  FFDQHD1X timer_count_r_reg_21_ ( .D(n135), .CK(clk_i), .Q(timer_count_r[21])
         );
  FFDQHD1X timer_count_r_reg_22_ ( .D(n134), .CK(clk_i), .Q(timer_count_r[22])
         );
  FFDQHD1X timer_count_r_reg_23_ ( .D(n133), .CK(clk_i), .Q(timer_count_r[23])
         );
  FFDQHD1X timer_count_r_reg_24_ ( .D(n132), .CK(clk_i), .Q(timer_count_r[24])
         );
  FFDQHD1X timer_count_r_reg_25_ ( .D(n131), .CK(clk_i), .Q(timer_count_r[25])
         );
  FFDQHD1X timer_count_r_reg_26_ ( .D(n130), .CK(clk_i), .Q(timer_count_r[26])
         );
  FFDQHD1X timer_count_r_reg_27_ ( .D(n129), .CK(clk_i), .Q(timer_count_r[27])
         );
  FFDQHD1X timer_count_r_reg_28_ ( .D(n128), .CK(clk_i), .Q(timer_count_r[28])
         );
  FFDQHD1X timer_count_r_reg_29_ ( .D(n127), .CK(clk_i), .Q(timer_count_r[29])
         );
  FFDQHD1X timer_count_r_reg_30_ ( .D(n126), .CK(clk_i), .Q(timer_count_r[30])
         );
  FFDQHD1X timer_count_r_reg_1_ ( .D(n155), .CK(clk_i), .Q(timer_count_r[1])
         );
  FFDQHD1X timer_count_r_reg_0_ ( .D(n156), .CK(clk_i), .Q(timer_count_r[0])
         );
  FFDQHD1X reg_data_r_reg_1_ ( .D(n116), .CK(clk_i), .Q(rdata_o[1]) );
  FFDQHD1X reg_data_r_reg_2_ ( .D(csr_w[2]), .CK(clk_i), .Q(rdata_o[2]) );
  FFDQHD1X reg_data_r_reg_0_ ( .D(csr_w[0]), .CK(clk_i), .Q(rdata_o[0]) );
  NOR4HD2X U33 ( .A(n45), .B(n50), .C(n48), .D(reset_i), .Z(n49) );
  NOR4B1HD1X U34 ( .AN(n113), .B(n116), .C(N1), .D(reset_i), .Z(n48) );
  NAND4B1HD1X U35 ( .AN(addr_i[2]), .B(addr_i[3]), .C(n41), .D(n40), .Z(n8) );
  NOR2HD2X U36 ( .A(n113), .B(reset_i), .Z(n50) );
  MUX2HDLX U37 ( .A(n2), .B(csr_w[0]), .S0(n3), .Z(n190) );
  AOI21HDLX U38 ( .A(n4), .B(n5), .C(reset_i), .Z(n3) );
  NAND2HDUX U39 ( .A(n6), .B(n7), .Z(n5) );
  NOR2HDUX U40 ( .A(reset_i), .B(n6), .Z(n2) );
  OAI22B2HDLX U41 ( .C(n8), .D(n9), .AN(timer_compare_r[31]), .BN(n10), .Z(
        n189) );
  INVCLKHD1X U42 ( .A(wdata_i[31]), .Z(n9) );
  OAI22B2HDLX U43 ( .C(n8), .D(n11), .AN(timer_compare_r[30]), .BN(n10), .Z(
        n188) );
  INVCLKHD1X U44 ( .A(wdata_i[30]), .Z(n11) );
  OAI22B2HDLX U45 ( .C(n8), .D(n12), .AN(timer_compare_r[29]), .BN(n10), .Z(
        n187) );
  INVCLKHD1X U46 ( .A(wdata_i[29]), .Z(n12) );
  OAI22B2HDLX U47 ( .C(n8), .D(n13), .AN(timer_compare_r[28]), .BN(n10), .Z(
        n186) );
  INVCLKHD1X U48 ( .A(wdata_i[28]), .Z(n13) );
  OAI22B2HDLX U49 ( .C(n8), .D(n14), .AN(timer_compare_r[27]), .BN(n10), .Z(
        n185) );
  INVCLKHD1X U50 ( .A(wdata_i[27]), .Z(n14) );
  OAI22B2HDLX U51 ( .C(n8), .D(n15), .AN(timer_compare_r[26]), .BN(n10), .Z(
        n184) );
  INVCLKHD1X U52 ( .A(wdata_i[26]), .Z(n15) );
  OAI22B2HDLX U53 ( .C(n8), .D(n16), .AN(timer_compare_r[25]), .BN(n10), .Z(
        n183) );
  INVCLKHD1X U54 ( .A(wdata_i[25]), .Z(n16) );
  OAI22B2HDLX U55 ( .C(n8), .D(n17), .AN(timer_compare_r[24]), .BN(n10), .Z(
        n182) );
  INVCLKHD1X U56 ( .A(wdata_i[24]), .Z(n17) );
  OAI22B2HDLX U57 ( .C(n8), .D(n18), .AN(timer_compare_r[23]), .BN(n10), .Z(
        n181) );
  INVCLKHD1X U58 ( .A(wdata_i[23]), .Z(n18) );
  OAI22B2HDLX U59 ( .C(n8), .D(n19), .AN(timer_compare_r[22]), .BN(n10), .Z(
        n180) );
  INVCLKHD1X U60 ( .A(wdata_i[22]), .Z(n19) );
  OAI22B2HDLX U61 ( .C(n8), .D(n20), .AN(timer_compare_r[21]), .BN(n10), .Z(
        n179) );
  INVCLKHD1X U62 ( .A(wdata_i[21]), .Z(n20) );
  OAI22B2HDLX U63 ( .C(n8), .D(n21), .AN(timer_compare_r[20]), .BN(n10), .Z(
        n178) );
  INVCLKHD1X U64 ( .A(wdata_i[20]), .Z(n21) );
  OAI22B2HDLX U65 ( .C(n8), .D(n22), .AN(timer_compare_r[19]), .BN(n10), .Z(
        n177) );
  INVCLKHD1X U66 ( .A(wdata_i[19]), .Z(n22) );
  OAI22B2HDLX U67 ( .C(n8), .D(n23), .AN(timer_compare_r[18]), .BN(n10), .Z(
        n176) );
  INVCLKHD1X U68 ( .A(wdata_i[18]), .Z(n23) );
  OAI22B2HDLX U69 ( .C(n8), .D(n24), .AN(timer_compare_r[17]), .BN(n10), .Z(
        n175) );
  INVCLKHD1X U70 ( .A(wdata_i[17]), .Z(n24) );
  OAI22B2HDLX U71 ( .C(n8), .D(n25), .AN(timer_compare_r[16]), .BN(n10), .Z(
        n174) );
  INVCLKHD1X U72 ( .A(wdata_i[16]), .Z(n25) );
  OAI22B2HDLX U73 ( .C(n8), .D(n26), .AN(timer_compare_r[15]), .BN(n10), .Z(
        n173) );
  INVCLKHD1X U74 ( .A(wdata_i[15]), .Z(n26) );
  OAI22B2HDLX U75 ( .C(n8), .D(n27), .AN(timer_compare_r[14]), .BN(n10), .Z(
        n172) );
  INVCLKHD1X U76 ( .A(wdata_i[14]), .Z(n27) );
  OAI22B2HDLX U77 ( .C(n8), .D(n28), .AN(timer_compare_r[13]), .BN(n10), .Z(
        n171) );
  INVCLKHD1X U78 ( .A(wdata_i[13]), .Z(n28) );
  OAI22B2HDLX U79 ( .C(n8), .D(n29), .AN(timer_compare_r[12]), .BN(n10), .Z(
        n170) );
  INVCLKHD1X U80 ( .A(wdata_i[12]), .Z(n29) );
  OAI22B2HDLX U81 ( .C(n8), .D(n30), .AN(timer_compare_r[11]), .BN(n10), .Z(
        n169) );
  INVCLKHD1X U82 ( .A(wdata_i[11]), .Z(n30) );
  OAI22B2HDLX U83 ( .C(n8), .D(n31), .AN(timer_compare_r[10]), .BN(n10), .Z(
        n168) );
  INVCLKHD1X U84 ( .A(wdata_i[10]), .Z(n31) );
  OAI22B2HDLX U85 ( .C(n8), .D(n32), .AN(timer_compare_r[9]), .BN(n10), .Z(
        n167) );
  INVCLKHD1X U86 ( .A(wdata_i[9]), .Z(n32) );
  OAI22B2HDLX U87 ( .C(n8), .D(n33), .AN(timer_compare_r[8]), .BN(n10), .Z(
        n166) );
  INVCLKHD1X U88 ( .A(wdata_i[8]), .Z(n33) );
  OAI22B2HDLX U89 ( .C(n8), .D(n34), .AN(timer_compare_r[7]), .BN(n10), .Z(
        n165) );
  INVCLKHD1X U90 ( .A(wdata_i[7]), .Z(n34) );
  OAI22B2HDLX U91 ( .C(n8), .D(n35), .AN(timer_compare_r[6]), .BN(n10), .Z(
        n164) );
  INVCLKHD1X U92 ( .A(wdata_i[6]), .Z(n35) );
  OAI22B2HDLX U93 ( .C(n8), .D(n36), .AN(timer_compare_r[5]), .BN(n10), .Z(
        n163) );
  INVCLKHD1X U94 ( .A(wdata_i[5]), .Z(n36) );
  OAI22B2HDLX U95 ( .C(n8), .D(n37), .AN(timer_compare_r[4]), .BN(n10), .Z(
        n162) );
  INVCLKHD1X U96 ( .A(wdata_i[4]), .Z(n37) );
  OAI22B2HDLX U97 ( .C(n8), .D(n38), .AN(timer_compare_r[3]), .BN(n10), .Z(
        n161) );
  INVCLKHD1X U98 ( .A(wdata_i[3]), .Z(n38) );
  OAI22B2HDLX U99 ( .C(n8), .D(n39), .AN(timer_compare_r[2]), .BN(n10), .Z(
        n160) );
  INVCLKHD1X U100 ( .A(wdata_i[2]), .Z(n39) );
  OAI22B2HDLX U101 ( .C(n8), .D(n7), .AN(timer_compare_r[1]), .BN(n10), .Z(
        n159) );
  INVCLKHD1X U102 ( .A(wdata_i[1]), .Z(n7) );
  OAI22B2HDLX U103 ( .C(n6), .D(n8), .AN(timer_compare_r[0]), .BN(n10), .Z(
        n158) );
  AND2CLKHD1X U104 ( .A(n8), .B(n40), .Z(n10) );
  INVCLKHD1X U105 ( .A(reset_i), .Z(n40) );
  INVCLKHD1X U106 ( .A(wdata_i[0]), .Z(n6) );
  MUX2HDLX U107 ( .A(n42), .B(csr_w[2]), .S0(n43), .Z(n157) );
  NOR3HD1X U108 ( .A(n44), .B(reset_i), .C(n45), .Z(n43) );
  NOR2HDUX U109 ( .A(reset_i), .B(n44), .Z(n42) );
  AND2CLKHD1X U110 ( .A(n4), .B(wdata_i[2]), .Z(n44) );
  NOR3B1HDLX U111 ( .AN(n41), .B(addr_i[2]), .C(addr_i[3]), .Z(n4) );
  NAND2HDUX U112 ( .A(n46), .B(n47), .Z(n156) );
  NAND2HDUX U113 ( .A(N31), .B(n48), .Z(n47) );
  AOI22HDLX U114 ( .A(timer_count_r[0]), .B(n49), .C(n50), .D(wdata_i[0]), .Z(
        n46) );
  NAND2HDUX U115 ( .A(n51), .B(n52), .Z(n155) );
  NAND2HDUX U116 ( .A(N32), .B(n48), .Z(n52) );
  AOI22HDLX U117 ( .A(timer_count_r[1]), .B(n49), .C(n50), .D(wdata_i[1]), .Z(
        n51) );
  NAND2HDUX U118 ( .A(n53), .B(n54), .Z(n154) );
  NAND2HDUX U119 ( .A(N33), .B(n48), .Z(n54) );
  AOI22HDLX U120 ( .A(timer_count_r[2]), .B(n49), .C(n50), .D(wdata_i[2]), .Z(
        n53) );
  NAND2HDUX U121 ( .A(n55), .B(n56), .Z(n153) );
  NAND2HDUX U122 ( .A(N34), .B(n48), .Z(n56) );
  AOI22HDLX U123 ( .A(timer_count_r[3]), .B(n49), .C(n50), .D(wdata_i[3]), .Z(
        n55) );
  NAND2HDUX U124 ( .A(n57), .B(n58), .Z(n152) );
  NAND2HDUX U125 ( .A(N35), .B(n48), .Z(n58) );
  AOI22HDLX U126 ( .A(timer_count_r[4]), .B(n49), .C(n50), .D(wdata_i[4]), .Z(
        n57) );
  NAND2HDUX U127 ( .A(n59), .B(n60), .Z(n151) );
  NAND2HDUX U128 ( .A(N36), .B(n48), .Z(n60) );
  AOI22HDLX U129 ( .A(timer_count_r[5]), .B(n49), .C(n50), .D(wdata_i[5]), .Z(
        n59) );
  NAND2HDUX U130 ( .A(n61), .B(n62), .Z(n150) );
  NAND2HDUX U131 ( .A(N37), .B(n48), .Z(n62) );
  AOI22HDLX U132 ( .A(timer_count_r[6]), .B(n49), .C(n50), .D(wdata_i[6]), .Z(
        n61) );
  NAND2HDUX U133 ( .A(n63), .B(n64), .Z(n149) );
  NAND2HDUX U134 ( .A(N38), .B(n48), .Z(n64) );
  AOI22HDLX U135 ( .A(timer_count_r[7]), .B(n49), .C(n50), .D(wdata_i[7]), .Z(
        n63) );
  NAND2HDUX U136 ( .A(n65), .B(n66), .Z(n148) );
  NAND2HDUX U137 ( .A(N39), .B(n48), .Z(n66) );
  AOI22HDLX U138 ( .A(timer_count_r[8]), .B(n49), .C(n50), .D(wdata_i[8]), .Z(
        n65) );
  NAND2HDUX U139 ( .A(n67), .B(n68), .Z(n147) );
  NAND2HDUX U140 ( .A(N40), .B(n48), .Z(n68) );
  AOI22HDLX U141 ( .A(timer_count_r[9]), .B(n49), .C(n50), .D(wdata_i[9]), .Z(
        n67) );
  NAND2HDUX U142 ( .A(n69), .B(n70), .Z(n146) );
  NAND2HDUX U143 ( .A(N41), .B(n48), .Z(n70) );
  AOI22HDLX U144 ( .A(timer_count_r[10]), .B(n49), .C(n50), .D(wdata_i[10]), 
        .Z(n69) );
  NAND2HDUX U145 ( .A(n71), .B(n72), .Z(n145) );
  NAND2HDUX U146 ( .A(N42), .B(n48), .Z(n72) );
  AOI22HDLX U147 ( .A(timer_count_r[11]), .B(n49), .C(n50), .D(wdata_i[11]), 
        .Z(n71) );
  NAND2HDUX U148 ( .A(n73), .B(n74), .Z(n144) );
  NAND2HDUX U149 ( .A(N43), .B(n48), .Z(n74) );
  AOI22HDLX U150 ( .A(timer_count_r[12]), .B(n49), .C(n50), .D(wdata_i[12]), 
        .Z(n73) );
  NAND2HDUX U151 ( .A(n75), .B(n76), .Z(n143) );
  NAND2HDUX U152 ( .A(N44), .B(n48), .Z(n76) );
  AOI22HDLX U153 ( .A(timer_count_r[13]), .B(n49), .C(n50), .D(wdata_i[13]), 
        .Z(n75) );
  NAND2HDUX U154 ( .A(n77), .B(n78), .Z(n142) );
  NAND2HDUX U155 ( .A(N45), .B(n48), .Z(n78) );
  AOI22HDLX U156 ( .A(timer_count_r[14]), .B(n49), .C(n50), .D(wdata_i[14]), 
        .Z(n77) );
  NAND2HDUX U157 ( .A(n79), .B(n80), .Z(n141) );
  NAND2HDUX U158 ( .A(N46), .B(n48), .Z(n80) );
  AOI22HDLX U159 ( .A(timer_count_r[15]), .B(n49), .C(n50), .D(wdata_i[15]), 
        .Z(n79) );
  NAND2HDUX U160 ( .A(n81), .B(n82), .Z(n140) );
  NAND2HDUX U161 ( .A(N47), .B(n48), .Z(n82) );
  AOI22HDLX U162 ( .A(timer_count_r[16]), .B(n49), .C(n50), .D(wdata_i[16]), 
        .Z(n81) );
  NAND2HDUX U163 ( .A(n83), .B(n84), .Z(n139) );
  NAND2HDUX U164 ( .A(N48), .B(n48), .Z(n84) );
  AOI22HDLX U165 ( .A(timer_count_r[17]), .B(n49), .C(n50), .D(wdata_i[17]), 
        .Z(n83) );
  NAND2HDUX U166 ( .A(n85), .B(n86), .Z(n138) );
  NAND2HDUX U167 ( .A(N49), .B(n48), .Z(n86) );
  AOI22HDLX U168 ( .A(timer_count_r[18]), .B(n49), .C(n50), .D(wdata_i[18]), 
        .Z(n85) );
  NAND2HDUX U169 ( .A(n87), .B(n88), .Z(n137) );
  NAND2HDUX U170 ( .A(N50), .B(n48), .Z(n88) );
  AOI22HDLX U171 ( .A(timer_count_r[19]), .B(n49), .C(n50), .D(wdata_i[19]), 
        .Z(n87) );
  NAND2HDUX U172 ( .A(n89), .B(n90), .Z(n136) );
  NAND2HDUX U173 ( .A(N51), .B(n48), .Z(n90) );
  AOI22HDLX U174 ( .A(timer_count_r[20]), .B(n49), .C(n50), .D(wdata_i[20]), 
        .Z(n89) );
  NAND2HDUX U175 ( .A(n91), .B(n92), .Z(n135) );
  NAND2HDUX U176 ( .A(N52), .B(n48), .Z(n92) );
  AOI22HDLX U177 ( .A(timer_count_r[21]), .B(n49), .C(n50), .D(wdata_i[21]), 
        .Z(n91) );
  NAND2HDUX U178 ( .A(n93), .B(n94), .Z(n134) );
  NAND2HDUX U179 ( .A(N53), .B(n48), .Z(n94) );
  AOI22HDLX U180 ( .A(timer_count_r[22]), .B(n49), .C(n50), .D(wdata_i[22]), 
        .Z(n93) );
  NAND2HDUX U181 ( .A(n95), .B(n96), .Z(n133) );
  NAND2HDUX U182 ( .A(N54), .B(n48), .Z(n96) );
  AOI22HDLX U183 ( .A(timer_count_r[23]), .B(n49), .C(n50), .D(wdata_i[23]), 
        .Z(n95) );
  NAND2HDUX U184 ( .A(n97), .B(n98), .Z(n132) );
  NAND2HDUX U185 ( .A(N55), .B(n48), .Z(n98) );
  AOI22HDLX U186 ( .A(timer_count_r[24]), .B(n49), .C(n50), .D(wdata_i[24]), 
        .Z(n97) );
  NAND2HDUX U187 ( .A(n99), .B(n100), .Z(n131) );
  NAND2HDUX U188 ( .A(N56), .B(n48), .Z(n100) );
  AOI22HDLX U189 ( .A(timer_count_r[25]), .B(n49), .C(n50), .D(wdata_i[25]), 
        .Z(n99) );
  NAND2HDUX U190 ( .A(n101), .B(n102), .Z(n130) );
  NAND2HDUX U191 ( .A(N57), .B(n48), .Z(n102) );
  AOI22HDLX U192 ( .A(timer_count_r[26]), .B(n49), .C(n50), .D(wdata_i[26]), 
        .Z(n101) );
  NAND2HDUX U193 ( .A(n103), .B(n104), .Z(n129) );
  NAND2HDUX U194 ( .A(N58), .B(n48), .Z(n104) );
  AOI22HDLX U195 ( .A(timer_count_r[27]), .B(n49), .C(n50), .D(wdata_i[27]), 
        .Z(n103) );
  NAND2HDUX U196 ( .A(n105), .B(n106), .Z(n128) );
  NAND2HDUX U197 ( .A(N59), .B(n48), .Z(n106) );
  AOI22HDLX U198 ( .A(timer_count_r[28]), .B(n49), .C(n50), .D(wdata_i[28]), 
        .Z(n105) );
  NAND2HDUX U199 ( .A(n107), .B(n108), .Z(n127) );
  NAND2HDUX U200 ( .A(N60), .B(n48), .Z(n108) );
  AOI22HDLX U201 ( .A(timer_count_r[29]), .B(n49), .C(n50), .D(wdata_i[29]), 
        .Z(n107) );
  NAND2HDUX U202 ( .A(n109), .B(n110), .Z(n126) );
  NAND2HDUX U203 ( .A(N61), .B(n48), .Z(n110) );
  AOI22HDLX U204 ( .A(timer_count_r[30]), .B(n49), .C(n50), .D(wdata_i[30]), 
        .Z(n109) );
  NAND2HDUX U205 ( .A(n111), .B(n112), .Z(n125) );
  NAND2HDUX U206 ( .A(N62), .B(n48), .Z(n112) );
  AOI22HDLX U207 ( .A(timer_count_r[31]), .B(n49), .C(n50), .D(wdata_i[31]), 
        .Z(n111) );
  NAND3B1HDLX U208 ( .AN(addr_i[3]), .B(n41), .C(addr_i[2]), .Z(n113) );
  AND3HD1X U209 ( .A(n114), .B(wr_i), .C(n115), .Z(n41) );
  NOR4HDLX U210 ( .A(addr_i[7]), .B(addr_i[6]), .C(addr_i[5]), .D(addr_i[4]), 
        .Z(n115) );
  NOR2HDUX U211 ( .A(addr_i[1]), .B(addr_i[0]), .Z(n114) );
  NOR2B1HD1X U212 ( .AN(N1), .B(n116), .Z(n45) );
  INVCLKHD1X U213 ( .A(csr_w[0]), .Z(n116) );
endmodule


module soc ( clk_i, reset_i, lock_o, uart_rx_i, uart_tx_o, gpio_in_i, 
        gpio_out_o );
  input [31:0] gpio_in_i;
  output [31:0] gpio_out_o;
  input clk_i, reset_i, uart_rx_i;
  output lock_o, uart_tx_o;
  wire   N2, N7, N12, N18, n6, n7, n8, SYNOPSYS_UNCONNECTED_1,
         SYNOPSYS_UNCONNECTED_2, SYNOPSYS_UNCONNECTED_3,
         SYNOPSYS_UNCONNECTED_4, SYNOPSYS_UNCONNECTED_5,
         SYNOPSYS_UNCONNECTED_6, SYNOPSYS_UNCONNECTED_7,
         SYNOPSYS_UNCONNECTED_8, SYNOPSYS_UNCONNECTED_9,
         SYNOPSYS_UNCONNECTED_10, SYNOPSYS_UNCONNECTED_11,
         SYNOPSYS_UNCONNECTED_12, SYNOPSYS_UNCONNECTED_13,
         SYNOPSYS_UNCONNECTED_14, SYNOPSYS_UNCONNECTED_15,
         SYNOPSYS_UNCONNECTED_16, SYNOPSYS_UNCONNECTED_17,
         SYNOPSYS_UNCONNECTED_18, SYNOPSYS_UNCONNECTED_19,
         SYNOPSYS_UNCONNECTED_20, SYNOPSYS_UNCONNECTED_21,
         SYNOPSYS_UNCONNECTED_22, SYNOPSYS_UNCONNECTED_23,
         SYNOPSYS_UNCONNECTED_24, SYNOPSYS_UNCONNECTED_25,
         SYNOPSYS_UNCONNECTED_26, SYNOPSYS_UNCONNECTED_27,
         SYNOPSYS_UNCONNECTED_28, SYNOPSYS_UNCONNECTED_29,
         SYNOPSYS_UNCONNECTED_30, SYNOPSYS_UNCONNECTED_31,
         SYNOPSYS_UNCONNECTED_32, SYNOPSYS_UNCONNECTED_33,
         SYNOPSYS_UNCONNECTED_34, SYNOPSYS_UNCONNECTED_35,
         SYNOPSYS_UNCONNECTED_36, SYNOPSYS_UNCONNECTED_37,
         SYNOPSYS_UNCONNECTED_38, SYNOPSYS_UNCONNECTED_39,
         SYNOPSYS_UNCONNECTED_40, SYNOPSYS_UNCONNECTED_41,
         SYNOPSYS_UNCONNECTED_42, SYNOPSYS_UNCONNECTED_43,
         SYNOPSYS_UNCONNECTED_44, SYNOPSYS_UNCONNECTED_45,
         SYNOPSYS_UNCONNECTED_46, SYNOPSYS_UNCONNECTED_47,
         SYNOPSYS_UNCONNECTED_48, SYNOPSYS_UNCONNECTED_49,
         SYNOPSYS_UNCONNECTED_50, SYNOPSYS_UNCONNECTED_51,
         SYNOPSYS_UNCONNECTED_52, SYNOPSYS_UNCONNECTED_53;
  tri   clk_i;
  tri   reset_i;
  tri   lock_o;
  tri   [31:28] daddr_w;
  tri   uart_wr_w;
  tri   gpio_wr_w;
  tri   timer_wr_w;
  tri   uart_rd_w;
  tri   gpio_rd_w;
  tri   timer_rd_w;
  tri   [1:0] uart_size_w;
  tri   [1:0] gpio_size_w;
  tri   [1:0] timer_size_w;
  tri   [31:0] uart_rdata_w;
  tri   [31:0] gpio_rdata_w;
  tri   [31:0] timer_rdata_w;
  tri   [31:0] uart_wdata_w;
  tri   [31:0] gpio_wdata_w;
  tri   [31:0] timer_wdata_w;
  tri   [31:0] uart_addr_w;
  tri   [31:0] gpio_addr_w;
  tri   [31:0] timer_addr_w;
  tri   N3;
  tri   N8;
  tri   N13;
  tri   N19;
  tri   mem_wr_w;
  tri   mem_wdata_w_9_;
  tri   mem_wdata_w_8_;
  tri   mem_wdata_w_7_;
  tri   mem_wdata_w_6_;
  tri   mem_wdata_w_5_;
  tri   mem_wdata_w_4_;
  tri   mem_wdata_w_3_;
  tri   mem_wdata_w_31_;
  tri   mem_wdata_w_30_;
  tri   mem_wdata_w_2_;
  tri   mem_wdata_w_29_;
  tri   mem_wdata_w_28_;
  tri   mem_wdata_w_27_;
  tri   mem_wdata_w_26_;
  tri   mem_wdata_w_25_;
  tri   mem_wdata_w_24_;
  tri   mem_wdata_w_23_;
  tri   mem_wdata_w_22_;
  tri   mem_wdata_w_21_;
  tri   mem_wdata_w_20_;
  tri   mem_wdata_w_1_;
  tri   mem_wdata_w_19_;
  tri   mem_wdata_w_18_;
  tri   mem_wdata_w_17_;
  tri   mem_wdata_w_16_;
  tri   mem_wdata_w_15_;
  tri   mem_wdata_w_14_;
  tri   mem_wdata_w_13_;
  tri   mem_wdata_w_12_;
  tri   mem_wdata_w_11_;
  tri   mem_wdata_w_10_;
  tri   mem_wdata_w_0_;
  tri   mem_size_w_1_;
  tri   mem_size_w_0_;
  tri   mem_rdata_w_9_;
  tri   mem_rdata_w_8_;
  tri   mem_rdata_w_7_;
  tri   mem_rdata_w_6_;
  tri   mem_rdata_w_5_;
  tri   mem_rdata_w_4_;
  tri   mem_rdata_w_3_;
  tri   mem_rdata_w_31_;
  tri   mem_rdata_w_30_;
  tri   mem_rdata_w_2_;
  tri   mem_rdata_w_29_;
  tri   mem_rdata_w_28_;
  tri   mem_rdata_w_27_;
  tri   mem_rdata_w_26_;
  tri   mem_rdata_w_25_;
  tri   mem_rdata_w_24_;
  tri   mem_rdata_w_23_;
  tri   mem_rdata_w_22_;
  tri   mem_rdata_w_21_;
  tri   mem_rdata_w_20_;
  tri   mem_rdata_w_1_;
  tri   mem_rdata_w_19_;
  tri   mem_rdata_w_18_;
  tri   mem_rdata_w_17_;
  tri   mem_rdata_w_16_;
  tri   mem_rdata_w_15_;
  tri   mem_rdata_w_14_;
  tri   mem_rdata_w_13_;
  tri   mem_rdata_w_12_;
  tri   mem_rdata_w_11_;
  tri   mem_rdata_w_10_;
  tri   mem_rdata_w_0_;
  tri   mem_rd_w;
  tri   mem_addr_w_9_;
  tri   mem_addr_w_8_;
  tri   mem_addr_w_7_;
  tri   mem_addr_w_6_;
  tri   mem_addr_w_5_;
  tri   mem_addr_w_4_;
  tri   mem_addr_w_3_;
  tri   mem_addr_w_31_;
  tri   mem_addr_w_30_;
  tri   mem_addr_w_2_;
  tri   mem_addr_w_29_;
  tri   mem_addr_w_28_;
  tri   mem_addr_w_27_;
  tri   mem_addr_w_26_;
  tri   mem_addr_w_25_;
  tri   mem_addr_w_24_;
  tri   mem_addr_w_23_;
  tri   mem_addr_w_22_;
  tri   mem_addr_w_21_;
  tri   mem_addr_w_20_;
  tri   mem_addr_w_1_;
  tri   mem_addr_w_19_;
  tri   mem_addr_w_18_;
  tri   mem_addr_w_17_;
  tri   mem_addr_w_16_;
  tri   mem_addr_w_15_;
  tri   mem_addr_w_14_;
  tri   mem_addr_w_13_;
  tri   mem_addr_w_12_;
  tri   mem_addr_w_11_;
  tri   mem_addr_w_10_;
  tri   mem_addr_w_0_;
  tri   irdata_w_9_;
  tri   irdata_w_8_;
  tri   irdata_w_7_;
  tri   irdata_w_6_;
  tri   irdata_w_5_;
  tri   irdata_w_4_;
  tri   irdata_w_3_;
  tri   irdata_w_31_;
  tri   irdata_w_30_;
  tri   irdata_w_2_;
  tri   irdata_w_29_;
  tri   irdata_w_28_;
  tri   irdata_w_27_;
  tri   irdata_w_26_;
  tri   irdata_w_25_;
  tri   irdata_w_24_;
  tri   irdata_w_23_;
  tri   irdata_w_22_;
  tri   irdata_w_21_;
  tri   irdata_w_20_;
  tri   irdata_w_1_;
  tri   irdata_w_19_;
  tri   irdata_w_18_;
  tri   irdata_w_17_;
  tri   irdata_w_16_;
  tri   irdata_w_15_;
  tri   irdata_w_14_;
  tri   irdata_w_13_;
  tri   irdata_w_12_;
  tri   irdata_w_11_;
  tri   irdata_w_10_;
  tri   irdata_w_0_;
  tri   ird_w;
  tri   iaddr_w_9_;
  tri   iaddr_w_8_;
  tri   iaddr_w_7_;
  tri   iaddr_w_6_;
  tri   iaddr_w_5_;
  tri   iaddr_w_4_;
  tri   iaddr_w_3_;
  tri   iaddr_w_31_;
  tri   iaddr_w_30_;
  tri   iaddr_w_2_;
  tri   iaddr_w_29_;
  tri   iaddr_w_28_;
  tri   iaddr_w_27_;
  tri   iaddr_w_26_;
  tri   iaddr_w_25_;
  tri   iaddr_w_24_;
  tri   iaddr_w_23_;
  tri   iaddr_w_22_;
  tri   iaddr_w_21_;
  tri   iaddr_w_20_;
  tri   iaddr_w_1_;
  tri   iaddr_w_19_;
  tri   iaddr_w_18_;
  tri   iaddr_w_17_;
  tri   iaddr_w_16_;
  tri   iaddr_w_15_;
  tri   iaddr_w_14_;
  tri   iaddr_w_13_;
  tri   iaddr_w_12_;
  tri   iaddr_w_11_;
  tri   iaddr_w_10_;
  tri   iaddr_w_0_;
  tri   dwr_w;
  tri   dwdata_w_9_;
  tri   dwdata_w_8_;
  tri   dwdata_w_7_;
  tri   dwdata_w_6_;
  tri   dwdata_w_5_;
  tri   dwdata_w_4_;
  tri   dwdata_w_3_;
  tri   dwdata_w_31_;
  tri   dwdata_w_30_;
  tri   dwdata_w_2_;
  tri   dwdata_w_29_;
  tri   dwdata_w_28_;
  tri   dwdata_w_27_;
  tri   dwdata_w_26_;
  tri   dwdata_w_25_;
  tri   dwdata_w_24_;
  tri   dwdata_w_23_;
  tri   dwdata_w_22_;
  tri   dwdata_w_21_;
  tri   dwdata_w_20_;
  tri   dwdata_w_1_;
  tri   dwdata_w_19_;
  tri   dwdata_w_18_;
  tri   dwdata_w_17_;
  tri   dwdata_w_16_;
  tri   dwdata_w_15_;
  tri   dwdata_w_14_;
  tri   dwdata_w_13_;
  tri   dwdata_w_12_;
  tri   dwdata_w_11_;
  tri   dwdata_w_10_;
  tri   dwdata_w_0_;
  tri   dsize_w_1_;
  tri   dsize_w_0_;
  tri   drdata_w_9_;
  tri   drdata_w_8_;
  tri   drdata_w_7_;
  tri   drdata_w_6_;
  tri   drdata_w_5_;
  tri   drdata_w_4_;
  tri   drdata_w_3_;
  tri   drdata_w_31_;
  tri   drdata_w_30_;
  tri   drdata_w_2_;
  tri   drdata_w_29_;
  tri   drdata_w_28_;
  tri   drdata_w_27_;
  tri   drdata_w_26_;
  tri   drdata_w_25_;
  tri   drdata_w_24_;
  tri   drdata_w_23_;
  tri   drdata_w_22_;
  tri   drdata_w_21_;
  tri   drdata_w_20_;
  tri   drdata_w_1_;
  tri   drdata_w_19_;
  tri   drdata_w_18_;
  tri   drdata_w_17_;
  tri   drdata_w_16_;
  tri   drdata_w_15_;
  tri   drdata_w_14_;
  tri   drdata_w_13_;
  tri   drdata_w_12_;
  tri   drdata_w_11_;
  tri   drdata_w_10_;
  tri   drdata_w_0_;
  tri   drd_w;
  tri   daddr_w_9_;
  tri   daddr_w_8_;
  tri   daddr_w_7_;
  tri   daddr_w_6_;
  tri   daddr_w_5_;
  tri   daddr_w_4_;
  tri   daddr_w_3_;
  tri   daddr_w_2_;
  tri   daddr_w_27_;
  tri   daddr_w_26_;
  tri   daddr_w_25_;
  tri   daddr_w_24_;
  tri   daddr_w_23_;
  tri   daddr_w_22_;
  tri   daddr_w_21_;
  tri   daddr_w_20_;
  tri   daddr_w_1_;
  tri   daddr_w_19_;
  tri   daddr_w_18_;
  tri   daddr_w_17_;
  tri   daddr_w_16_;
  tri   daddr_w_15_;
  tri   daddr_w_14_;
  tri   daddr_w_13_;
  tri   daddr_w_12_;
  tri   daddr_w_11_;
  tri   daddr_w_10_;
  tri   daddr_w_0_;

  riscv_core core_i ( .clk_i(clk_i), .reset_i(reset_i), .lock_o(lock_o), 
        .iaddr_o({iaddr_w_31_, iaddr_w_30_, iaddr_w_29_, iaddr_w_28_, 
        iaddr_w_27_, iaddr_w_26_, iaddr_w_25_, iaddr_w_24_, iaddr_w_23_, 
        iaddr_w_22_, iaddr_w_21_, iaddr_w_20_, iaddr_w_19_, iaddr_w_18_, 
        iaddr_w_17_, iaddr_w_16_, iaddr_w_15_, iaddr_w_14_, iaddr_w_13_, 
        iaddr_w_12_, iaddr_w_11_, iaddr_w_10_, iaddr_w_9_, iaddr_w_8_, 
        iaddr_w_7_, iaddr_w_6_, iaddr_w_5_, iaddr_w_4_, iaddr_w_3_, iaddr_w_2_, 
        iaddr_w_1_, iaddr_w_0_}), .irdata_i({irdata_w_31_, irdata_w_30_, 
        irdata_w_29_, irdata_w_28_, irdata_w_27_, irdata_w_26_, irdata_w_25_, 
        irdata_w_24_, irdata_w_23_, irdata_w_22_, irdata_w_21_, irdata_w_20_, 
        irdata_w_19_, irdata_w_18_, irdata_w_17_, irdata_w_16_, irdata_w_15_, 
        irdata_w_14_, irdata_w_13_, irdata_w_12_, irdata_w_11_, irdata_w_10_, 
        irdata_w_9_, irdata_w_8_, irdata_w_7_, irdata_w_6_, irdata_w_5_, 
        irdata_w_4_, irdata_w_3_, irdata_w_2_, irdata_w_1_, irdata_w_0_}), 
        .ird_o(ird_w), .daddr_o({daddr_w, daddr_w_27_, daddr_w_26_, 
        daddr_w_25_, daddr_w_24_, daddr_w_23_, daddr_w_22_, daddr_w_21_, 
        daddr_w_20_, daddr_w_19_, daddr_w_18_, daddr_w_17_, daddr_w_16_, 
        daddr_w_15_, daddr_w_14_, daddr_w_13_, daddr_w_12_, daddr_w_11_, 
        daddr_w_10_, daddr_w_9_, daddr_w_8_, daddr_w_7_, daddr_w_6_, 
        daddr_w_5_, daddr_w_4_, daddr_w_3_, daddr_w_2_, daddr_w_1_, daddr_w_0_}), .dwdata_o({dwdata_w_31_, dwdata_w_30_, dwdata_w_29_, dwdata_w_28_, 
        dwdata_w_27_, dwdata_w_26_, dwdata_w_25_, dwdata_w_24_, dwdata_w_23_, 
        dwdata_w_22_, dwdata_w_21_, dwdata_w_20_, dwdata_w_19_, dwdata_w_18_, 
        dwdata_w_17_, dwdata_w_16_, dwdata_w_15_, dwdata_w_14_, dwdata_w_13_, 
        dwdata_w_12_, dwdata_w_11_, dwdata_w_10_, dwdata_w_9_, dwdata_w_8_, 
        dwdata_w_7_, dwdata_w_6_, dwdata_w_5_, dwdata_w_4_, dwdata_w_3_, 
        dwdata_w_2_, dwdata_w_1_, dwdata_w_0_}), .drdata_i({drdata_w_31_, 
        drdata_w_30_, drdata_w_29_, drdata_w_28_, drdata_w_27_, drdata_w_26_, 
        drdata_w_25_, drdata_w_24_, drdata_w_23_, drdata_w_22_, drdata_w_21_, 
        drdata_w_20_, drdata_w_19_, drdata_w_18_, drdata_w_17_, drdata_w_16_, 
        drdata_w_15_, drdata_w_14_, drdata_w_13_, drdata_w_12_, drdata_w_11_, 
        drdata_w_10_, drdata_w_9_, drdata_w_8_, drdata_w_7_, drdata_w_6_, 
        drdata_w_5_, drdata_w_4_, drdata_w_3_, drdata_w_2_, drdata_w_1_, 
        drdata_w_0_}), .dsize_o({dsize_w_1_, dsize_w_0_}), .drd_o(drd_w), 
        .dwr_o(dwr_w) );
  bus_mux bus_mux_i ( .clk_i(clk_i), .reset_i(reset_i), .ss_i({N3, N8, N13, 
        N19}), .m_addr_i({daddr_w, daddr_w_27_, daddr_w_26_, daddr_w_25_, 
        daddr_w_24_, daddr_w_23_, daddr_w_22_, daddr_w_21_, daddr_w_20_, 
        daddr_w_19_, daddr_w_18_, daddr_w_17_, daddr_w_16_, daddr_w_15_, 
        daddr_w_14_, daddr_w_13_, daddr_w_12_, daddr_w_11_, daddr_w_10_, 
        daddr_w_9_, daddr_w_8_, daddr_w_7_, daddr_w_6_, daddr_w_5_, daddr_w_4_, 
        daddr_w_3_, daddr_w_2_, daddr_w_1_, daddr_w_0_}), .m_wdata_i({
        dwdata_w_31_, dwdata_w_30_, dwdata_w_29_, dwdata_w_28_, dwdata_w_27_, 
        dwdata_w_26_, dwdata_w_25_, dwdata_w_24_, dwdata_w_23_, dwdata_w_22_, 
        dwdata_w_21_, dwdata_w_20_, dwdata_w_19_, dwdata_w_18_, dwdata_w_17_, 
        dwdata_w_16_, dwdata_w_15_, dwdata_w_14_, dwdata_w_13_, dwdata_w_12_, 
        dwdata_w_11_, dwdata_w_10_, dwdata_w_9_, dwdata_w_8_, dwdata_w_7_, 
        dwdata_w_6_, dwdata_w_5_, dwdata_w_4_, dwdata_w_3_, dwdata_w_2_, 
        dwdata_w_1_, dwdata_w_0_}), .m_rdata_o({drdata_w_31_, drdata_w_30_, 
        drdata_w_29_, drdata_w_28_, drdata_w_27_, drdata_w_26_, drdata_w_25_, 
        drdata_w_24_, drdata_w_23_, drdata_w_22_, drdata_w_21_, drdata_w_20_, 
        drdata_w_19_, drdata_w_18_, drdata_w_17_, drdata_w_16_, drdata_w_15_, 
        drdata_w_14_, drdata_w_13_, drdata_w_12_, drdata_w_11_, drdata_w_10_, 
        drdata_w_9_, drdata_w_8_, drdata_w_7_, drdata_w_6_, drdata_w_5_, 
        drdata_w_4_, drdata_w_3_, drdata_w_2_, drdata_w_1_, drdata_w_0_}), 
        .m_size_i({dsize_w_1_, dsize_w_0_}), .m_rd_i(drd_w), .m_wr_i(dwr_w), 
        .s_addr_o({mem_addr_w_31_, mem_addr_w_30_, mem_addr_w_29_, 
        mem_addr_w_28_, mem_addr_w_27_, mem_addr_w_26_, mem_addr_w_25_, 
        mem_addr_w_24_, mem_addr_w_23_, mem_addr_w_22_, mem_addr_w_21_, 
        mem_addr_w_20_, mem_addr_w_19_, mem_addr_w_18_, mem_addr_w_17_, 
        mem_addr_w_16_, mem_addr_w_15_, mem_addr_w_14_, mem_addr_w_13_, 
        mem_addr_w_12_, mem_addr_w_11_, mem_addr_w_10_, mem_addr_w_9_, 
        mem_addr_w_8_, mem_addr_w_7_, mem_addr_w_6_, mem_addr_w_5_, 
        mem_addr_w_4_, mem_addr_w_3_, mem_addr_w_2_, mem_addr_w_1_, 
        mem_addr_w_0_, uart_addr_w, gpio_addr_w, timer_addr_w}), .s_wdata_o({
        mem_wdata_w_31_, mem_wdata_w_30_, mem_wdata_w_29_, mem_wdata_w_28_, 
        mem_wdata_w_27_, mem_wdata_w_26_, mem_wdata_w_25_, mem_wdata_w_24_, 
        mem_wdata_w_23_, mem_wdata_w_22_, mem_wdata_w_21_, mem_wdata_w_20_, 
        mem_wdata_w_19_, mem_wdata_w_18_, mem_wdata_w_17_, mem_wdata_w_16_, 
        mem_wdata_w_15_, mem_wdata_w_14_, mem_wdata_w_13_, mem_wdata_w_12_, 
        mem_wdata_w_11_, mem_wdata_w_10_, mem_wdata_w_9_, mem_wdata_w_8_, 
        mem_wdata_w_7_, mem_wdata_w_6_, mem_wdata_w_5_, mem_wdata_w_4_, 
        mem_wdata_w_3_, mem_wdata_w_2_, mem_wdata_w_1_, mem_wdata_w_0_, 
        uart_wdata_w, gpio_wdata_w, timer_wdata_w}), .s_rdata_i({
        mem_rdata_w_31_, mem_rdata_w_30_, mem_rdata_w_29_, mem_rdata_w_28_, 
        mem_rdata_w_27_, mem_rdata_w_26_, mem_rdata_w_25_, mem_rdata_w_24_, 
        mem_rdata_w_23_, mem_rdata_w_22_, mem_rdata_w_21_, mem_rdata_w_20_, 
        mem_rdata_w_19_, mem_rdata_w_18_, mem_rdata_w_17_, mem_rdata_w_16_, 
        mem_rdata_w_15_, mem_rdata_w_14_, mem_rdata_w_13_, mem_rdata_w_12_, 
        mem_rdata_w_11_, mem_rdata_w_10_, mem_rdata_w_9_, mem_rdata_w_8_, 
        mem_rdata_w_7_, mem_rdata_w_6_, mem_rdata_w_5_, mem_rdata_w_4_, 
        mem_rdata_w_3_, mem_rdata_w_2_, mem_rdata_w_1_, mem_rdata_w_0_, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        uart_rdata_w[7:0], gpio_rdata_w, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        timer_rdata_w[2:0]}), .s_size_o({mem_size_w_1_, mem_size_w_0_, 
        uart_size_w, gpio_size_w, timer_size_w}), .s_rd_o({mem_rd_w, uart_rd_w, 
        gpio_rd_w, timer_rd_w}), .s_wr_o({mem_wr_w, uart_wr_w, gpio_wr_w, 
        timer_wr_w}) );
  memory memory_i ( .clk_i(clk_i), .reset_i(reset_i), .iaddr_i({iaddr_w_31_, 
        iaddr_w_30_, iaddr_w_29_, iaddr_w_28_, iaddr_w_27_, iaddr_w_26_, 
        iaddr_w_25_, iaddr_w_24_, iaddr_w_23_, iaddr_w_22_, iaddr_w_21_, 
        iaddr_w_20_, iaddr_w_19_, iaddr_w_18_, iaddr_w_17_, iaddr_w_16_, 
        iaddr_w_15_, iaddr_w_14_, iaddr_w_13_, iaddr_w_12_, iaddr_w_11_, 
        iaddr_w_10_, iaddr_w_9_, iaddr_w_8_, iaddr_w_7_, iaddr_w_6_, 
        iaddr_w_5_, iaddr_w_4_, iaddr_w_3_, iaddr_w_2_, iaddr_w_1_, iaddr_w_0_}), .irdata_o({irdata_w_31_, irdata_w_30_, irdata_w_29_, irdata_w_28_, 
        irdata_w_27_, irdata_w_26_, irdata_w_25_, irdata_w_24_, irdata_w_23_, 
        irdata_w_22_, irdata_w_21_, irdata_w_20_, irdata_w_19_, irdata_w_18_, 
        irdata_w_17_, irdata_w_16_, irdata_w_15_, irdata_w_14_, irdata_w_13_, 
        irdata_w_12_, irdata_w_11_, irdata_w_10_, irdata_w_9_, irdata_w_8_, 
        irdata_w_7_, irdata_w_6_, irdata_w_5_, irdata_w_4_, irdata_w_3_, 
        irdata_w_2_, irdata_w_1_, irdata_w_0_}), .ird_i(ird_w), .daddr_i({
        mem_addr_w_31_, mem_addr_w_30_, mem_addr_w_29_, mem_addr_w_28_, 
        mem_addr_w_27_, mem_addr_w_26_, mem_addr_w_25_, mem_addr_w_24_, 
        mem_addr_w_23_, mem_addr_w_22_, mem_addr_w_21_, mem_addr_w_20_, 
        mem_addr_w_19_, mem_addr_w_18_, mem_addr_w_17_, mem_addr_w_16_, 
        mem_addr_w_15_, mem_addr_w_14_, mem_addr_w_13_, mem_addr_w_12_, 
        mem_addr_w_11_, mem_addr_w_10_, mem_addr_w_9_, mem_addr_w_8_, 
        mem_addr_w_7_, mem_addr_w_6_, mem_addr_w_5_, mem_addr_w_4_, 
        mem_addr_w_3_, mem_addr_w_2_, mem_addr_w_1_, mem_addr_w_0_}), 
        .dwdata_i({mem_wdata_w_31_, mem_wdata_w_30_, mem_wdata_w_29_, 
        mem_wdata_w_28_, mem_wdata_w_27_, mem_wdata_w_26_, mem_wdata_w_25_, 
        mem_wdata_w_24_, mem_wdata_w_23_, mem_wdata_w_22_, mem_wdata_w_21_, 
        mem_wdata_w_20_, mem_wdata_w_19_, mem_wdata_w_18_, mem_wdata_w_17_, 
        mem_wdata_w_16_, mem_wdata_w_15_, mem_wdata_w_14_, mem_wdata_w_13_, 
        mem_wdata_w_12_, mem_wdata_w_11_, mem_wdata_w_10_, mem_wdata_w_9_, 
        mem_wdata_w_8_, mem_wdata_w_7_, mem_wdata_w_6_, mem_wdata_w_5_, 
        mem_wdata_w_4_, mem_wdata_w_3_, mem_wdata_w_2_, mem_wdata_w_1_, 
        mem_wdata_w_0_}), .drdata_o({mem_rdata_w_31_, mem_rdata_w_30_, 
        mem_rdata_w_29_, mem_rdata_w_28_, mem_rdata_w_27_, mem_rdata_w_26_, 
        mem_rdata_w_25_, mem_rdata_w_24_, mem_rdata_w_23_, mem_rdata_w_22_, 
        mem_rdata_w_21_, mem_rdata_w_20_, mem_rdata_w_19_, mem_rdata_w_18_, 
        mem_rdata_w_17_, mem_rdata_w_16_, mem_rdata_w_15_, mem_rdata_w_14_, 
        mem_rdata_w_13_, mem_rdata_w_12_, mem_rdata_w_11_, mem_rdata_w_10_, 
        mem_rdata_w_9_, mem_rdata_w_8_, mem_rdata_w_7_, mem_rdata_w_6_, 
        mem_rdata_w_5_, mem_rdata_w_4_, mem_rdata_w_3_, mem_rdata_w_2_, 
        mem_rdata_w_1_, mem_rdata_w_0_}), .dsize_i({mem_size_w_1_, 
        mem_size_w_0_}), .drd_i(mem_rd_w), .dwr_i(mem_wr_w) );
  per_uart per_uart_i ( .clk_i(clk_i), .reset_i(reset_i), .addr_i(uart_addr_w), 
        .wdata_i(uart_wdata_w), .rdata_o({SYNOPSYS_UNCONNECTED_1, 
        SYNOPSYS_UNCONNECTED_2, SYNOPSYS_UNCONNECTED_3, SYNOPSYS_UNCONNECTED_4, 
        SYNOPSYS_UNCONNECTED_5, SYNOPSYS_UNCONNECTED_6, SYNOPSYS_UNCONNECTED_7, 
        SYNOPSYS_UNCONNECTED_8, SYNOPSYS_UNCONNECTED_9, 
        SYNOPSYS_UNCONNECTED_10, SYNOPSYS_UNCONNECTED_11, 
        SYNOPSYS_UNCONNECTED_12, SYNOPSYS_UNCONNECTED_13, 
        SYNOPSYS_UNCONNECTED_14, SYNOPSYS_UNCONNECTED_15, 
        SYNOPSYS_UNCONNECTED_16, SYNOPSYS_UNCONNECTED_17, 
        SYNOPSYS_UNCONNECTED_18, SYNOPSYS_UNCONNECTED_19, 
        SYNOPSYS_UNCONNECTED_20, SYNOPSYS_UNCONNECTED_21, 
        SYNOPSYS_UNCONNECTED_22, SYNOPSYS_UNCONNECTED_23, 
        SYNOPSYS_UNCONNECTED_24, uart_rdata_w[7:0]}), .size_i(uart_size_w), 
        .rd_i(uart_rd_w), .wr_i(uart_wr_w), .uart_rx_i(uart_rx_i), .uart_tx_o(
        uart_tx_o) );
  per_gpio per_gpio_i ( .clk_i(clk_i), .reset_i(reset_i), .addr_i(gpio_addr_w), 
        .wdata_i(gpio_wdata_w), .rdata_o(gpio_rdata_w), .size_i(gpio_size_w), 
        .rd_i(gpio_rd_w), .wr_i(gpio_wr_w), .gpio_in_i(gpio_in_i), 
        .gpio_out_o(gpio_out_o) );
  per_timer per_timer_i ( .clk_i(clk_i), .reset_i(reset_i), .addr_i(
        timer_addr_w), .wdata_i(timer_wdata_w), .rdata_o({
        SYNOPSYS_UNCONNECTED_25, SYNOPSYS_UNCONNECTED_26, 
        SYNOPSYS_UNCONNECTED_27, SYNOPSYS_UNCONNECTED_28, 
        SYNOPSYS_UNCONNECTED_29, SYNOPSYS_UNCONNECTED_30, 
        SYNOPSYS_UNCONNECTED_31, SYNOPSYS_UNCONNECTED_32, 
        SYNOPSYS_UNCONNECTED_33, SYNOPSYS_UNCONNECTED_34, 
        SYNOPSYS_UNCONNECTED_35, SYNOPSYS_UNCONNECTED_36, 
        SYNOPSYS_UNCONNECTED_37, SYNOPSYS_UNCONNECTED_38, 
        SYNOPSYS_UNCONNECTED_39, SYNOPSYS_UNCONNECTED_40, 
        SYNOPSYS_UNCONNECTED_41, SYNOPSYS_UNCONNECTED_42, 
        SYNOPSYS_UNCONNECTED_43, SYNOPSYS_UNCONNECTED_44, 
        SYNOPSYS_UNCONNECTED_45, SYNOPSYS_UNCONNECTED_46, 
        SYNOPSYS_UNCONNECTED_47, SYNOPSYS_UNCONNECTED_48, 
        SYNOPSYS_UNCONNECTED_49, SYNOPSYS_UNCONNECTED_50, 
        SYNOPSYS_UNCONNECTED_51, SYNOPSYS_UNCONNECTED_52, 
        SYNOPSYS_UNCONNECTED_53, timer_rdata_w[2:0]}), .size_i(timer_size_w), 
        .rd_i(timer_rd_w), .wr_i(timer_wr_w) );
  INVHD1X I_2 ( .A(N7), .Z(N8) );
  INVHD1X I_4 ( .A(N12), .Z(N13) );
  INVHD1X I_7 ( .A(N18), .Z(N19) );
  INVHD1X I_0 ( .A(N2), .Z(N3) );
  NAND3HDLX U8 ( .A(n6), .B(n7), .C(daddr_w[28]), .Z(N7) );
  NAND3HDLX U9 ( .A(n8), .B(n7), .C(n6), .Z(N2) );
  INVCLKHD1X U10 ( .A(daddr_w[29]), .Z(n7) );
  NAND3HDLX U11 ( .A(daddr_w[28]), .B(n6), .C(daddr_w[29]), .Z(N18) );
  NAND3HDLX U12 ( .A(n6), .B(n8), .C(daddr_w[29]), .Z(N12) );
  INVCLKHD1X U13 ( .A(daddr_w[28]), .Z(n8) );
  NOR2HDUX U14 ( .A(daddr_w[30]), .B(daddr_w[31]), .Z(n6) );
endmodule


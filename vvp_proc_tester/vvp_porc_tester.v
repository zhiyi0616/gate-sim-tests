module vvp_proc_tester();
reg [7:0] clk, a1,b1,c1,a2,b2,c2,a3,c3;
always@(posedge clk) begin
    a1 = 1;
    #11 b1 = a1;
    c1 = #22 b1;
end

always@(posedge clk) begin
    a2 <= 1;
    #17 b2 <= a2;
    c2 <= #28 b2;
end

always begin
    a3 = 1;
    #10 c3 = 1;
end

initial forever begin
        a3 = 1;
        #10 c3 = 1;
end


endmodule




本设计用于测试 VVP 文件，不需要综合。

# VVP 中 always 和 initial 的区别

## always
```
T_X
    %wait EVENT
    <operations>
    %jmp T_X
    .thread T_X
```

## initial
```
T_X
    <operations>
    %end
    .thread T_X
```

## 测试对比

always 不带 @ 测试如下：
```verilog
always begin
    a = 1;
    #10 c = 1;
end
```

```
T_X ;
    %pushi/vec4 1, 0, 8;
    %store/vec4 labe_a_, 0, 8;
    %delay 10, 0;
    %pushi/vec4 1, 0, 8;
    %store/vec4 labe_c_, 0, 8;
    %jmp T_X;
    .thread T_X;
```


initial-forever 测试如下：
```verilog
initial forever begin
        a = 1;
        #10 c = 1;
end
```
```
T_X ;
T_X.0 ;
    %pushi/vec4 1, 0, 8;
    %store/vec4 labe_a_, 0, 8;
    %delay 10, 0;
    %pushi/vec4 1, 0, 8;
    %store/vec4 labe_c_, 0, 8;
    %jmp T_X.0;
    %end;
    .thread T_X;
```
总结：本质没有区别。但是 initial 一般会被综合器忽略。
